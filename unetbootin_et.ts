<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="et_EE">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="266"/>
        <source>LeftToRight</source>
        <translation>Vasakult paremale</translation>
    </message>
</context>
<context>
    <name>unetbootin</name>
    <message>
        <location filename="unetbootin.cpp" line="213"/>
        <location filename="unetbootin.cpp" line="314"/>
        <location filename="unetbootin.cpp" line="315"/>
        <location filename="unetbootin.cpp" line="384"/>
        <location filename="unetbootin.cpp" line="558"/>
        <location filename="unetbootin.cpp" line="3436"/>
        <location filename="unetbootin.cpp" line="3449"/>
        <location filename="unetbootin.cpp" line="3631"/>
        <location filename="unetbootin.cpp" line="4291"/>
        <source>Hard Disk</source>
        <translation>Kõvaketas</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="215"/>
        <location filename="unetbootin.cpp" line="311"/>
        <location filename="unetbootin.cpp" line="312"/>
        <location filename="unetbootin.cpp" line="386"/>
        <location filename="unetbootin.cpp" line="562"/>
        <location filename="unetbootin.cpp" line="728"/>
        <location filename="unetbootin.cpp" line="748"/>
        <location filename="unetbootin.cpp" line="1002"/>
        <location filename="unetbootin.cpp" line="1577"/>
        <location filename="unetbootin.cpp" line="1664"/>
        <location filename="unetbootin.cpp" line="2593"/>
        <location filename="unetbootin.cpp" line="2636"/>
        <location filename="unetbootin.cpp" line="3440"/>
        <location filename="unetbootin.cpp" line="3466"/>
        <location filename="unetbootin.cpp" line="3635"/>
        <location filename="unetbootin.cpp" line="3959"/>
        <location filename="unetbootin.cpp" line="4295"/>
        <source>USB Drive</source>
        <translation>USB-ketas</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="216"/>
        <location filename="unetbootin.cpp" line="233"/>
        <location filename="unetbootin.cpp" line="234"/>
        <location filename="unetbootin.cpp" line="350"/>
        <location filename="unetbootin.cpp" line="676"/>
        <location filename="unetbootin.cpp" line="679"/>
        <location filename="unetbootin.cpp" line="680"/>
        <location filename="unetbootin.cpp" line="3525"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="217"/>
        <location filename="unetbootin.cpp" line="229"/>
        <location filename="unetbootin.cpp" line="230"/>
        <location filename="unetbootin.cpp" line="356"/>
        <location filename="unetbootin.cpp" line="676"/>
        <location filename="unetbootin.cpp" line="684"/>
        <location filename="unetbootin.cpp" line="685"/>
        <location filename="unetbootin.cpp" line="3517"/>
        <source>Floppy</source>
        <translation>Diskett</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="250"/>
        <location filename="unetbootin.cpp" line="256"/>
        <location filename="unetbootin.cpp" line="260"/>
        <location filename="unetbootin.cpp" line="264"/>
        <location filename="unetbootin.cpp" line="268"/>
        <location filename="unetbootin.cpp" line="274"/>
        <location filename="unetbootin.cpp" line="302"/>
        <source>either</source>
        <translation>või siis</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="281"/>
        <source>LiveUSB persistence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="296"/>
        <source>FAT32-formatted USB drive</source>
        <translation>FAT32-vormingus USB ketas</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="300"/>
        <source>EXT2-formatted USB drive</source>
        <translation>EXT2-vormingus USB ketas</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="676"/>
        <source>Open Disk Image File</source>
        <translation>Kettatõmmise avamine</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="676"/>
        <source>All Files</source>
        <translation>Kõik failid</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="694"/>
        <location filename="unetbootin.cpp" line="702"/>
        <location filename="unetbootin.cpp" line="710"/>
        <source>All Files (*)</source>
        <translation>Kõik failid (*)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="694"/>
        <source>Open Kernel File</source>
        <translation>Kerneli faili avamine</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="702"/>
        <source>Open Initrd File</source>
        <translation>Initrd faili avamine</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="710"/>
        <source>Open Bootloader Config File</source>
        <translation>Alglaaduri seadistusfaili avamine</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="732"/>
        <source>Insert a USB flash drive</source>
        <translation>Sisesta USB flash-ketas</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="733"/>
        <source>No USB flash drives were found. If you have already inserted a USB drive, try reformatting it as FAT32.</source>
        <translation>Mitte ühtegi USB flash-ketast ei leitud. Kui sa oled juba sisestanud USB ketta, proovi vormindada see FAT32 failisüsteemina.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="752"/>
        <source>%1 not mounted</source>
        <translation>%1 ei ole haagitud</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="753"/>
        <source>You must first mount the USB drive %1 to a mountpoint. Most distributions will do this automatically after you remove and reinsert the USB drive.</source>
        <translation>Kõigepealt pead USB-ketta %1 haakima. Enamik distributsioone teeb seda automaatselt pärast USB-ketta eemaldamist ja uuesti sisestamist.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="768"/>
        <source>Select a distro</source>
        <translation>Vali distributsioon</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="769"/>
        <source>You must select a distribution to load.</source>
        <translation>Pead valima distributsioonid, mida laadida.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="783"/>
        <source>Select a disk image file</source>
        <translation>Kettatõmmise faili valimine</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="784"/>
        <source>You must select a disk image file to load.</source>
        <translation>Pead valima kettatõmmise faili, mida laadida.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="798"/>
        <source>Select a kernel and/or initrd file</source>
        <translation>Kerneli ja/või initrd faili valimine</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="799"/>
        <source>You must select a kernel and/or initrd file to load.</source>
        <translation>Sa pead valima kerneli ja/või initrd faili, mida laadida.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="813"/>
        <source>Diskimage file not found</source>
        <translation>Kettatõmmist ei leitud</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="814"/>
        <source>The specified diskimage file %1 does not exist.</source>
        <translation>Määratud kettatõmmise faili %1 pole olemas.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="828"/>
        <source>Kernel file not found</source>
        <translation>Kernelifaili ei leitud</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="829"/>
        <source>The specified kernel file %1 does not exist.</source>
        <translation>Määratud kernelifaili %1 pole olemas.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="843"/>
        <source>Initrd file not found</source>
        <translation>Initrd faili ei leitud</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="844"/>
        <source>The specified initrd file %1 does not exist.</source>
        <translation>Määratud initrd faili %1 pole olemas.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="948"/>
        <source>%1 exists, overwrite?</source>
        <translation>%1 on juba olemas, kas kirjutada üle?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="949"/>
        <source>The file %1 already exists. Press &apos;Yes to All&apos; to overwrite it and not be prompted again, &apos;Yes&apos; to overwrite files on an individual basis, and &apos;No&apos; to retain your existing version. If in doubt, press &apos;Yes to All&apos;.</source>
        <translation>Fail %1 on juba olemas. Vajuta &apos;Kõigile jah&apos;, et see ja kõik teised üle kirjutada. &apos;Jah&apos;, et üle kirjutada ainult see fail ning &apos;Ei&apos; et säilitada paregune fail. Kui sa kahtled, vali &apos;Kõigile jah&apos;.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="975"/>
        <source>%1 is out of space, abort installation?</source>
        <translation>%1 on mälu täis, kas katkesta paigaldamine?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="976"/>
        <source>The directory %1 is out of space. Press &apos;Yes&apos; to abort installation, &apos;No&apos; to ignore this error and attempt to continue installation, and &apos;No to All&apos; to ignore all out-of-space errors.</source>
        <translation>Kaustas %1 ei ole rohkem vaba ruumi. Vajuta &apos;Jah&apos;, et katkestada paigaldus, &apos;Ei&apos;, et eirata seda veateadet ning püüda paigaldust jätkata, ning &apos;Kõigile ei&apos;, et eirata kõiki ruumilõppemise teateid.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1070"/>
        <source>Locating kernel file in %1</source>
        <translation>Kernelifaili otsimine asukohast %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1121"/>
        <source>Copying kernel file from %1</source>
        <translation>Kernelifaili kopeerimine asukohast %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1127"/>
        <source>Locating initrd file in %1</source>
        <translation>Initrd faili otsimine asukohast %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1168"/>
        <source>Copying initrd file from %1</source>
        <translation>Initrd faili kopeerimine asukohast %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1174"/>
        <location filename="unetbootin.cpp" line="1254"/>
        <source>Extracting bootloader configuration</source>
        <translation>Alglaaduri seadistuse lahtipakkimine</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1483"/>
        <location filename="unetbootin.cpp" line="1509"/>
        <source>&lt;b&gt;Extracting compressed iso:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Pakitud iso faili lahtipakkimine:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1763"/>
        <source>Copying file, please wait...</source>
        <translation>Faili kopeerimine, palun oota...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1764"/>
        <location filename="unetbootin.cpp" line="2578"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;b&gt;Allikas:&lt;/b&gt; &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1765"/>
        <location filename="unetbootin.cpp" line="2579"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Sihtkoht:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1766"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; 0 bytes</source>
        <translation>&lt;b&gt;Kopeeritud:&lt;/b&gt; 0 baiti</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1804"/>
        <source>Extracting files, please wait...</source>
        <translation>Failide lahtipakkimine, palun oota...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1805"/>
        <source>&lt;b&gt;Archive:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Arhiiv:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1806"/>
        <source>&lt;b&gt;Source:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Allikas:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1807"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Sihtkoht:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1808"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; 0 of %1 files</source>
        <translation>&lt;b&gt;Lahti pakitud:&lt;/b&gt; 0  %1-st failist</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1811"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; %1 (%2)</source>
        <translation>&lt;b&gt;Allikas:&lt;/b&gt; %1 (%2)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1812"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1%2</source>
        <translation>&lt;b&gt;Sihtkoht:&lt;/b&gt; %1%2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1813"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; %1 of %2 files</source>
        <translation>&lt;b&gt;Lahti pakitud:&lt;/b&gt; %1 %2-st failist</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2577"/>
        <source>Downloading files, please wait...</source>
        <translation>Failide allalaadimine, palun oota...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2580"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; 0 bytes</source>
        <translation>&lt;b&gt;Allalaaditud:&lt;/b&gt; 0 baiti</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2704"/>
        <source>Download of %1 %2 from %3 failed. Please try downloading the ISO file from the website directly and supply it via the diskimage option.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2727"/>
        <location filename="unetbootin.cpp" line="2742"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; %1 of %2</source>
        <translation>&lt;b&gt;Allalaaditud:&lt;/b&gt; %1 %2-st</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2757"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; %1 of %2</source>
        <translation>&lt;b&gt;Kopeeritud:&lt;/b&gt; %1 / %2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2848"/>
        <source>Searching in &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>Otsimine kaustast &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2852"/>
        <source>%1/%2 matches in &lt;a href=&quot;%3&quot;&gt;%3&lt;/a&gt;</source>
        <translation>%1/%2 vastavust kohas &lt;a href=&quot;%3&quot;&gt;%3&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3125"/>
        <source>%1 not found</source>
        <translation>%1 ei leitud</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3126"/>
        <source>%1 not found. This is required for %2 install mode.
Install the &quot;%3&quot; package or your distribution&apos;s equivalent.</source>
        <translation>%1 ei leitud. See on vajalik %2 paigaldusrežiimiks. 
Paigalda &quot;%3&quot; pakett või distributsiooni vastava.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3417"/>
        <source>(Current)</source>
        <translation>(käsil)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3418"/>
        <source>(Done)</source>
        <translation>(valmis)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3710"/>
        <source>Configuring grub2 on %1</source>
        <translation>Grub2 seadistamine seadmel %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3722"/>
        <source>Configuring grldr on %1</source>
        <translation>Grldr seadistamine seadmel %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3750"/>
        <source>Configuring grub on %1</source>
        <translation>Grubi seadistamine seadmel %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4048"/>
        <source>Installing syslinux to %1</source>
        <translation>Syslinuxi paigaldamine kausta %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4083"/>
        <source>Installing extlinux to %1</source>
        <translation>Extlinuxi paigaldamine kausta %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4243"/>
        <source>Syncing filesystems</source>
        <translation>Failisüsteemide sünkroniseerimine</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4248"/>
        <source>Setting up persistence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4293"/>
        <source>After rebooting, select the </source>
        <translation>Pärast taaskäivitamist vali </translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4298"/>
        <source>After rebooting, select the USB boot option in the BIOS boot menu.%1
Reboot now?</source>
        <translation>Pärast taaskäivitamist vali USB alglaadimine BIOSi menüüst.%1
Kas taaskäivitada kohe?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4301"/>
        <source>The created USB device will not boot off a Mac. Insert it into a PC, and select the USB boot option in the BIOS boot menu.%1</source>
        <translation></translation>
    </message>
    <message>
        <source>
*IMPORTANT* Before rebooting, place an Ubuntu alternate (not desktop) install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.ubuntu.com</source>
        <translation type="obsolete">
*TÄHTIS* Enne taaskäivitust aseta Ubuntu (alternate iso)  iso paigaldusfail kõvaketta juurkausta või USB kettale.
ISO-failid leiab cdimage.ubuntu.com</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;mirrors.kernel.org&apos; when prompted for a server, and enter &apos;/centos/%1/os/%2&apos; when asked for the folder.</source>
        <translation type="obsolete">
*TÄHTIS* Peale taaskäivitamist, ignoreeri igasugused veateated ja vali Tagasi, kui CD on prompted, siis mine põhimenüüsse, vali &apos;Käivita install&apos; valik, vali ´Võrk´ asukohaks, vali ´HTTP´ protokolliks, sisesta ´mirrors.kernel.org´, kui prompted serverina ja sisesta  &apos;/centos/%1/os/%2&apos;  kui küsitakse kausta.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* Before rebooting, place a Debian install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.debian.org</source>
        <translation type="obsolete">
*TÄHTIS* Enne taaskäivitust aseta Debiani iso paigaldusfail kõvaketta juurkausta või USB kettale.
ISO-failid leiab cdimage.debian.org</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="764"/>
        <source>== Select Distribution ==</source>
        <translation>==VALI DISTRIBUTSIOON==</translation>
    </message>
    <message>
        <source>== Select Version ==</source>
        <translation type="obsolete">==VALI VERSIOON==</translation>
    </message>
    <message>
        <source>Welcome to &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;, the Universal Netboot Installer. Usage:&lt;ol&gt;&lt;li&gt;Select a distribution and version to download from the list above, or manually specify files to load below.&lt;/li&gt;&lt;li&gt;Select an installation type, and press OK to begin installing.&lt;/li&gt;&lt;/ol&gt;</source>
        <translation type="obsolete">Tere tulemast kasutama  &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootini&lt;/a&gt;, universaalset Netbooti paigaldajat. Kasutamine:&lt;ol&gt;&lt;li&gt;Vali operatsioonisüsteem ja allalaadimise versioon loendist või osuta alumistel väljadel failidele.&lt;/li&gt;&lt;li&gt;Vali paigalduse liik ja vajuta Olgu, et paigaldust alustada.&lt;/li&gt;&lt;/ol&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.archlinux.org/&quot;&gt;http://www.archlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Arch Linux is a lightweight distribution optimized for speed and flexibility.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for installation over the internet (FTP).</source>
        <translation type="obsolete">&lt;b&gt;Koduleht:&lt;/b&gt; &lt;a href=&quot;http://www.archlinux.org/&quot;&gt;http://www.archlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Kirjeldus:&lt;/b&gt; Arch Linux on vähenõudlik distributsioon, optimeeritud kiiruse ja paindlikuse poolest.&lt;br/&gt;&lt;b&gt;Paigaldusmärkmed:&lt;/b&gt; Vaikeversiooni lubab paigaldada üle interneti (FTP ).</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.backtrack-linux.org/&quot;&gt;http://www.backtrack-linux.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; BackTrack is a distribution focused on network analysis and penetration testing.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; BackTrack is booted and run in live mode; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Kodulehekülg:&lt;/b&gt; &lt;a href=&quot;http://www.backtrack-linux.org/&quot;&gt;http://www.backtrack-linux.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Kirjeldus:&lt;/b&gt; BackTrack on distributsioon, mis keskendub võrguanalüüsile ja ulatuse testimisele.&lt;br/&gt;&lt;b&gt;Paigaldusmärkmed:&lt;/b&gt; BackTrack laaditakse live-režiimis, kasutamiseks pole paigaldamine vajalik.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://clonezilla.org/&quot;&gt;http://clonezilla.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; CloneZilla is a distribution used for disk backup and imaging.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; CloneZilla is booted and run in live mode; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Kodulehekülg:&lt;/b&gt; &lt;a href=&quot;http://clonezilla.org/&quot;&gt;http://clonezilla.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Kirjeldus:&lt;/b&gt; CloneZilla on distributsioon, mida kasutatakse kõvaketta varundamiseks ja tõmmise tegemiseks.&lt;br/&gt;&lt;b&gt;Paigaldusmärkmed:&lt;/b&gt; CloneZilla laaditakse live-režiimis, kasutamiseks pole paigaldamine vajalik.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://damnsmalllinux.org/&quot;&gt;http://damnsmalllinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Damn Small Linux is a minimalist distribution designed for older computers.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional.</source>
        <translation type="obsolete">&lt;b&gt;Kodulehekülg:&lt;/b&gt; &lt;a href=&quot;http://damnsmalllinux.org/&quot;&gt;http://damnsmalllinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Kirjeldus:&lt;/b&gt; Damn Small Linux on minimalistliku välimuse ja suurusega operatsioonisüsteem, mis on mõeldud vanematele arvutitele.&lt;br/&gt;&lt;b&gt;Paigaldamise abi:&lt;/b&gt; Live versioon laadib terve süsteemi RAMli ja käivitub mälust, paigaldus pole nõutav, kuid on võimalik.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.dreamlinux.com.br/&quot;&gt;http://www.dreamlinux.com.br&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Dreamlinux is a user-friendly Debian-based distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Kodulehekülg:&lt;/b&gt; &lt;a href=&quot;http://www.dreamlinux.com.br/&quot;&gt;http://www.dreamlinux.com.br&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Kirjeldus:&lt;/b&gt; Dreamlinux on kasutajasõbralik Debianil põhinev operatsioonisüsteem.&lt;br/&gt;&lt;b&gt;Paigalduse abi:&lt;/b&gt; Live versiooni saab laadida Live režiimis ilma midagi paigaldamata, misjärel võib soovi korral käivatada paigaldaja.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freedos.org/&quot;&gt;http://www.freedos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeDOS is a free MS-DOS compatible operating system.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; See the &lt;a href=&quot;http://fd-doc.sourceforge.net/wiki/index.php?n=FdDocEn.FdInstall&quot;&gt;manual&lt;/a&gt; for installation details.</source>
        <translation type="obsolete">&lt;b&gt;Kodulehekülg:&lt;/b&gt; &lt;a href=&quot;http://www.freedos.org/&quot;&gt;http://www.freedos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Kirjeldus:&lt;/b&gt; FreeDOS on vabavaraline MS-DOSiga ühilduv operatsioonisüsteem.&lt;br/&gt;&lt;b&gt;Paigaldusmärkmed:&lt;/b&gt; Vaata paigalduse üksikasju &lt;a href=&quot;http://fd-doc.sourceforge.net/wiki/index.php?n=FdDocEn.FdInstall&quot;&gt;käsiraamatust&lt;/a&gt;.</translation>
    </message>
</context>
<context>
    <name>unetbootinui</name>
    <message>
        <location filename="unetbootin.ui" line="20"/>
        <source>Unetbootin</source>
        <translation>UNetbootin</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="44"/>
        <location filename="unetbootin.ui" line="65"/>
        <source>Select from a list of supported distributions</source>
        <translation>Vali distributsioon loendist</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="47"/>
        <source>&amp;Distribution</source>
        <translation>&amp;Distributsioon</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="131"/>
        <source>Specify a disk image file to load</source>
        <translation>Määratud kettatõmmise faili laadimine</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="134"/>
        <source>Disk&amp;image</source>
        <translation>&amp;Kettatõmmis</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="147"/>
        <source>Manually specify a kernel and initrd to load</source>
        <translation>Määratud kerneli ja initrd laadimine</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="150"/>
        <source>&amp;Custom</source>
        <translation>&amp;Kohandatud</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="414"/>
        <location filename="unetbootin.ui" line="430"/>
        <source>Space to reserve for user files which are preserved across reboots. Works only for LiveUSBs for Ubuntu and derivatives. If value exceeds drive capacity, the maximum space available will be used.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="417"/>
        <source>Space used to preserve files across reboots (Ubuntu only):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="440"/>
        <source>MB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="503"/>
        <source>OK</source>
        <translation>Olgu</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="506"/>
        <source>Return</source>
        <translation>Tagasi</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="513"/>
        <source>Cancel</source>
        <translation>Loobu</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="516"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="561"/>
        <source>Reboot Now</source>
        <translation>Taaskäivita kohe</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="568"/>
        <source>Exit</source>
        <translation>Välju</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="660"/>
        <source>1. Downloading Files</source>
        <translation>1. Failide allalaadimine</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="667"/>
        <source>2. Extracting and Copying Files</source>
        <translation>2. Failide lahtipakkimine ja kopeerimine</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="674"/>
        <source>3. Installing Bootloader</source>
        <translation>3. Alglaaduri paigaldamine</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="681"/>
        <source>4. Installation Complete, Reboot</source>
        <translation>4. Paigaldamine lõpetatud, taaskäivita.</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="477"/>
        <location filename="unetbootin.ui" line="496"/>
        <source>Select the target drive to install to</source>
        <translation>Vali sihtkoha ketas, kuhu paigaldada</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="480"/>
        <source>Dri&amp;ve:</source>
        <translation>&amp;Ketas:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="451"/>
        <location filename="unetbootin.ui" line="470"/>
        <source>Select the installation target type</source>
        <translation>Vali paigalduse sihtkoha liik</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="454"/>
        <source>&amp;Type:</source>
        <translation>&amp;Liik:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="81"/>
        <source>Select the distribution version</source>
        <translation>Distributsiooni versiooni valimine</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="347"/>
        <source>Select disk image file</source>
        <translation>Kettatõmmise faili valimine</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="286"/>
        <location filename="unetbootin.ui" line="350"/>
        <location filename="unetbootin.ui" line="375"/>
        <location filename="unetbootin.ui" line="400"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="188"/>
        <source>Select the disk image type</source>
        <translation>Kettatõmmise faili liigi valimine</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="243"/>
        <source>Specify a floppy/hard disk image, or CD image (ISO) file to load</source>
        <translation>Vali disketi/kõvaketta tõmmis või plaaditõmmis (ISO), mida laadida</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="207"/>
        <location filename="unetbootin.ui" line="258"/>
        <source>Specify a kernel file to load</source>
        <translation>Kerneli faili valimine, mida laadida</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="283"/>
        <source>Select kernel file</source>
        <translation>Kerneli faili valimine</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="293"/>
        <location filename="unetbootin.ui" line="312"/>
        <source>Specify an initrd file to load</source>
        <translation>Initrd faili valimine, mida laadida</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="372"/>
        <source>Select initrd file</source>
        <translation>Initrd faili valimine</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="397"/>
        <source>Select syslinux.cfg or isolinux.cfg file</source>
        <translation>Syslinux.cfg või isolinux.cfg faili valimine</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="226"/>
        <location filename="unetbootin.ui" line="321"/>
        <source>Specify parameters and options to pass to the kernel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="210"/>
        <source>&amp;Kernel:</source>
        <translation>&amp;Kernel:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="296"/>
        <source>Init&amp;rd:</source>
        <translation>Init&amp;rd:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="229"/>
        <source>&amp;Options:</source>
        <translation>&amp;Valikud:</translation>
    </message>
</context>
<context>
    <name>uninstaller</name>
    <message>
        <location filename="main.cpp" line="156"/>
        <source>Uninstallation Complete</source>
        <translation>Eemaldamine lõpetatud</translation>
    </message>
    <message>
        <location filename="main.cpp" line="157"/>
        <source>%1 has been uninstalled.</source>
        <translation>%1 on eemaldatud.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="322"/>
        <source>Must run as root</source>
        <translation>Tuleb käivitada juurkasutajana</translation>
    </message>
    <message>
        <location filename="main.cpp" line="324"/>
        <source>%2 must be run as root. Close it, and re-run using either:&lt;br/&gt;&lt;b&gt;sudo %1&lt;/b&gt;&lt;br/&gt;or:&lt;br/&gt;&lt;b&gt;su - -c &apos;%1&apos;&lt;/b&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main.cpp" line="361"/>
        <source>%1 Uninstaller</source>
        <translation>%1 eemaldaja</translation>
    </message>
    <message>
        <location filename="main.cpp" line="362"/>
        <source>%1 is currently installed. Remove the existing version?</source>
        <translation>Hetkel on paigaldatud %1. Kas eemalda olemasolev versioon?</translation>
    </message>
</context>
</TS>
