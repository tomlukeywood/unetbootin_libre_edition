<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="lt_LT">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="266"/>
        <source>LeftToRight</source>
        <translation>Iš kairės į dešinę</translation>
    </message>
</context>
<context>
    <name>unetbootin</name>
    <message>
        <location filename="unetbootin.cpp" line="213"/>
        <location filename="unetbootin.cpp" line="314"/>
        <location filename="unetbootin.cpp" line="315"/>
        <location filename="unetbootin.cpp" line="384"/>
        <location filename="unetbootin.cpp" line="558"/>
        <location filename="unetbootin.cpp" line="3436"/>
        <location filename="unetbootin.cpp" line="3449"/>
        <location filename="unetbootin.cpp" line="3631"/>
        <location filename="unetbootin.cpp" line="4291"/>
        <source>Hard Disk</source>
        <translation>Standusis diskas</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="215"/>
        <location filename="unetbootin.cpp" line="311"/>
        <location filename="unetbootin.cpp" line="312"/>
        <location filename="unetbootin.cpp" line="386"/>
        <location filename="unetbootin.cpp" line="562"/>
        <location filename="unetbootin.cpp" line="728"/>
        <location filename="unetbootin.cpp" line="748"/>
        <location filename="unetbootin.cpp" line="1002"/>
        <location filename="unetbootin.cpp" line="1577"/>
        <location filename="unetbootin.cpp" line="1664"/>
        <location filename="unetbootin.cpp" line="2593"/>
        <location filename="unetbootin.cpp" line="2636"/>
        <location filename="unetbootin.cpp" line="3440"/>
        <location filename="unetbootin.cpp" line="3466"/>
        <location filename="unetbootin.cpp" line="3635"/>
        <location filename="unetbootin.cpp" line="3959"/>
        <location filename="unetbootin.cpp" line="4295"/>
        <source>USB Drive</source>
        <translation>USB diskas</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="216"/>
        <location filename="unetbootin.cpp" line="233"/>
        <location filename="unetbootin.cpp" line="234"/>
        <location filename="unetbootin.cpp" line="350"/>
        <location filename="unetbootin.cpp" line="676"/>
        <location filename="unetbootin.cpp" line="679"/>
        <location filename="unetbootin.cpp" line="680"/>
        <location filename="unetbootin.cpp" line="3525"/>
        <source>ISO</source>
        <translation>ISO atvaizdas</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="217"/>
        <location filename="unetbootin.cpp" line="229"/>
        <location filename="unetbootin.cpp" line="230"/>
        <location filename="unetbootin.cpp" line="356"/>
        <location filename="unetbootin.cpp" line="676"/>
        <location filename="unetbootin.cpp" line="684"/>
        <location filename="unetbootin.cpp" line="685"/>
        <location filename="unetbootin.cpp" line="3517"/>
        <source>Floppy</source>
        <translation>Diskelis</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="250"/>
        <location filename="unetbootin.cpp" line="256"/>
        <location filename="unetbootin.cpp" line="260"/>
        <location filename="unetbootin.cpp" line="264"/>
        <location filename="unetbootin.cpp" line="268"/>
        <location filename="unetbootin.cpp" line="274"/>
        <location filename="unetbootin.cpp" line="302"/>
        <source>either</source>
        <translation>arba</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="281"/>
        <source>LiveUSB persistence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="296"/>
        <source>FAT32-formatted USB drive</source>
        <translation>USB diskas suformatuotas į FAT32 failų sistemą</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="300"/>
        <source>EXT2-formatted USB drive</source>
        <translation>USB diskas formatuotas į EXT2 failų sistemą</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="676"/>
        <source>Open Disk Image File</source>
        <translation>Atverti disko atvaizdo rinkmeną</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="676"/>
        <source>All Files</source>
        <translation>Visos rinkmenos</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="694"/>
        <location filename="unetbootin.cpp" line="702"/>
        <location filename="unetbootin.cpp" line="710"/>
        <source>All Files (*)</source>
        <translation>Visos rinkmenos (*)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="694"/>
        <source>Open Kernel File</source>
        <translation>Atverti branduolio rinkmeną</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="702"/>
        <source>Open Initrd File</source>
        <translation>Atvetri Initrd rinkmeną</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="710"/>
        <source>Open Bootloader Config File</source>
        <translation>Atverti pradinės paleisties programos konfigūracinę rinkmeną</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="732"/>
        <source>Insert a USB flash drive</source>
        <translation>Prijungti USB diską</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="733"/>
        <source>No USB flash drives were found. If you have already inserted a USB drive, try reformatting it as FAT32.</source>
        <translation>USB diskas nerastas. Jei Jūs tikrai prijungėte USB diską, pamėginkite jį suformuoti į FAT32 failų sistemą.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="752"/>
        <source>%1 not mounted</source>
        <translation>%1 nėra pajungtas</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="753"/>
        <source>You must first mount the USB drive %1 to a mountpoint. Most distributions will do this automatically after you remove and reinsert the USB drive.</source>
        <translation>Iš pradžių primontuokite %1 USB diską. Dauguma operacinių sistemų, prijungiant ar atjungiant USB diską, tai daro be vartotojo pagalbos.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="768"/>
        <source>Select a distro</source>
        <translation>Pasirinkite distributyvą</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="769"/>
        <source>You must select a distribution to load.</source>
        <translation>Jūs turite pasirinkti distributyvą, kad jį užkrauti.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="783"/>
        <source>Select a disk image file</source>
        <translation>Pasirinkite disko atvaizdo rinkmeną</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="784"/>
        <source>You must select a disk image file to load.</source>
        <translation>Jūs turite pasirinkti disko atvaizdo rinkmeną, kad ją užkrauti.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="798"/>
        <source>Select a kernel and/or initrd file</source>
        <translation>Pasirinkite branduolio ir (arba) initrd rinkmeną (-as)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="799"/>
        <source>You must select a kernel and/or initrd file to load.</source>
        <translation>Jūs turite pasirinkti branduolio ir (arba) initdr rinkmeną(-as), kad ją (jas) užkrauti.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="813"/>
        <source>Diskimage file not found</source>
        <translation>Disko atvaizdo rinkmena nerasta.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="814"/>
        <source>The specified diskimage file %1 does not exist.</source>
        <translation>Nurodyta disko atvaizdo rinkmena %1 neegzistuoja (sugadinta).</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="828"/>
        <source>Kernel file not found</source>
        <translation>Branduolio rinkmena nerasta.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="829"/>
        <source>The specified kernel file %1 does not exist.</source>
        <translation>Nurodyta branduolio rinkmena %1 neegzistuoja (sugadinta).</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="843"/>
        <source>Initrd file not found</source>
        <translation>Initrd rinkmena nerasta.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="844"/>
        <source>The specified initrd file %1 does not exist.</source>
        <translation>Nurodyta Initrd rinkmena %1 neegzistuoja (sugadinta).</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="948"/>
        <source>%1 exists, overwrite?</source>
        <translation>Rinkmena %1 jau egzistuoja, ar norite ją pakeisti?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="949"/>
        <source>The file %1 already exists. Press &apos;Yes to All&apos; to overwrite it and not be prompted again, &apos;Yes&apos; to overwrite files on an individual basis, and &apos;No&apos; to retain your existing version. If in doubt, press &apos;Yes to All&apos;.</source>
        <translation>Rinkmena %1 jau egzistuoja. Spustelėkite &quot;Taip, viską&quot;, kad pakeisti ją (jas) ir toliau daugiau nebeklausti to, &quot;Taip&quot;, kad pakeisti ją (jas) kiekvieną individualiai, arba &quot;Ne&quot;, kad palikti esamą. Jei abejojate, paspauskite &quot;Taip, viską&quot;.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="975"/>
        <source>%1 is out of space, abort installation?</source>
        <translation>%1 trūksta vietos, nutraukti diegimą?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="976"/>
        <source>The directory %1 is out of space. Press &apos;Yes&apos; to abort installation, &apos;No&apos; to ignore this error and attempt to continue installation, and &apos;No to All&apos; to ignore all out-of-space errors.</source>
        <translation>Segtuve %1 trūksta vietos. Spustelėkite &quot;Taip&quot;, kad nutraukti diegimą, &quot;Ne&quot;, kad nekreiptumėte į šį klaidos pranešimą ir pabandytumėte tęsti diegimą, arba &quot;Ne, viskam&quot;, kad ignoruoti visus &quot;trūksta vietos&quot; klaidų pranešimus.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1070"/>
        <source>Locating kernel file in %1</source>
        <translation>Į %1 talpinama branduolio rinkmena</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1121"/>
        <source>Copying kernel file from %1</source>
        <translation>Iš %1 kopijuojama branduolio rinkmena</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1127"/>
        <source>Locating initrd file in %1</source>
        <translation>Į %1 talpinama Initrd rinkmena</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1168"/>
        <source>Copying initrd file from %1</source>
        <translation>Iš %1 kopijuojama Initrd rinkmena</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1174"/>
        <location filename="unetbootin.cpp" line="1254"/>
        <source>Extracting bootloader configuration</source>
        <translation>Išskleidžiama pradinės paleisties programos (bootloader) konfigūracinė rinkmena.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1483"/>
        <location filename="unetbootin.cpp" line="1509"/>
        <source>&lt;b&gt;Extracting compressed iso:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Išskleidžiamas suglaudintas ISO atvaizdas:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1763"/>
        <source>Copying file, please wait...</source>
        <translation>Kopijuojama rinkmena, prašome palaukti...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1764"/>
        <location filename="unetbootin.cpp" line="2578"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;b&gt;Šaltinis:&lt;/b&gt; &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1765"/>
        <location filename="unetbootin.cpp" line="2579"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Paskirtis:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1766"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; 0 bytes</source>
        <translation>&lt;b&gt;Nukopijuota:&lt;/b&gt; 0 baitų</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1804"/>
        <source>Extracting files, please wait...</source>
        <translation>Išskleidžiamos bylos, prašome palaukti...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1805"/>
        <source>&lt;b&gt;Archive:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Archyvas:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1806"/>
        <source>&lt;b&gt;Source:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Šaltinis:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1807"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Paskirtis:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1808"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; 0 of %1 files</source>
        <translation>&lt;b&gt;Išskleista:&lt;/b&gt; 0 of %1 rinkmenų</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1811"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; %1 (%2)</source>
        <translation>&lt;b&gt;Šaltinis:&lt;/b&gt; %1 (%2)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1812"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1%2</source>
        <translation>&lt;b&gt;Paskirtis:&lt;/b&gt; %1%2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1813"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; %1 of %2 files</source>
        <translation>&lt;b&gt;Išskleista:&lt;/b&gt; %1 iš %2 rinkmenų</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2577"/>
        <source>Downloading files, please wait...</source>
        <translation>Parsiunčiamos rinkmenos, prašome palaukti...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2580"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; 0 bytes</source>
        <translation>&lt;b&gt;Parsiųsta:&lt;/b&gt; 0 baitų</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2704"/>
        <source>Download of %1 %2 from %3 failed. Please try downloading the ISO file from the website directly and supply it via the diskimage option.</source>
        <translation>Parsisiųsti %1 %2 iš %3 nepavyko. Prašome pabandyti parsisiųsti ISO rinkmeną tiesiogiai iš svetainės ir pateikti ją kaip disko atvaizdo variantą.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2727"/>
        <location filename="unetbootin.cpp" line="2742"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; %1 of %2</source>
        <translation>&lt;b&gt;Parsiųsta:&lt;/b&gt; %1 iš %2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2757"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; %1 of %2</source>
        <translation>&lt;b&gt;Nukopijuota:&lt;/b&gt; %1 iš %2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2848"/>
        <source>Searching in &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>ieškoma &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2852"/>
        <source>%1/%2 matches in &lt;a href=&quot;%3&quot;&gt;%3&lt;/a&gt;</source>
        <translation>%1/%2 atitikmenų &lt;a href=&quot;%3&quot;&gt;%3&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3125"/>
        <source>%1 not found</source>
        <translation>%1 nerasta</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3126"/>
        <source>%1 not found. This is required for %2 install mode.
Install the &quot;%3&quot; package or your distribution&apos;s equivalent.</source>
        <translation>%1 nerastas. Jis reikalingas %2 įdiegti.
Įdiekite &quot;%3&quot; paketą arba savo distributyvo atitikmenį.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3417"/>
        <source>(Current)</source>
        <translation>(Dabartinis)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3418"/>
        <source>(Done)</source>
        <translation>(Baigta)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3710"/>
        <source>Configuring grub2 on %1</source>
        <translation>%1 grub2 konfigūravimas</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3722"/>
        <source>Configuring grldr on %1</source>
        <translation>%1 grldr konfigūravimas</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3750"/>
        <source>Configuring grub on %1</source>
        <translation>%1 grub konfigūravimas</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4048"/>
        <source>Installing syslinux to %1</source>
        <translation>Į %1 diegiamas syslinux</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4083"/>
        <source>Installing extlinux to %1</source>
        <translation>Į %1 diegiamas extlinux</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4243"/>
        <source>Syncing filesystems</source>
        <translation>Failų sistema sinchronizuojama</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4248"/>
        <source>Setting up persistence</source>
        <translation>Nustatomas gajumas</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4293"/>
        <source>After rebooting, select the </source>
        <translation>Po pakartotinės sistemos paleisties, pasirinkite </translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4298"/>
        <source>After rebooting, select the USB boot option in the BIOS boot menu.%1
Reboot now?</source>
        <translation>Po pakartotinės sistemos paleisties, pasirinkite USB įkrovos parinktį BIOS įkrovos meniu.%1
Dabar pakartotinai paleisti kompiuterį iš naujo?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4301"/>
        <source>The created USB device will not boot off a Mac. Insert it into a PC, and select the USB boot option in the BIOS boot menu.%1</source>
        <translation>Sukurtas USB diskas negali paleisti Mac kompiuterio. Prijunkite jį prie PC mašinos ir pasirinkite USB įkrovos parinktį BIOS įkrovos meniu.%1</translation>
    </message>
    <message>
        <source>
*IMPORTANT* Before rebooting, place an Ubuntu alternate (not desktop) install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.ubuntu.com</source>
        <translation type="obsolete">
*SVARBU* Prieš tai, kad iš naujo paleisti kompiuterį, patalpinkite šakniniame standaus ar USB disko segtuve alternatyvų Ubuntu (Desktop) diegimo ISO disko atvaizdą. Jį galima gauti cdimage.ubuntu.com</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;mirrors.kernel.org&apos; when prompted for a server, and enter &apos;/centos/%1/os/%2&apos; when asked for the folder.</source>
        <translation type="obsolete">
*SVARBU* Po pakartotinės sistemos paleisties, nekreipkite dėmesio į bet kokius klaidų pranešimus ir pasirinkite sugrįžti atgal į CD, nueikite į pagrindinį meniu, parinkite &quot;Pradėti diegimą&quot; parinktį, pažymėkite &quot;Tinklas&quot;, kaip šaltinis, pažymėkite &quot;HTTP&quot;, kaip protokolas, įveskite &quot;mirrors.kernel.org&quot;, kai paprašys nurodyti serverio adresą, ir įveskite &quot;/centos/%1/os/%2&quot;, kai paprašys nurodyti paskirties segtuvą.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* Before rebooting, place a Debian install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.debian.org</source>
        <translation type="obsolete">
*SVARBU* Prieš tai, kad iš naujo paleisti kompiuterį, patalpinkite šakniniame standaus ar USB disko segtuve alternatyvų Debian diegimo ISO disko atvaizdą. Jį galima gauti cdimage.debian.org</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.fedora.redhat.com&apos; when prompted for a server, and enter &apos;/pub/fedora/linux/development/%1/os&apos; when asked for the folder.</source>
        <translation type="obsolete">
*SVARBU* Po pakartotinės sistemos paleisties, nekreipkite dėmesio į bet kokius klaidų pranešimus ir pasirinkite sugrįžti atgal į CD, nueikite į pagrindinį meniu, parinkite &quot;Pradėti diegimą&quot; parinktį, pažymėkite &quot;Tinklas&quot;, kaip šaltinis, pažymėkite &quot;HTTP&quot;, kaip protokolas, įveskite &quot;download.fedora.redhat.com&quot;, kai paprašys nurodyti serverio adresą, ir įveskite &quot;/pub/fedora/linux/development/%1/os&quot;, kai paprašys nurodyti paskirties segtuvą.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.fedora.redhat.com&apos; when prompted for a server, and enter &apos;/pub/fedora/linux/releases/%1/Fedora/%2/os&apos; when asked for the folder.</source>
        <translation type="obsolete">
*SVARBU* Po pakartotinės sistemos paleisties, nekreipkite dėmesio į bet kokius klaidų pranešimus ir pasirinkite sugrįžti atgal į CD, nueikite į pagrindinį meniu, parinkite &quot;Pradėti diegimą&quot; parinktį, pažymėkite &quot;Tinklas&quot;, kaip šaltinis, pažymėkite &quot;HTTP&quot;, kaip protokolas, įveskite &quot;download.fedora.redhat.com&quot;, kai paprašys nurodyti serverio adresą, ir įveskite &quot;/pub/fedora/linux/releases/%1/Fedora/%2/os&quot;, kai paprašys nurodyti paskirties segtuvą.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.opensuse.org&apos; when prompted for a server, and enter &apos;/factory/repo/oss&apos; when asked for the folder.</source>
        <translation type="obsolete">
*SVARBU* Po pakartotinės sistemos paleisties, nekreipkite dėmesio į bet kokius klaidų pranešimus ir pasirinkite sugrįžti atgal į CD, nueikite į pagrindinį meniu, parinkite &quot;Pradėti diegimą&quot; parinktį, pažymėkite &quot;Tinklas&quot;, kaip šaltinis, pažymėkite &quot;HTTP&quot;, kaip protokolas, įveskite &quot;download.opensuse.org&quot;, kai paprašys nurodyti serverio adresą, ir įveskite &quot;/factory/repo/oss&quot;, kai paprašys nurodyti paskirties segtuvą.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.opensuse.org&apos; when prompted for a server, and enter &apos;/distribution/%1/repo/oss&apos; when asked for the folder.</source>
        <translation type="obsolete">
*SVARBU* Po pakartotinės sistemos paleisties, nekreipkite dėmesio į bet kokius klaidų pranešimus ir pasirinkite sugrįžti atgal į CD, nueikite į pagrindinį meniu, parinkite &quot;Pradėti diegimą&quot; parinktį, pažymėkite &quot;Tinklas&quot;, kaip šaltinis, pažymėkite &quot;HTTP&quot;, kaip protokolas, įveskite &quot;download.opensuse.org&quot;, kai paprašys nurodyti serverio adresą, ir įveskite &quot;/distribution/%1/repo/oss&quot;, kai paprašys nurodyti paskirties segtuvą.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="764"/>
        <source>== Select Distribution ==</source>
        <translation>== Pasirinkite Distributyvą ==</translation>
    </message>
    <message>
        <source>== Select Version ==</source>
        <translation type="obsolete">== Pasirinkite Versiją ==</translation>
    </message>
    <message>
        <source>Welcome to &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;, the Universal Netboot Installer. Usage:&lt;ol&gt;&lt;li&gt;Select a distribution and version to download from the list above, or manually specify files to load below.&lt;/li&gt;&lt;li&gt;Select an installation type, and press OK to begin installing.&lt;/li&gt;&lt;/ol&gt;</source>
        <translation type="obsolete">Sveiki atvykę į &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;, Universal Netboot Installer tinklapį. Naudojimas:&lt;ol&gt;&lt;li&gt;Pasirinkite iš sąrašo distribuciją ir versiją, kad ją parsisiųsti, ar pasirinkite įkelti Jūsų nurodytą rinkmeną.&lt;/li&gt;&lt;li&gt;Pažymėkite diegimo būdą ir nuspausdami &quot;Taip&quot;, pradėkite diegimą.&lt;/li&gt;&lt;/ol&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.archlinux.org/&quot;&gt;http://www.archlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Arch Linux is a lightweight distribution optimized for speed and flexibility.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for installation over the internet (FTP).</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.archlinux.org/&quot;&gt;http://www.archlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; Arch Linux yra mažai vietos užimantis distributyvas, greitas ir lankstus.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; Pagal nutylėjimą leidžia diegti tiesiogiai siunčiantis iš interneto (FTP).</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.backtrack-linux.org/&quot;&gt;http://www.backtrack-linux.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; BackTrack is a distribution focused on network analysis and penetration testing.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; BackTrack is booted and run in live mode; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.backtrack-linux.org/&quot;&gt;http://www.backtrack-linux.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; BackTrack, distributyvas, paruoštas tinklo analizei ir skverbčiai testuoti.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; BackTrack paleidžiamas &lt;i&gt;gyvu&lt;/i&gt; būdu; jo naudojimui nereikalingas diegimas.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.centos.org/&quot;&gt;http://www.centos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; CentOS is a free Red Hat Enterprise Linux clone.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.centos.org/&quot;&gt;http://www.centos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; CentOS yra nemokamas Red Hat Enterprise Linux klonas.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; Pagal nutylėjimą leidžia diegti tiesiogiai siunčiantis iš interneto (FTP), arba atsijungus nuo tinklo, prieš tai parsisiuntus ISO diegimo rinkmeną.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://clonezilla.org/&quot;&gt;http://clonezilla.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; CloneZilla is a distribution used for disk backup and imaging.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; CloneZilla is booted and run in live mode; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://clonezilla.org/&quot;&gt;http://clonezilla.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; CloneZilla, tai distributyvas, skirtas disko atsarginių kopijų ir atvaizdų rinkmenų paruošimui.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; CloneZilla  paleidžiamas &lt;i&gt;gyvu&lt;/i&gt; būdu; jo naudojimui nereikalingas diegimas.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://damnsmalllinux.org/&quot;&gt;http://damnsmalllinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Damn Small Linux is a minimalist distribution designed for older computers.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://damnsmalllinux.org/&quot;&gt;http://damnsmalllinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; Damn Small Linux - minimalistinis distributyvas, paruoštas naudoti senuose kompiuteriuose.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; ,i&gt;Gyva&lt;/&gt; versija užkrauna sistemą į RAM ir pasileidžia iš operatyviosios atminties, todėl diegimas nereikalingas, bet įmanoma įdiegti ir į standųjį diską.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.debian.org/&quot;&gt;http://www.debian.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Debian is a community-developed Linux distribution that supports a wide variety of architectures and offers a large repository of packages.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The NetInstall version allows for installation over FTP. If you would like to use a pre-downloaded install iso, use the HdMedia option, and then place the install iso file on the root directory of your hard drive or USB drive</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.debian.org/&quot;&gt;http://www.debian.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; Debian - kūrėjų bendruomenės Linux distributyvas, kurie palaiko didelę architektūrų įvairovę ir turi didelę paketų saugyklą.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; Tinklinė (NetInstall) versija leidžia diegti siunčiantis tiesiai iš FTP. Jei Jūs mėgstate naudotis iš anksto parsisiųsta ISO diegimo rinkmena, pasirinkite HdMedia būdą, ir patalpinkite ISO rinkmeną šakniniame standžiojo ar USB disko segtuve.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.dreamlinux.com.br/&quot;&gt;http://www.dreamlinux.com.br&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Dreamlinux is a user-friendly Debian-based distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.dreamlinux.com.br/&quot;&gt;http://www.dreamlinux.com.br&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; Dreamlinux - tai draugiškas vartotojui, Debian pagrindu paruoštas distributyvas.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; &lt;i&gt;Gyva&lt;/i&gt; versija leidžia pasileisti kompiuterį &lt;i&gt;gyvai&lt;/i&gt;, nediegiant, iš kurio po to galima ir įdiegti į standųjį diską.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freedrweb.com/livecd&quot;&gt;http://www.freedrweb.com/livecd&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Dr.Web AntiVirus is an anti-virus emergency kit to restore a system that broke due to malware.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which malware scans can be launched.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.freedrweb.com/livecd&quot;&gt;http://www.freedrweb.com/livecd&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; Dr.Web AntiVirus yra antivirusinės pagalbos rinkinys, kuris leidžia atstatyti &lt;i&gt;malware&lt;/i&gt; sugadintą sistemą.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; &lt;i&gt;Gyva&lt;/i&gt; versija leidžia paleidus kompiuterį iš laikmenos patikrinti jo standųjį diską dėl kenkėjiškų (malware) programų.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.elivecd.org/&quot;&gt;http://www.elivecd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Elive is a Debian-based distribution featuring the Enlightenment window manager.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.elivecd.org/&quot;&gt;http://www.elivecd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; Elive - tai Debian pagrindu paruoštas distributyvas, kurio ypatybė - Enlightenment langų failų tvarkyklė.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; &lt;i&gt;Gyva&lt;/i&gt; versija leidžia pasileisti kompiuterį &lt;i&gt;gyvai&lt;/i&gt;, nediegiant, iš kurio po to galima ir įdiegti į standųjį diską.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://fedoraproject.org/&quot;&gt;http://fedoraproject.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Fedora is a Red Hat sponsored community distribution which showcases the latest cutting-edge free/open-source software.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://fedoraproject.org/&quot;&gt;http://fedoraproject.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; Fedora - tai Red Hat išlaikomos bendruomenės distributyvas,  kuris naudoja naujausią/pažangiausią nemokamą/atvirojo kodo programinę įrangą.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; &lt;i&gt;Gyva&lt;/i&gt; versija leidžia pasileisti kompiuterį &lt;i&gt;gyvai&lt;/i&gt;, nediegiant, iš kurio po to galima ir įdiegti į standųjį diską. Tinklinė(NetInstall) versija leidžia diegti abiem būdais: tiesiogiai siunčiantis iš interneto (FTP), ar atsijungus, naudojant prieš tai parsisiųstas diegimo ISO rinkmenas.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freebsd.org/&quot;&gt;http://www.freebsd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeBSD is a general-purpose Unix-like operating system designed for scalability and performance.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.freebsd.org/&quot;&gt;http://www.freebsd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; FreeBSD yra bendros paskirties Unix šeimos operacinė sistema, suprojektuota masteliavimui (scalability) ir efektyvumui.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; Galima diegti abiem būdais: tiesiogiai siunčiantis iš interneto (FTP), ar atsijungus, naudojant prieš tai parsisiųstas diegimo ISO rinkmenas.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freedos.org/&quot;&gt;http://www.freedos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeDOS is a free MS-DOS compatible operating system.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; See the &lt;a href=&quot;http://fd-doc.sourceforge.net/wiki/index.php?n=FdDocEn.FdInstall&quot;&gt;manual&lt;/a&gt; for installation details.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.freedos.org/&quot;&gt;http://www.freedos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; FreeDOS yra nemokama MS-DOS suderinama operacinė sistema.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; Daugiau apie diegimą žiūrėkite &lt;a href=&quot;http://fd-doc.sourceforge.net/wiki/index.php?n=FdDocEn.FdInstall&quot;&gt;vadovą&lt;/a&gt;.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://freenas.org/&quot;&gt;http://www.freenas.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeNAS is an embedded open source NAS (Network-Attached Storage) distribution based on FreeBSD.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The LiveCD version creates a RAM drive for FreeNAS, and uses a FAT formatted floppy disk or USB key for saving the configuration file. The embedded version allows installation to hard disk.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://freenas.org/&quot;&gt;http://www.freenas.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; FreeNAS yra integruota atvirojo kodo NAS (Network-Attached Storage) distribucija, kuri remiasi FreeBSD.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; LiveCD versija sukuria RAM diską FreeNAS sistemai ir naudoja FAT failų sistema suformatuotą diskelį ar USB diską, kuriame saugoja konfigūracinę rinkmeną. Integruotoji versija leidžia įdiegti į standųjį diską.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://frugalware.org/&quot;&gt;http://frugalware.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Frugalware is a general-purpose Slackware-based distro for advanced users.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default option allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://frugalware.org/&quot;&gt;http://frugalware.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; Frugalware  - tai bendros paskirties Slackware paremta distribucija pažengusiems vartotojams.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; Galima diegti abiem būdais: tiesiogiai siunčiantis iš interneto (FTP), ar atsijungus, naudojant prieš tai parsisiųstas diegimo ISO rinkmenas.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.geexbox.org/&quot;&gt;http://www.geexbox.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; GeeXboX is an Embedded Linux Media Center Distribution.&lt;br/&gt;</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.geexbox.org/&quot;&gt;http://www.geexbox.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; GeeXboX - tai integruota Linux Media Center Distribucija.&lt;br/&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; gNewSense is an FSF-endorsed distribution based on Ubuntu with all non-free components removed.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; gNewSense - tai FSF patvirtinta distribucija, paremta Ubuntu, iš kurios pašalinti visi mokami komponentai.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; &lt;i&gt;Gyva&lt;/i&gt; (Live) versija leidžia paleisti kompiuterį &lt;i&gt;gyvai&lt;/i&gt;, iš kurios galima paleisti diegimą.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://gujin.sourceforge.net/&quot;&gt;http://gujin.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Gujin is a graphical boot manager which can bootstrap various volumes and files.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Gujin simply boots and runs; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://gujin.sourceforge.net/&quot;&gt;http://gujin.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; Gujin - tai grafinė paleidimo tvarkyklė, kuri paleidžia įvairius disko skirsnius ir rinkmenas.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; Gujin paprastai pasileidžia ir veikia; nereikalauja diegimo.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://ftp.kaspersky.com/devbuilds/RescueDisk/&quot;&gt;http://ftp.kaspersky.com/devbuilds/RescueDisk/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Kaspersky Rescue Disk detects and removes malware from your Windows installation.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which malware scans can be launched.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://ftp.kaspersky.com/devbuilds/RescueDisk/&quot;&gt;http://ftp.kaspersky.com/devbuilds/RescueDisk/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; Kaspersky Rescue Disk aptinka ir pašalina kenkėjiškas (malware) programas Jūsų Windows sistemoje.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; &lt;i&gt;Gyva&lt;/i&gt; versija leidžia pasileisti kompiuterį iš laikmenos ir išvalyti kompiuterį nuo kenkėjiškų (malware) programėlių.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.kubuntu.org/&quot;&gt;http://www.kubuntu.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Kubuntu is an official Ubuntu derivative featuring the KDE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over FTP, and can install Kubuntu and other official Ubuntu derivatives. If you would like to use a pre-downloaded alternate (not desktop) install iso, use the HdMedia option, and then place the alternate install iso file on the root directory of your hard drive or USB drive</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.kubuntu.org/&quot;&gt;http://www.kubuntu.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; Kubuntu - tai oficialus Ubuntu darinys su KDE darbastaliu.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; &lt;i&gt;Gyva&lt;/i&gt; versija leidžia paleisti kompiuterį iš laikmenos ir naudotis sistema, ar iš jos paleisti diegimą. &lt;i&gt;Tinklinė&lt;/i&gt; (NetInstall) versija leidžia diegti tiesiogiai siunčiantis iš interneto (FTP) kaip Kubuntu, taip ir kitus oficialius Ubuntu darinius. Jei Jūs mėgstate diegimui naudoti iš anksto parsiųstas alternatyvias diegimo ISO rinkmenas, pasirinkite HdMedia būdą ir patalpinkite atitinkamą alternatyvią ISO rinkmeną šakniniame standžiojo ar USB disko segtuve.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://linuxconsole.org/&quot;&gt;http://linuxconsole.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; LinuxConsole is a desktop distro to play games, easy to install, easy to use and fast to boot .&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The 1.0.2010 is latest 1.0, now available on rolling release (run liveuptate to update modules and kernel).</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://linuxconsole.org/&quot;&gt;http://linuxconsole.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; LinuxConsole - tai namų kompiuterio distributyvas, skirtas žaisti žaidimus, paprastai įdiegti ir naudoti, bei greitai paleisti kompiuterį .&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; 1.0.2010 versija yra paskutinis 1.0 leidimas, dabar pasiekiamas &lt;i&gt;rolling&lt;/i&gt; leidimas (paleiskite liveupdate, kad atnaujinti modulius ir branduolį).</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://linuxmint.com/&quot;&gt;http://linuxmint.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Linux Mint is a user-friendly Ubuntu-based distribution which includes additional proprietary codecs and other software by default.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://linuxmint.com/&quot;&gt;http://linuxmint.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; Linux Mint yra patogus Ubuntu paremtas distributyvas, pagal nutylėjimą papildytas kitokios nuosavybės kodekais ir kita programine įranga.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; &lt;i&gt;Gyva&lt;/i&gt; versija leidžia pasileisti kompiuterį iš laikmenos ir naudotis sistema, ar iš jos paleisti diegimą į standųjį diską.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.lubuntu.net/&quot;&gt;http://www.lubuntu.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Lubuntu is an official Ubuntu derivative featuring the LXDE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over FTP, and can install Kubuntu and other official Ubuntu derivatives. If you would like to use a pre-downloaded alternate (not desktop) install iso, use the HdMedia option, and then place the alternate install iso file on the root directory of your hard drive or USB drive</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.lubuntu.net/&quot;&gt;http://www.lubuntu.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; Lubuntu yra oficialus Ubuntu darinys su LXDE darbastaliu.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; &lt;i&gt;Gyva&lt;/i&gt; versija leidžia pasileisti kompiuterį iš laikmenos ir naudotis sistema ar iš jos paleisti diegimą į standųjį diską. &lt;i&gt;Tinklinė&lt;/i&gt; (NetInstall) versija leidžia diegti tiesiogiai siunčiantis iš interneto (FTP) kaip Lubuntu, taip ir kitus oficialius Ubuntu darinius. Jei Jūs mėgstate diegimui naudoti iš anksto parsiųstas alternatyvias diegimo ISO rinkmenas, pasirinkite HdMedia būdą ir patalpinkite atitinkamą alternatyvią ISO rinkmeną šakniniame standžiojo ar USB disko segtuve.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.mandriva.com/&quot;&gt;http://www.mandriva.com/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Mandriva is a user-friendly distro formerly known as Mandrake Linux.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over the internet (FTP) or via pre-downloaded &lt;a href=&quot;http://www.mandriva.com/en/download&quot;&gt;&quot;Free&quot; iso image files&lt;/a&gt;.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.mandriva.com/&quot;&gt;http://www.mandriva.com/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; Mandriva yra patogus distributyvas, dar kitaip žinomas kaip Mandrake Linux.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; &lt;i&gt;Gyva&lt;/i&gt; versija leidžia pasileisti kompiuterį iš laikmenos ir naudotis sistema ar iš jos paleisti diegimą į standųjį diską. &lt;i&gt;Tinklinė&lt;/i&gt; (NetInstall) versija leidžia diegti tiesiogiai siunčiantis iš interneto (FTP) ar iš anksto parsisiųsta &lt;a href=&quot;http://www.mandriva.com/en/download&quot;&gt;&quot;Free&quot; iso rinkmena&lt;/a&gt;.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.mepis.org/&quot;&gt;http://www.mepis.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; MEPIS is a Debian-based distribution. SimplyMEPIS is a user-friendly version based on KDE, while AntiX is a lightweight version for older computers.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; MEPIS supports booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.mepis.org/&quot;&gt;http://www.mepis.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; MEPIS yra Debian paremta distribucija. SimplyMEPIS yra vartotojui patogi versija, naudojanti KDE darbastalį, kai AntiX yra nedaug vietos (lengva) versija, skirta seniems kompiuteriams.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; MEPIS leidžia pasileisti kompiuterį iš laikmenos &lt;i&gt;gyvai&lt;/i&gt; ir naudotis sistema, ar iš jos paleisti diegimą į standųjį diską.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://netbootcd.tuxfamily.org/&quot;&gt;http://netbootcd.tuxfamily.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; NetbootCD is a small boot CD that downloads and boots network-based installers for other distributions.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NetbootCD boots and runs in live mode.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://netbootcd.tuxfamily.org/&quot;&gt;http://netbootcd.tuxfamily.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; NetbootCD mažas kompiuterio paleidimo CD, kuris paleidžia sistemą, parsisiunčia ir paleidžia kitų distribucijų tinklinius įdiegėjus.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; NetbootCD pasileidžia ir veikia &lt;i&gt;gyvai&lt;/i&gt; (live mode).</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.nimblex.net/&quot;&gt;http://www.nimblex.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; NimbleX is a small, versatile Slackware-based distribution. It is built using the linux-live scripts, and features the KDE desktop. It can be booted from CD or flash memory (USB pens or MP3 players), and can easily be customized and extended.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NimbleX boots in Live mode.</source>
        <translation type="obsolete">&lt;b&gt;Namų tinklapis:&lt;/b&gt; &lt;a href=&quot;http://www.nimblex.net/&quot;&gt;http://www.nimblex.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Aprašymas:&lt;/b&gt; NimbleX yra maža, universali Slackware paremta distribucija. Ji surinkta naudojant linux-live skriptais ir funkcijonuoja KDE. Ji gali pasileisti iš CD ar flash laikmenos (USB disko ar MP3 grotuvo) ir gali būti lengvai pritaikoma ir praplečiama.&lt;br/&gt;&lt;b&gt;Diegimo pastabos:&lt;/b&gt; NimbleX pasileidžia ir veikia &lt;i&gt;gyvai&lt;/i&gt; (live mode).</translation>
    </message>
</context>
<context>
    <name>unetbootinui</name>
    <message>
        <location filename="unetbootin.ui" line="20"/>
        <source>Unetbootin</source>
        <translation>Unetbootin</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="44"/>
        <location filename="unetbootin.ui" line="65"/>
        <source>Select from a list of supported distributions</source>
        <translation>Pasirinkite iš palaikomų distribucijų sąrašo</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="47"/>
        <source>&amp;Distribution</source>
        <translation>&amp;Distribucija</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="131"/>
        <source>Specify a disk image file to load</source>
        <translation>Nurodykite įkelti disko atvaizdo rinkmeną</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="134"/>
        <source>Disk&amp;image</source>
        <translation>Disko&amp;atvaizdas</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="147"/>
        <source>Manually specify a kernel and initrd to load</source>
        <translation>Patys parinkite įkelti branduolio ir initrd rinkmenas</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="150"/>
        <source>&amp;Custom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="414"/>
        <location filename="unetbootin.ui" line="430"/>
        <source>Space to reserve for user files which are preserved across reboots. Works only for LiveUSBs for Ubuntu and derivatives. If value exceeds drive capacity, the maximum space available will be used.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="417"/>
        <source>Space used to preserve files across reboots (Ubuntu only):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="440"/>
        <source>MB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="503"/>
        <source>OK</source>
        <translation>Gerai</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="506"/>
        <source>Return</source>
        <translation>Grąžinti</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="513"/>
        <source>Cancel</source>
        <translation>Atšaukti</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="516"/>
        <source>Esc</source>
        <translation>Gr(Esc)</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="561"/>
        <source>Reboot Now</source>
        <translation>Tučtuojau paleisti iš naujo</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="568"/>
        <source>Exit</source>
        <translation>Išeiti</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="660"/>
        <source>1. Downloading Files</source>
        <translation>1. Parsiunčiamos rinkmenos</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="667"/>
        <source>2. Extracting and Copying Files</source>
        <translation>2. Išskleidžiamos ir kopijuojamos rinkmenos</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="674"/>
        <source>3. Installing Bootloader</source>
        <translation>3. Diegiama pradinės paleisties programa</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="681"/>
        <source>4. Installation Complete, Reboot</source>
        <translation>4. Diegimas baigtas, perleisti iš naujo</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="477"/>
        <location filename="unetbootin.ui" line="496"/>
        <source>Select the target drive to install to</source>
        <translation>Pasirinkti diegimo paskirties diską</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="480"/>
        <source>Dri&amp;ve:</source>
        <translation>Dis&amp;kas:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="451"/>
        <location filename="unetbootin.ui" line="470"/>
        <source>Select the installation target type</source>
        <translation>Pasirinkti diegimo tipą</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="454"/>
        <source>&amp;Type:</source>
        <translation>&amp;Tipas:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="81"/>
        <source>Select the distribution version</source>
        <translation>Parinkti distributyvo versiją</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="347"/>
        <source>Select disk image file</source>
        <translation>Pasirinkite disko atvaizdo rinkmeną</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="286"/>
        <location filename="unetbootin.ui" line="350"/>
        <location filename="unetbootin.ui" line="375"/>
        <location filename="unetbootin.ui" line="400"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="188"/>
        <source>Select the disk image type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="243"/>
        <source>Specify a floppy/hard disk image, or CD image (ISO) file to load</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="207"/>
        <location filename="unetbootin.ui" line="258"/>
        <source>Specify a kernel file to load</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="283"/>
        <source>Select kernel file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="293"/>
        <location filename="unetbootin.ui" line="312"/>
        <source>Specify an initrd file to load</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="372"/>
        <source>Select initrd file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="397"/>
        <source>Select syslinux.cfg or isolinux.cfg file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="226"/>
        <location filename="unetbootin.ui" line="321"/>
        <source>Specify parameters and options to pass to the kernel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="210"/>
        <source>&amp;Kernel:</source>
        <translation>&amp;Branduolys:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="296"/>
        <source>Init&amp;rd:</source>
        <translation>Init&amp;rd</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="229"/>
        <source>&amp;Options:</source>
        <translation>&amp;Opcijos</translation>
    </message>
</context>
<context>
    <name>uninstaller</name>
    <message>
        <location filename="main.cpp" line="156"/>
        <source>Uninstallation Complete</source>
        <translation>Baigtas išdiegimas</translation>
    </message>
    <message>
        <location filename="main.cpp" line="157"/>
        <source>%1 has been uninstalled.</source>
        <translation>Išdiegtas %1.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="322"/>
        <source>Must run as root</source>
        <translation>Turi būti paleistas administratoriaus teisėmis</translation>
    </message>
    <message>
        <location filename="main.cpp" line="324"/>
        <source>%2 must be run as root. Close it, and re-run using either:&lt;br/&gt;&lt;b&gt;sudo %1&lt;/b&gt;&lt;br/&gt;or:&lt;br/&gt;&lt;b&gt;su - -c &apos;%1&apos;&lt;/b&gt;</source>
        <translation>%2 turi būti paleistas administratoriaus teisėmis. Užverkite ir paleiskite iš naujo, įvesdami:&lt;br/&gt;&lt;b&gt;sudo %1&lt;/b&gt;&lt;br/&gt;arba:&lt;br/&gt;&lt;b&gt;su - -c &apos;%1&apos;&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="361"/>
        <source>%1 Uninstaller</source>
        <translation>%1 išdiegėjas</translation>
    </message>
    <message>
        <location filename="main.cpp" line="362"/>
        <source>%1 is currently installed. Remove the existing version?</source>
        <translation>%1 jau yra įdiegtas. Išdiegti esamą versiją?</translation>
    </message>
</context>
</TS>
