<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="nl_NL">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="266"/>
        <source>LeftToRight</source>
        <translation>LinksNaarRechts</translation>
    </message>
</context>
<context>
    <name>unetbootin</name>
    <message>
        <location filename="unetbootin.cpp" line="213"/>
        <location filename="unetbootin.cpp" line="314"/>
        <location filename="unetbootin.cpp" line="315"/>
        <location filename="unetbootin.cpp" line="384"/>
        <location filename="unetbootin.cpp" line="558"/>
        <location filename="unetbootin.cpp" line="3436"/>
        <location filename="unetbootin.cpp" line="3449"/>
        <location filename="unetbootin.cpp" line="3631"/>
        <location filename="unetbootin.cpp" line="4291"/>
        <source>Hard Disk</source>
        <translation>Harde schijf</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="215"/>
        <location filename="unetbootin.cpp" line="311"/>
        <location filename="unetbootin.cpp" line="312"/>
        <location filename="unetbootin.cpp" line="386"/>
        <location filename="unetbootin.cpp" line="562"/>
        <location filename="unetbootin.cpp" line="728"/>
        <location filename="unetbootin.cpp" line="748"/>
        <location filename="unetbootin.cpp" line="1002"/>
        <location filename="unetbootin.cpp" line="1577"/>
        <location filename="unetbootin.cpp" line="1664"/>
        <location filename="unetbootin.cpp" line="2593"/>
        <location filename="unetbootin.cpp" line="2636"/>
        <location filename="unetbootin.cpp" line="3440"/>
        <location filename="unetbootin.cpp" line="3466"/>
        <location filename="unetbootin.cpp" line="3635"/>
        <location filename="unetbootin.cpp" line="3959"/>
        <location filename="unetbootin.cpp" line="4295"/>
        <source>USB Drive</source>
        <translation>USB-schijf</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="216"/>
        <location filename="unetbootin.cpp" line="233"/>
        <location filename="unetbootin.cpp" line="234"/>
        <location filename="unetbootin.cpp" line="350"/>
        <location filename="unetbootin.cpp" line="676"/>
        <location filename="unetbootin.cpp" line="679"/>
        <location filename="unetbootin.cpp" line="680"/>
        <location filename="unetbootin.cpp" line="3525"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="217"/>
        <location filename="unetbootin.cpp" line="229"/>
        <location filename="unetbootin.cpp" line="230"/>
        <location filename="unetbootin.cpp" line="356"/>
        <location filename="unetbootin.cpp" line="676"/>
        <location filename="unetbootin.cpp" line="684"/>
        <location filename="unetbootin.cpp" line="685"/>
        <location filename="unetbootin.cpp" line="3517"/>
        <source>Floppy</source>
        <translation>Diskette</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="250"/>
        <location filename="unetbootin.cpp" line="256"/>
        <location filename="unetbootin.cpp" line="260"/>
        <location filename="unetbootin.cpp" line="264"/>
        <location filename="unetbootin.cpp" line="268"/>
        <location filename="unetbootin.cpp" line="274"/>
        <location filename="unetbootin.cpp" line="302"/>
        <source>either</source>
        <translation>ofwel</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="281"/>
        <source>LiveUSB persistence</source>
        <translation>LiveUSB-blijvende opslag</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="296"/>
        <source>FAT32-formatted USB drive</source>
        <translation>FAT32-geformatteerde USB-schijf</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="300"/>
        <source>EXT2-formatted USB drive</source>
        <translation>EXT2-geformatteerde USB-schijf</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="676"/>
        <source>Open Disk Image File</source>
        <translation>Schijfimagebestand (iso) openen</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="676"/>
        <source>All Files</source>
        <translation>Alle bestanden</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="694"/>
        <location filename="unetbootin.cpp" line="702"/>
        <location filename="unetbootin.cpp" line="710"/>
        <source>All Files (*)</source>
        <translation>Alle bestanden (*)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="694"/>
        <source>Open Kernel File</source>
        <translation>Kernelbestand openen</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="702"/>
        <source>Open Initrd File</source>
        <translation>initrd-bestand openen</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="710"/>
        <source>Open Bootloader Config File</source>
        <translation>Configuratiebestand voor opstartlader openen</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="732"/>
        <source>Insert a USB flash drive</source>
        <translation>Sluit een USB-staafje aan</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="733"/>
        <source>No USB flash drives were found. If you have already inserted a USB drive, try reformatting it as FAT32.</source>
        <translation>Er zijn geen USB-staafjes gevonden. Als u reeds een USB-staafje hebt aangesloten, probeer dat dan opnieuw als FAT32 te formatteren.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="752"/>
        <source>%1 not mounted</source>
        <translation>%1 niet aangekoppeld</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="753"/>
        <source>You must first mount the USB drive %1 to a mountpoint. Most distributions will do this automatically after you remove and reinsert the USB drive.</source>
        <translation>U moet de USB-schijf %1 eerst koppelen aan een koppelpunt. De meeste Linuxdistributies doen dat automatisch als u de USB-schijf verwijdert en vervolgens opnieuw aansluit.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="768"/>
        <source>Select a distro</source>
        <translation>Selecteer een Linuxdistributie</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="769"/>
        <source>You must select a distribution to load.</source>
        <translation>U dient een Linuxdistributie te selecteren om te laden.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="783"/>
        <source>Select a disk image file</source>
        <translation>Selecteer een schijfimagebestand (iso)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="784"/>
        <source>You must select a disk image file to load.</source>
        <translation>U dient een schijfimagebestand (iso) te selecteren om te laden.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="798"/>
        <source>Select a kernel and/or initrd file</source>
        <translation>Selecteer een kernel- en/of initrd-bestand</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="799"/>
        <source>You must select a kernel and/or initrd file to load.</source>
        <translation>U dient een kernel- en/of initrd-bestand te selecteren om te laden.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="813"/>
        <source>Diskimage file not found</source>
        <translation>Schijfimagebestand niet gevonden</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="814"/>
        <source>The specified diskimage file %1 does not exist.</source>
        <translation>Het opgegeven schijfimagebestand %1 bestaat niet.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="828"/>
        <source>Kernel file not found</source>
        <translation>Kernelbestand niet gevonden</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="829"/>
        <source>The specified kernel file %1 does not exist.</source>
        <translation>Het opgegeven kernelbestand %1 bestaat niet.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="843"/>
        <source>Initrd file not found</source>
        <translation>Initrd-bestand niet gevonden</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="844"/>
        <source>The specified initrd file %1 does not exist.</source>
        <translation>Het opgegeven initrd-bestand %1 bestaat niet.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="948"/>
        <source>%1 exists, overwrite?</source>
        <translation>%1 bestaat al, wilt u het overschrijven?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="949"/>
        <source>The file %1 already exists. Press &apos;Yes to All&apos; to overwrite it and not be prompted again, &apos;Yes&apos; to overwrite files on an individual basis, and &apos;No&apos; to retain your existing version. If in doubt, press &apos;Yes to All&apos;.</source>
        <translation>Het bestand %1 bestaat al. Klik op &apos;Ja op alles&apos; om het te overschrijven en dat in alle volgende conflictsituaties ook te doen, op &apos;Ja&apos; om het te overschrijven en bij een volgend conflict weer te vragen en op &apos;Nee&apos; om de huidige versie te behouden. Als u twijfelt, klik dan op &apos;Ja op alles&apos;, want dat is bijna altijd de beste keuze.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="975"/>
        <source>%1 is out of space, abort installation?</source>
        <translation>Geen ruimte meer op %1, installatie afbreken?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="976"/>
        <source>The directory %1 is out of space. Press &apos;Yes&apos; to abort installation, &apos;No&apos; to ignore this error and attempt to continue installation, and &apos;No to All&apos; to ignore all out-of-space errors.</source>
        <translation>De map %1 heeft geen vrije ruimte meer. Klik op &apos;Ja&apos; om te annuleren, of op &apos;Nee&apos; om deze fout te negeren en toch te proberen om door te gaan. Kies &apos;Nee op alles&apos; om alle toekomstige ruimtetekort-meldingen te negeren.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1070"/>
        <source>Locating kernel file in %1</source>
        <translation>Kernelbestand aan het zoeken in %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1121"/>
        <source>Copying kernel file from %1</source>
        <translation>Kernelbestand aan het kopiëren van %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1127"/>
        <source>Locating initrd file in %1</source>
        <translation>initrd-bestand aan het zoeken in %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1168"/>
        <source>Copying initrd file from %1</source>
        <translation>initrd-bestand aan het kopiëren van %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1174"/>
        <location filename="unetbootin.cpp" line="1254"/>
        <source>Extracting bootloader configuration</source>
        <translation>Configuratie van opstartlader aan het uitpakken</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1483"/>
        <location filename="unetbootin.cpp" line="1509"/>
        <source>&lt;b&gt;Extracting compressed iso:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Ingepakt iso-bestand aan het uitpakken:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1763"/>
        <source>Copying file, please wait...</source>
        <translation>Bestand wordt gekopieerd, even geduld a.u.b...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1764"/>
        <location filename="unetbootin.cpp" line="2578"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;b&gt;Bron:&lt;/b&gt; &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1765"/>
        <location filename="unetbootin.cpp" line="2579"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Bestemming:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1766"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; 0 bytes</source>
        <translation>&lt;b&gt;Gekopieerd:&lt;/b&gt; 0 bytes</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1804"/>
        <source>Extracting files, please wait...</source>
        <translation>Bestanden aan het uitpakken, even geduld a.u.b...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1805"/>
        <source>&lt;b&gt;Archive:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Archief:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1806"/>
        <source>&lt;b&gt;Source:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Bron:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1807"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Bestemming:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1808"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; 0 of %1 files</source>
        <translation>&lt;b&gt;Uitgepakt:&lt;/b&gt; 0 van %1 bestanden</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1811"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; %1 (%2)</source>
        <translation>&lt;b&gt;Bron:&lt;/b&gt; %1 (%2)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1812"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1%2</source>
        <translation>&lt;b&gt;Bestemming:&lt;/b&gt; %1%2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1813"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; %1 of %2 files</source>
        <translation>&lt;b&gt;Uitgepakt:&lt;/b&gt; %1 van %2 bestanden</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2577"/>
        <source>Downloading files, please wait...</source>
        <translation>Bestanden worden opgehaald, een ogenblik geduld a.u.b...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2580"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; 0 bytes</source>
        <translation>&lt;b&gt;Gedownload:&lt;/b&gt; 0 bytes</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2704"/>
        <source>Download of %1 %2 from %3 failed. Please try downloading the ISO file from the website directly and supply it via the diskimage option.</source>
        <translation>Het downloaden van %1 %2 van %3 is mislukt. Probeer a.u.b. om het ISO-bestand rechtstreeks van de website binnen te halen en het te laden via de Schijfimage-optie.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2727"/>
        <location filename="unetbootin.cpp" line="2742"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; %1 of %2</source>
        <translation>&lt;b&gt;Gedownload:&lt;b&gt; %1 van %2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2757"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; %1 of %2</source>
        <translation>&lt;b&gt;Gekopieerd:&lt;/b&gt; %1 of %2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2848"/>
        <source>Searching in &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>Aan het zoeken in &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2852"/>
        <source>%1/%2 matches in &lt;a href=&quot;%3&quot;&gt;%3&lt;/a&gt;</source>
        <translation>%1/%2  overeenkomsten in &lt;a href=&quot;%3&quot;&gt;%3&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3125"/>
        <source>%1 not found</source>
        <translation>%1 niet gevonden</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3126"/>
        <source>%1 not found. This is required for %2 install mode.
Install the &quot;%3&quot; package or your distribution&apos;s equivalent.</source>
        <translation>%1 niet gevonden. Dit is noodzakelijk voor de %2 installatiemodus.
Installeer het pakket &apos;%3&apos; of het equivalent daarvan in uw Linuxdistributie.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3417"/>
        <source>(Current)</source>
        <translation>(Huidig)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3418"/>
        <source>(Done)</source>
        <translation>(Voltooid)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3710"/>
        <source>Configuring grub2 on %1</source>
        <translation>Grub2 instellen op %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3722"/>
        <source>Configuring grldr on %1</source>
        <translation>grldr instellen op %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3750"/>
        <source>Configuring grub on %1</source>
        <translation>Grub instellen op %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4048"/>
        <source>Installing syslinux to %1</source>
        <translation>syslinux installeren op %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4083"/>
        <source>Installing extlinux to %1</source>
        <translation>extlinux installeren op %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4243"/>
        <source>Syncing filesystems</source>
        <translation>Bestandssystemen synchroniseren</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4248"/>
        <source>Setting up persistence</source>
        <translation>Blijvende opslag aan het instellen</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4293"/>
        <source>After rebooting, select the </source>
        <translation>Na herstart: selecteer de </translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4298"/>
        <source>After rebooting, select the USB boot option in the BIOS boot menu.%1
Reboot now?</source>
        <translation>Kies na het herstarten, in het BIOS de opstartoptie &apos;USB boot&apos;. %1
Nu herstarten?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4301"/>
        <source>The created USB device will not boot off a Mac. Insert it into a PC, and select the USB boot option in the BIOS boot menu.%1</source>
        <translation>Het gecreëerde USB-apparaat kan niet op een Mac opstarten. Sluit het aan op een PC, en selecteer de USB-opstartoptie in het opstartmenu van het BIOS.%1</translation>
    </message>
    <message>
        <source>
*IMPORTANT* Before rebooting, place an Ubuntu alternate (not desktop) install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.ubuntu.com</source>
        <translation type="obsolete">
*BELANGRIJK* Plaats voor het herstarten een iso-bestand van Lubuntu Alternate (niet Desktop) in de hoofdmap van uw harde schijf of USB-schijf. Deze iso-bestanden zijn beschikbaar op cdimage.ubuntu.com.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;mirrors.kernel.org&apos; when prompted for a server, and enter &apos;/centos/%1/os/%2&apos; when asked for the folder.</source>
        <translation type="obsolete">
*BELANGRIJK* Negeer na het herstarten mogelijke foutmeldingen en kies &apos;back&apos; als er gevraagd wordt om een CD. Ga dan naar het hoofdmenu, kies de optie &apos;Start installatie&apos;, kies &apos;Netwerk&apos; als bron en kies &apos;HTTP&apos; als protocol. Als er gevraagd wordt om een server voert u &apos;mirrors.kernel.org&apos; in. Voer &apos;/centos/%1/os/%2&apos; in wanneer er gevraagd wordt naar de map.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* Before rebooting, place a Debian install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.debian.org</source>
        <translation type="obsolete">
*BELANGRIJK* Plaats voor het herstarten een Debian-iso-installatiebestand in de hoofdmap van uw harde schijf of USB-schijf. Deze iso-bestanden zijn beschikbaar op cdimage.debian.org</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.fedora.redhat.com&apos; when prompted for a server, and enter &apos;/pub/fedora/linux/development/%1/os&apos; when asked for the folder.</source>
        <translation type="obsolete">
*BELANGRIJK* Negeer na het herstarten mogelijke foutmeldingen en kies &apos;back&apos; als er gevraagd wordt om een CD. Ga dan naar het hoofdmenu, kies de optie &apos;Start installatie&apos;, kies &apos;Netwerk&apos; als bron en kies &apos;HTTP&apos; als protocol. Als er gevraagd wordt om een server voert u &apos;download.fedora.redhat.com&apos; in. Voer &apos;/pub/fedora/linux/development/%1/os&apos; in wanneer er gevraagd wordt naar de map.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.fedora.redhat.com&apos; when prompted for a server, and enter &apos;/pub/fedora/linux/releases/%1/Fedora/%2/os&apos; when asked for the folder.</source>
        <translation type="obsolete">
*BELANGRIJK* Negeer na het herstarten mogelijke foutmeldingen en kies &apos;back&apos; als er gevraagd wordt om een CD. Ga dan naar het hoofdmenu, kies de optie &apos;Start installatie&apos;, kies &apos;Netwerk&apos; als bron en kies &apos;HTTP&apos; als protocol. Als er gevraagd wordt om een server voert u &apos;download.fedora.redhat.com&apos; in. Voer &apos;/pub/fedora/linux/releases/%1/Fedora/%2/os&apos; in wanneer er gevraagd wordt naar de map.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.opensuse.org&apos; when prompted for a server, and enter &apos;/factory/repo/oss&apos; when asked for the folder.</source>
        <translation type="obsolete">
*BELANGRIJK* Negeer na het herstarten mogelijke foutmeldingen en kies &apos;back&apos; als er gevraagd wordt om een CD. Ga dan naar het hoofdmenu, kies de optie &apos;Start installatie&apos;, kies &apos;Netwerk&apos; als bron en kies &apos;HTTP&apos; als protocol. Als er gevraagd wordt om een server voert u &apos;download.opensuse.org&apos; in. Voer &apos;/factory/repo/oss&apos; in wanneer er gevraagd wordt naar de map.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.opensuse.org&apos; when prompted for a server, and enter &apos;/distribution/%1/repo/oss&apos; when asked for the folder.</source>
        <translation type="obsolete">
*BELANGRIJK* Negeer na het herstarten mogelijke foutmeldingen en kies &apos;back&apos; als er gevraagd wordt om een CD. Ga dan naar het hoofdmenu, kies de optie &apos;Start installatie&apos;, kies &apos;Netwerk&apos; als bron en kies &apos;HTTP&apos; als protocol. Als er gevraagd wordt om een server voert u &apos;download.opensuse.org&apos; in. Voer &apos;/distribution/%1/repo/oss&apos; in wanneer er gevraagd wordt naar de map.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="764"/>
        <source>== Select Distribution ==</source>
        <translation>== Selecteer Linuxdistributie ==</translation>
    </message>
    <message>
        <source>== Select Version ==</source>
        <translation type="obsolete">== Selecteer versie ==</translation>
    </message>
    <message>
        <source>Welcome to &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;, the Universal Netboot Installer. Usage:&lt;ol&gt;&lt;li&gt;Select a distribution and version to download from the list above, or manually specify files to load below.&lt;/li&gt;&lt;li&gt;Select an installation type, and press OK to begin installing.&lt;/li&gt;&lt;/ol&gt;</source>
        <translation type="obsolete">Welkom bij &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;, de Universele Netboot Installeerder. Gebruik:&lt;ol&gt;&lt;li&gt;selecteer een Linuxdistributie en versie om te downloaden in de lijst hierboven, of kies hier beneden handmatig bestanden om te laden.&lt;/li&gt;&lt;li&gt;Kies een installatietype en klik op OK om te beginnen met installeren.&lt;/li&gt;&lt;/ol&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.archlinux.org/&quot;&gt;http://www.archlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Arch Linux is a lightweight distribution optimized for speed and flexibility.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for installation over the internet (FTP).</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.archlinux.org/&quot;&gt;http://www.archlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Arch Linux is een lichtgewichtdistributie waarbij de nadruk ligt op optimalisatie voor snelheid en flexibiliteit.&lt;br/&gt;&lt;b&gt;Installatie-opmerkingen:&lt;/b&gt; De standaardversie biedt de mogelijkheid om te installeren via internet (FTP).</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.backtrack-linux.org/&quot;&gt;http://www.backtrack-linux.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; BackTrack is a distribution focused on network analysis and penetration testing.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; BackTrack is booted and run in live mode; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.backtrack-linux.org/&quot;&gt;http://www.backtrack-linux.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; BackTrack is een besturingssysteem dat volledig in het teken staat van netwerkanalyse en veiligheidsproeven.&lt;br/&gt;&lt;b&gt;Installatie-opmerkingen:&lt;/b&gt; BackTrack start op in de live-modus; u hoeft niets te installeren om hem te gebruiken.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.centos.org/&quot;&gt;http://www.centos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; CentOS is a free Red Hat Enterprise Linux clone.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.centos.org/&quot;&gt;http://www.centos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt;CentOS is een gratis kloon van Red Hat Enterprise Linux.&lt;br/&gt;&lt;b&gt;Installatie-opmerkingen:&lt;/b&gt; de standaardversie kan worden gebruikt voor een installatie over FTP of via vooraf opgehaalde ISO-bestanden.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://clonezilla.org/&quot;&gt;http://clonezilla.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; CloneZilla is a distribution used for disk backup and imaging.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; CloneZilla is booted and run in live mode; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://clonezilla.org/&quot;&gt;http://clonezilla.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; CloneZilla is een distributie die wordt gebruikt om reservekopieën te maken van schijven.&lt;br/&gt;&lt;b&gt;Installatie-opmerkingen:&lt;/b&gt; CloneZilla wordt opgestart en uitgevoerd in live-modus; er is geen installatie nodig om hem te gebruiken.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://damnsmalllinux.org/&quot;&gt;http://damnsmalllinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Damn Small Linux is a minimalist distribution designed for older computers.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://damnsmalllinux.org/&quot;&gt;http://damnsmalllinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Damn Small Linux is een superlichtgewichtdistributie, ontworpen voor oudere computers.&lt;br/&gt;&lt;b&gt;Installatie-opmerkingen:&lt;/b&gt; de Live-versie laadt het gehele systeem in het RAM-geheugen en start daarvan op. Installatie is dus niet noodzakelijk maar optioneel.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.debian.org/&quot;&gt;http://www.debian.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Debian is a community-developed Linux distribution that supports a wide variety of architectures and offers a large repository of packages.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The NetInstall version allows for installation over FTP. If you would like to use a pre-downloaded install iso, use the HdMedia option, and then place the install iso file on the root directory of your hard drive or USB drive</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.debian.org/&quot;&gt;http://www.debian.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Debian is een door haar gemeenschap ontwikkelde Linuxdistributie die uiteenlopende architecturen (zoals 32-bit en 64-bit) ondersteunt. Verder beschikt zij over pakketbronnen met zeer veel programmatuur.&lt;br/&gt;&lt;b&gt;Installatie-opmerkingen:&lt;/b&gt; Met de Netinstall-versie kunt u via FTP installeren. Wilt u al op voorhand over de benodigde installatiebronnen beschikken? Gebruik dan een ISO-bestand met de Hdmedia-optie, en plaats het in de hoofdmap van uw harde schijf of USB-schijf.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.dreamlinux.com.br/&quot;&gt;http://www.dreamlinux.com.br&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Dreamlinux is a user-friendly Debian-based distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.dreamlinux.com.br/&quot;&gt;http://www.dreamlinux.com.br&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt;Dreamlinux is een gebruiksvriendelijke op Debian gebaseerde distributie.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Met de Live-versie kunt u opstarten naar Live-modus, van waaruit de installatie eventueel gestart kan worden.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freedrweb.com/livecd&quot;&gt;http://www.freedrweb.com/livecd&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Dr.Web AntiVirus is an anti-virus emergency kit to restore a system that broke due to malware.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which malware scans can be launched.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.freedrweb.com/livecd&quot;&gt;http://www.freedrweb.com/livecd&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Dr. Web AntiVirus is een antivirus-noodreparatiepakket om een systeem dat kapot ging door kwaadaardige programma&apos;s, te herstellen.&lt;br/&gt;&lt;b&gt;Installatie-opmerkingen:&lt;/b&gt; Met de Live-versie kunt u opstarten naar Live-modus, van waaruit het zoeken naar kwaadaardige programma&apos;s kan worden uitgevoerd.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.elivecd.org/&quot;&gt;http://www.elivecd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Elive is a Debian-based distribution featuring the Enlightenment window manager.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.elivecd.org/&quot;&gt;http://www.elivecd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Elive is gebaseerd op Debian en heeft de Enlightenment-vensterbeheerder aan boord .&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Met de Live-versie kunt u opstarten naar Live-modus, van waaruit de installatie eventueel gestart kan worden.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://fedoraproject.org/&quot;&gt;http://fedoraproject.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Fedora is a Red Hat sponsored community distribution which showcases the latest cutting-edge free/open-source software.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://fedoraproject.org/&quot;&gt;http://fedoraproject.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Fedora is een door Red Hat gesponsorde gemeenschapsdistributie waarin de meest recente open-bronprogramma&apos;s zijn opgenomen.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Met de Live-versie kunt u opstarten naar Live-modus, van waaruit de installatie eventueel gestart kan worden. Met de NetInstall-versie kunt u zowel via FTP als met vooraf opgehaalde ISO-bestanden het systeem installeren.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freebsd.org/&quot;&gt;http://www.freebsd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeBSD is a general-purpose Unix-like operating system designed for scalability and performance.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.freebsd.org/&quot;&gt;http://www.freebsd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; FreeBSD is een Unix-besturingssysteem voor algemeen gebruik waarin prestaties en uitbreidbaarheid/schaalbaarheid centraal staan.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; De standaardversie kan zowel via het internet (FTP) als met vooraf opgehaalde ISO-bestanden worden geïnstalleerd.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freedos.org/&quot;&gt;http://www.freedos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeDOS is a free MS-DOS compatible operating system.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; See the &lt;a href=&quot;http://fd-doc.sourceforge.net/wiki/index.php?n=FdDocEn.FdInstall&quot;&gt;manual&lt;/a&gt; for installation details.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.freedos.org/&quot;&gt;http://www.freedos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; FreeDOS is een gratis MS-DOS-compatibel besturingssysteem.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Zie de &lt;a href=&quot;http://fd-doc.sourceforge.net/wiki/index.php?n=FdDocEn.FdInstall&quot;&gt;gebruiksaanwijzing&lt;/a&gt; voor bijzonderheden over de installatie.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://freenas.org/&quot;&gt;http://www.freenas.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeNAS is an embedded open source NAS (Network-Attached Storage) distribution based on FreeBSD.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The LiveCD version creates a RAM drive for FreeNAS, and uses a FAT formatted floppy disk or USB key for saving the configuration file. The embedded version allows installation to hard disk.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://freenas.org/&quot;&gt;http://www.freenas.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; FreeNAS is een geïntegreerde open-bron NAS (Network-Attached Storage)-distributie gebaseerd op FreeBSD.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; De Live-cd-versie maakt een RAM-schijf aan voor FreeNAS en gebruikt een FAT-geformatteerde diskette of USB-staafje om het configuratiebestand op te slaan. Met de zogeheten &apos;ingebedde&apos; versie kunt u FreeNAS installeren op uw harde schijf.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://frugalware.org/&quot;&gt;http://frugalware.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Frugalware is a general-purpose Slackware-based distro for advanced users.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default option allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://frugalware.org/&quot;&gt;http://frugalware.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Frugalware is een op Slackware gebaseerde distributie voor meer geavanceerde gebruikers.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt;De standaardoptie biedt de mogelijkheid om te installeren via het internet (FTP) of via vooraf opgehaalde ISO-bestanden.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.geexbox.org/&quot;&gt;http://www.geexbox.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; GeeXboX is an Embedded Linux Media Center Distribution.&lt;br/&gt;</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.geexbox.org/&quot;&gt;http://www.geexbox.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; GeeXboX is een &apos;ingebedde&apos; Linux-mediacentrumdistributie.&lt;br/&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; gNewSense is an FSF-endorsed distribution based on Ubuntu with all non-free components removed.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; gNewSense is een door de Free Software Foundation ondersteunde distributie, gebaseerd op Ubuntu met uitsluiten vrije onderdelen.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt;Met de Live-versie kunt u opstarten naar Live-modus, van waaruit eventueel de installatie kan worden gestart.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://gujin.sourceforge.net/&quot;&gt;http://gujin.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Gujin is a graphical boot manager which can bootstrap various volumes and files.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Gujin simply boots and runs; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://gujin.sourceforge.net/&quot;&gt;http://gujin.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Gujin is een grafische opstartbeheerder waarmee verschillende schijven en bestanden kunnen worden opgestart.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Gujin start gewoon op en doet het meteen, er is geen installatie benodigd om hem te gebruiken.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://ftp.kaspersky.com/devbuilds/RescueDisk/&quot;&gt;http://ftp.kaspersky.com/devbuilds/RescueDisk/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Kaspersky Rescue Disk detects and removes malware from your Windows installation.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which malware scans can be launched.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://ftp.kaspersky.com/devbuilds/RescueDisk/&quot;&gt;http://ftp.kaspersky.com/devbuilds/RescueDisk/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Kaspersky Rescue Disk detecteert en verwijdert kwaadaardige programma&apos;s in uw Windows-installatie.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Met de Live-versie kunt u opstarten naar Live-modus, van waaruit gezocht kan worden naar kwaadaardige programma&apos;s.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.kubuntu.org/&quot;&gt;http://www.kubuntu.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Kubuntu is an official Ubuntu derivative featuring the KDE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over FTP, and can install Kubuntu and other official Ubuntu derivatives. If you would like to use a pre-downloaded alternate (not desktop) install iso, use the HdMedia option, and then place the alternate install iso file on the root directory of your hard drive or USB drive</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.kubuntu.org/&quot;&gt;http://www.kubuntu.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Kubuntu is een officieel lid van de Ubuntufamilie en beschikt over de KDE-werkomgeving.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Met de Live-versie kunt u opstarten naar Live-modus, van waaruit de installatie eventueel kan worden gestart . Met de NetInstall-versie kunt u Kubuntu installeren (en ook andere officiële familieleden van Ubuntu). Wilt u een ISO-installatiebestand (geen desktopversie) gebruiken dat u op voorhand hebt binnengehaald? Gebruik daarvoor de HdMedia-optie en plaats de zogeheten &quot;alternate install&quot;-ISO in de hoofdmap van uw harde schijf of USB-schijf.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://linuxconsole.org/&quot;&gt;http://linuxconsole.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; LinuxConsole is a desktop distro to play games, easy to install, easy to use and fast to boot .&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The 1.0.2010 is latest 1.0, now available on rolling release (run liveuptate to update modules and kernel).</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://linuxconsole.org/&quot;&gt;http://linuxconsole.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; LinuxConsole is een bureaubladdistributie om spellen te spelen. Hij is makkelijk in gebruik en installatie en start snel op.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Versie 1.0.2010 is de nieuwste 1.0, nu beschikbaar als &apos;rollende uitgave&apos; (gebruik live-update om modules en de kernel bij te werken).</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://linuxmint.com/&quot;&gt;http://linuxmint.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Linux Mint is a user-friendly Ubuntu-based distribution which includes additional proprietary codecs and other software by default.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://linuxmint.com/&quot;&gt;http://linuxmint.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Linux Mint is een gebruiksvriendelijke op Ubuntu gebaseerde distributie, waarbij standaard codecs en gesloten software worden meegeleverd.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Met de Live-versie kunt u opstarten naar Live-modus, van waaruit de installatie eventueel kan worden gestart.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.lubuntu.net/&quot;&gt;http://www.lubuntu.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Lubuntu is an official Ubuntu derivative featuring the LXDE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over FTP, and can install Kubuntu and other official Ubuntu derivatives. If you would like to use a pre-downloaded alternate (not desktop) install iso, use the HdMedia option, and then place the alternate install iso file on the root directory of your hard drive or USB drive</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.lubuntu.net/&quot;&gt;http://www.lubuntu.net&lt;/a&gt;&lt;br&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Lubuntu is een officieel familielid van Ubuntu met de LXDE-werkomgeving.&lt;br&gt;&lt;b&gt;Installatie-opmerkingen:&lt;/b&gt; De Live-versie staat opstarten in Live-modus toe, van waaruit desgewenst de installatie kan worden gestart. De NetInstall-versie staat installatie via FTP toe, en kan Kubuntu en andere officiële familieleden van Ubuntu installeren. Als u een eerder opgehaalde, alternatieve ISO wilt gebruiken, gebruik dan de HdMedia-optie, en plaats de alternatieve ISO in de hoofdmap van uw harde schijf of USB-schijf.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.mandriva.com/&quot;&gt;http://www.mandriva.com/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Mandriva is a user-friendly distro formerly known as Mandrake Linux.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over the internet (FTP) or via pre-downloaded &lt;a href=&quot;http://www.mandriva.com/en/download&quot;&gt;&quot;Free&quot; iso image files&lt;/a&gt;.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.mandriva.com/&quot;&gt;http://www.mandriva.com/&lt;/a&gt;&lt;br&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Mandriva is een gebruiksvriendelijke distributie, voorheen bekend als Mandrake Linux.&lt;br&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Met de Live-versie kunt u opstarten naar Live-modus, van waaruit de de installatie eventueel kan worden gestart. Met de Netinstall-versie kan via het internet (FTP) worden geïnstalleerd, of via vooraf opgehaalde &lt;a href=&quot;http://www.mandriva.com/en/download&quot;&gt;&quot;vrije&quot; ISO-bestanden&lt;/a&gt;.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.mepis.org/&quot;&gt;http://www.mepis.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; MEPIS is a Debian-based distribution. SimplyMEPIS is a user-friendly version based on KDE, while AntiX is a lightweight version for older computers.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; MEPIS supports booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.mepis.org/&quot;&gt;http://www.mepis.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; MEPIS is een op Debian gebaseerde distributie. SimplyMEPIS is een gebruiksvriendelijke versie gebaseerd op KDE, en AntiX is een lichte versie voor oudere computers.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; MEPIS ondersteunt opstarten naar Live-modus, van waaruit eventueel de installatie kan worden gestart.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://netbootcd.tuxfamily.org/&quot;&gt;http://netbootcd.tuxfamily.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; NetbootCD is a small boot CD that downloads and boots network-based installers for other distributions.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NetbootCD boots and runs in live mode.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://netbootcd.tuxfamily.org/&quot;&gt;http://netbootcd.tuxfamily.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; NetbootCD is een kleine opstartbare CD die netwerkinstallaties van andere besturingssystemen kan downloaden en starten.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; NetbootCD start en werkt in Live-modus.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.nimblex.net/&quot;&gt;http://www.nimblex.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; NimbleX is a small, versatile Slackware-based distribution. It is built using the linux-live scripts, and features the KDE desktop. It can be booted from CD or flash memory (USB pens or MP3 players), and can easily be customized and extended.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NimbleX boots in Live mode.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.nimblex.net/&quot;&gt;http://www.nimblex.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Nimblex is een kleine en veelzijdige distributie die is gebaseerd op Slackware. Hij is gebouwd met de linux-live scripts, and heeft de KDE-werkomgeving. Opstarten kan vanaf cd of flashgeheugen (USB-staafjes of MP3-spelers). Verder kan Nimblex gemakkelijk aangepast en uitgebreid worden.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; NimbleX start op in de live-modus.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://pogostick.net/~pnh/ntpasswd/&quot;&gt;http://pogostick.net/~pnh/ntpasswd/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; The Offline NT Password and Registry Editor can reset Windows passwords and edit the registry on Windows 2000-Vista.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NTPasswd is booted and run in live mode; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://pogostick.net/~pnh/ntpasswd/&quot;&gt;http://pogostick.net/~pnh/ntpasswd/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; De Offline NT Password and Registry Editor kan Windows-wachtwoorden opnieuw instellen en het register wijzigen in Windows 2000-Vista.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; NTPasswd start en werkt in Live-modus, er is geen installatie nodig om hem te gebruiken.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.opensuse.org/&quot;&gt;http://www.opensuse.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; openSUSE is a user-friendly Novell sponsored distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.opensuse.org/&quot;&gt;http://www.opensuse.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; openSUSE is een gebruiksvriendelijke distributie gesponsord door SUSE.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Met de standaardversie kan over het internet alsook met vooraf opgehaalde ISO-bestanden worden geïnstalleerd.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://ophcrack.sourceforge.net/&quot;&gt;http://ophcrack.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Ophcrack can crack Windows passwords.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Ophcrack is booted and run in live mode; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://ophcrack.sourceforge.net/&quot;&gt;http://ophcrack.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Ophcrack kan Windows-wachtwoorden kraken.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Ophcrack wordt naar een live-modus opgestart en hoeft niet te worden geïnstalleerd om hem te gebruiken.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://partedmagic.com/&quot;&gt;http://partedmagic.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Parted Magic includes the GParted partition manager and other system utilities which can resize, copy, backup, and manipulate disk partitions.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Parted Magic is booted and run in live mode; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://partedmagic.com/&quot;&gt;http://partedmagic.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Parted Magic bevat de GParted partitiebeheerder en andere systeembenodigdheden waarmee schijfpartities kunnen worden vergroot/verkleind, gekopieerd en aangepast.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Parted Magic wordt naar een live-modus opgestart en hoeft niet te worden geïnstalleerd om hem te gebruiken.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.puppylinux.com/&quot;&gt;http://www.puppylinux.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Puppy Linux is a lightweight distribution designed for older computers.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.puppylinux.com/&quot;&gt;http://www.puppylinux.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Puppy Linux is een lichte distributie ontworpen voor oudere computers.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; De live-versie laadt het gehele systeem in het RAM-geheugen en start daarvan op. Installatie is niet noodzakelijk maar optioneel.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.sabayonlinux.org/&quot;&gt;http://www.sabayonlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Sabayon Linux is a Gentoo-based Live DVD distribution which features the Entropy binary package manager in addition to the source-based Portage.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The LiteMCE edition is 2 GB, while the full edition will need an 8 GB USB drive</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.sabayonlinux.org/&quot;&gt;http://www.sabayonlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beschrijving:&lt;/b&gt; Sabayon Linux is een op Gentoo gebaseerde LiveDVD-distributie waarin de binaire pakketbeheerder Entropy is opgenomen naast het brongebaseerde Portage.&lt;br/&gt;&lt;b&gt;Opmerkingen m.b.t. installatie:&lt;/b&gt; Met de Live-versie kan in Live-modus worden opgestart van waaruit eventueel de installatie kan worden gestart. De LiteMCE editie is 2 GB, en de volledige editie heeft een 8GB USB drive nodig.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://salixos.org&quot;&gt;http://salixos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Salix is a GNU/Linux distribution based on Slackware (fully compatible) that is simple, fast and easy to use.&lt;br/&gt;Like a bonsai, Salix is small, light &amp; the product of infinite care.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.&lt;br/&gt;Default root password is &lt;b&gt;live&lt;/b&gt;.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://salixos.org&quot;&gt;http://salixos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Salix is een GNU/Linux distributie gebaseerd op (en geheel compatibel met) Slackware die simpel, snel en makkelijk te gebruiken is.&lt;br/&gt;Net als een bonsai is Salis klein, lichtgewicht en het resultaat van onbeperkte aandacht.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Met de Live-versie kunt u opstarten naar Live-modus, van waaruit de installatie eventueel kan worden gestart.&lt;br/&gt;Het standaardwachtwoord voor de rootgebruiker is &lt;b&gt;live&lt;/b&gt;.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.slax.org/&quot;&gt;http://www.slax.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Slax is a Slackware-based distribution featuring the KDE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.slax.org/&quot;&gt;http://www.slax.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beschrijving:&lt;/b&gt; Slax is een op Slackware gebaseerde distributie met de KDE desktop.&lt;br/&gt;&lt;b&gt;Opmerkingen m.b.t. installatie:&lt;/b&gt;Met de Live versie kan in Live modus worden opgestart vanaf waar eventueel de installatie kan worden gestart.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.slitaz.org/en/&quot;&gt;http://www.slitaz.org/en&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; SliTaz is a lightweight, desktop-oriented micro distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.slitaz.org/en/&quot;&gt;http://www.slitaz.org/en&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beschrijving:&lt;/b&gt; SliTaz is een lichte, bureaublad-georiënteerde microdistributie.&lt;br/&gt;&lt;b&gt;Opmerkingen m.b.t. installatie:&lt;/b&gt;Het hele systeem laadt in het RAM-geheugen en start daarvan op. Installatie is niet noodzakelijk maar optioneel.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://btmgr.sourceforge.net/about.html&quot;&gt;http://btmgr.sourceforge.net/about.html&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Smart Boot Manager is a bootloader which can overcome some boot-related BIOS limitations and bugs.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; SBM simply boots and runs; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://btmgr.sourceforge.net/about.html&quot;&gt;http://btmgr.sourceforge.net/about.html&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beschrijving:&lt;/b&gt; Smart Boot Manager is een opstartlader die bepaalde opstart-gerelateerde BIOS-beperkingen en -fouten kan omzeilen.&lt;br/&gt;&lt;b&gt;Opmerkingen m.b.t. installatie:&lt;/b&gt; SBM laadt in het RAM-geheugen en start daarvan op.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.supergrubdisk.org&quot;&gt;http://www.supergrubdisk.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Super Grub Disk is a bootloader which can perform a variety of MBR and bootloader recovery tasks.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; SGD simply boots and runs; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.supergrubdisk.org&quot;&gt;http://www.supergrubdisk.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beschrijving:&lt;/b&gt; Super Grub Disk is een opstartlader die verschillende hersteltaken kan uitvoeren voor MBR en opstartlader.&lt;br/&gt;&lt;b&gt;Opmerkingen m.b.t. installatie:&lt;/b&gt; SGD start gewoon op en werkt meteen, installatie is niet benodigd.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://hacktolive.org/wiki/Super_OS&quot;&gt;http://hacktolive.org/wiki/Super_OS&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Super OS is an unofficial derivative of Ubuntu which includes additional software by default. Requires a 2GB USB drive to install.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://hacktolive.org/wiki/Super_OS&quot;&gt;http://hacktolive.org/wiki/Super_OS&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Super OS is onofficieel afgeleid van Ubuntu en beschikt standaard over extra software. Een USB-schijf van 2GB is een vereiste.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Met de live-versie kunt u in de live-modus opstarten, en dan kan de installatie eventueel worden opgestart.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.ubuntu.com/&quot;&gt;http://www.ubuntu.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Ubuntu is a user-friendly Debian-based distribution. It is currently the most popular Linux desktop distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over FTP, and can install Kubuntu and other official Ubuntu derivatives. If you would like to use a pre-downloaded alternate (not desktop) install iso, use the HdMedia option, and then place the alternate install iso file on the root directory of your hard drive or USB drive</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.ubuntu.com/&quot;&gt;http://www.ubuntu.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Ubuntu is een gebruiksvriendelijke, op Debian gebaseerde distributie. Momenteel is het de meest populaire distributie.&lt;br/&gt;&lt;b&gt;Installatieopmerkingen:&lt;/b&gt; Met de live-versie kunt u opstarten in de live-modus, en van daaruit kan de installatie eventueel worden opgestart. Met de NetInstall-versie kunt u Kubuntu en andere officiële afgeleide distributies installeren via FTP. Gebruik de HdMedia-optie als u een zogeheten &quot;alternate install&quot;-ISO-installatiebestand (niet te verwarren met de desktopvariant) wil gebruiken dat u al op voorhand hebt gedownload. Plaats het ISO-installatiebestand in de hoofdmap van uw harde schijf of USB-schijf.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.xpud.org/&quot;&gt;http://www.xpud.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; xPUD is a lightweight distribution featuring a simple kiosk-like interface with a web browser and media player.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.xpud.org/&quot;&gt;http://www.xpud.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beschrijving:&lt;/b&gt; xPUD is een lichtgewicht-distributie met een simpele kiosk-achtige bedieningsschil inclusief een webbrowser en een mediaspeler.&lt;br/&gt;&lt;b&gt;Installatie-opmerkingen:&lt;/b&gt; De Live-versie laad het gehele systeem in het RAM-geheugen en start op uit dat geheugen.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.xubuntu.org/&quot;&gt;http://www.xubuntu.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Xubuntu is an official Ubuntu derivative featuring the XFCE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over FTP, and can install Kubuntu and other official Ubuntu derivatives. If you would like to use a pre-downloaded alternate (not desktop) install iso, use the HdMedia option, and then place the alternate install iso file on the root directory of your hard drive or USB drive</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.xubuntu.org/&quot;&gt;http://www.xubuntu.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Xubuntu is een officiële variant van Ubuntu met de XFCE-werkomgeving aan boord.&lt;br/&gt;&lt;b&gt;Installatie-opmerkingen:&lt;/b&gt; Met de live-versie kunt u opstarten in de live-modus, en van daaruit kan de installatie eventueel worden opgestart. Met de NetInstall-versie kunt u Kubuntu en andere officiële afgeleide distributies installeren via FTP. Gebruik de HdMedia-optie als u een zogeheten &quot;alternate install&quot;-ISO-installatiebestand (niet te verwarren met de desktopvariant) wil gebruiken dat u al op voorhand hebt gedownload. Plaats het ISO-installatiebestand in de hoofdmap van uw harde schijf of USB-schijf.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.zenwalk.org/&quot;&gt;http://www.zenwalk.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Zenwalk is a Slackware-based distribution featuring the XFCE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.zenwalk.org/&quot;&gt;http://www.zenwalk.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beschrijving:&lt;/b&gt; Zenwalk is een op Slackware gebaseerde distributie met daarin de werkomgeving XFCE.&lt;br/&gt;&lt;b&gt;Opmerkingen m.b.t. installatie:&lt;/b&gt; Met de Live-versie kan in Live-modus worden opgestart van waaruit eventueel de installatie kan worden gestart.</translation>
    </message>
    <message>
        <source>&lt;img src=&quot;:/eeepclos.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.eeepclinuxos.com/&quot;&gt;http://www.eeepclinuxos.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; EeePCLinuxOS is a user-friendly PCLinuxOS based distribution for the EeePC.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Make sure install media is empty and formatted before proceeding with install.</source>
        <translation type="obsolete">&lt;img src=&quot;:/eeepclos.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.eeepclinuxos.com/&quot;&gt;http://www.eeepclinuxos.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beschrijving:&lt;/b&gt; EeePCLinuxOS is een gebruiksvriendelijke distributie voor de eeepc gebaseerd op  PCLinuxOS&lt;br/&gt;&lt;b&gt;Opmerkingen m.b.t. installatie:&lt;/b&gt; zorg ervoor dat het installatiemedium leeg en geformatteerd is voor u doorgaat met de installatie.</translation>
    </message>
    <message>
        <source>&lt;img src=&quot;:/eeeubuntu.png&quot; style=&quot;float:left;&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.ubuntu-eee.com/&quot;&gt;http://www.ubuntu-eee.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Ubuntu Eee is not only Ubuntu optimized for the Asus Eee PC. It&apos;s an operating system, using the Netbook Remix interface, which favors the best software available instead of open source alternatives (ie. Skype instead of Ekiga).&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Make sure install media is empty and formatted before proceeding with install.</source>
        <translation type="obsolete">&lt;img src=&quot;:/eeeubuntu.png&quot; style=&quot;float:left;&quot; /&gt;&lt;br/&gt;&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.ubuntu-eee.com/&quot;&gt;http://www.ubuntu-eee.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beschrijving:&lt;/b&gt; Ubuntu Eee is niet alleen Ubuntu geoptimaliseerd voor de Asus Eee PC. Het is een besturingssysteem met de Netbook Remix-gebruikersomgeving. De voorkeur wordt gegeven aan de beste programmatuur, in plaats van vast te houden aan openbron-alternatieven.&lt;br/&gt;&lt;b&gt;Opmerkingen m.b.t. installatie:&lt;/b&gt; zorg ervoor dat het installatiemedium leeg en geformatteerd is voor u doorgaat met de installatie.</translation>
    </message>
    <message>
        <source>&lt;img src=&quot;:/elive.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.elivecd.org/&quot;&gt;http://www.elivecd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Elive is a Debian-based distribution featuring the Enlightenment window manager.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;img src=&quot;:/elive.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.elivecd.org/&quot;&gt;http://www.elivecd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; Elive is een op Debian gebaseerde distributie en heeft de Enlightenment-vensterbeheerder aan boord.&lt;br/&gt;&lt;b&gt;Installatie-opmerkingen:&lt;/b&gt; Met de live-versie kan u in de live-modus opstarten, en van daaruit kan de installatie eventueel worden opgestart.</translation>
    </message>
    <message>
        <source>&lt;img src=&quot;:/kiwi_logo_ro.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.kiwilinux.org/&quot;&gt;http://www.kiwilinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Kiwi Linux is an Ubuntu derivative primarily made for Romanian, Hungarian and English speaking users.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Make sure install media is empty and formatted before proceeding with install.</source>
        <translation type="obsolete">&lt;img src=&quot;:/kiwi_logo_ro.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.kiwilinux.org/&quot;&gt;http://www.kiwilinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beschrijving:&lt;/b&gt; Kiwi Linux is een afgeleide van Ubuntu, vooral voor Roemeens-, Hongaars- of Engelstalige gebruikers.&lt;br/&gt;&lt;b&gt;Installatie-opmerkingen:&lt;/b&gt; Verzeker uzelf ervan dat het installatiemedium leeg en geformatteerd is voordat u verder gaat met de installatie.</translation>
    </message>
    <message>
        <source>&lt;img src=&quot;:/gnewsense.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; gNewSense is a high-quality GNU/Linux distribution that extends and improves Ubuntu to create a completely free operating system without any binary blobs or package trees that contain proprietary software.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Make sure install media is empty and formatted before proceeding with install.</source>
        <translation type="obsolete">&lt;img src=&quot;:/gnewsense.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beschrijving:&lt;/b&gt; gNewSense is een GNU/Linuxdistributie gebaseerd op Ubuntu maar zonder gesloten software.&lt;br/&gt;&lt;b&gt;Opmerkingen m.b.t. installatie:&lt;/b&gt; zorg ervoor dat het installatiemedium leeg en geformatteerd is voor u doorgaat met de installatie.</translation>
    </message>
    <message>
        <source>&lt;img src=&quot;:/nimblex.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.nimblex.net/&quot;&gt;http://www.nimblex.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; NimbleX is a small, versatile Slackware-based distribution. It is built using the linux-live scripts, and features the KDE desktop. It can be booted from CD or flash memory (USB pens or MP3 players), and can easily be customized and extended.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NimbleX boots in Live mode.</source>
        <translation type="obsolete">&lt;img src=&quot;:/nimblex.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.nimblex.net/&quot;&gt;http://www.nimblex.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Omschrijving:&lt;/b&gt; NimbleX is een kleine, veelzijdige distributie die gebaseerd is op Slackware. Hij is gebouwd met de linux-live scripts, and heeft de KDE-werkomgeving in petto. Opstarten kan vanaf cd of flashgeheugen (USB-staafjes of MP3-spelers). Verder kan Nimblex gemakkelijk aangepast en uitgebreid worden.&lt;br/&gt;&lt;b&gt;Installatie-opmerkingen:&lt;/b&gt; NimbleX start op in de live-modus.</translation>
    </message>
    <message>
        <source>&lt;img src=&quot;:/slitaz.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.slitaz.org/en/&quot;&gt;http://www.slitaz.org/en&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; SliTaz is a lightweight, desktop-oriented micro distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional. This installer is based on &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;.</source>
        <translation type="obsolete">&lt;img src=&quot;:/slitaz.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.slitaz.org/en/&quot;&gt;http://www.slitaz.org/en&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beschrijving:&lt;/b&gt; SliTaz is een lichte, bureaublad-georienteerde microdistributie&lt;br/&gt;&lt;b&gt;Opmerkingen m.b.t. installatie:&lt;/b&gt; De live-versie laadt het gehele systeem in het RAM-geheugen en start daarvan op. Installatie is daarom niet noodzakelijk, maar optioneel. Dit installatieprogramma is gebaseerd op&lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;.</translation>
    </message>
    <message>
        <source>&lt;img src=&quot;:/xpud.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.xpud.org/&quot;&gt;http://www.xpud.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; xPUD is a lightweight distribution featuring a simple kiosk-like interface with a web browser and media player.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory.</source>
        <translation type="obsolete">&lt;img src=&quot;:/xpud.png&quot; /&gt;&lt;br/&gt;&lt;b&gt;Thuispagina:&lt;/b&gt; &lt;a href=&quot;http://www.xpud.org/&quot;&gt;http://www.xpud.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Beschrijving:&lt;/b&gt; xPUD is een lichtgewicht-distributie met een simpele kiosk-achtige bedieningsschil inclusief een webbrowser en mediaspeler.&lt;br/&gt;&lt;b&gt;Installatie-opmerkingen:&lt;/b&gt; De Live-versie laadt het gehele systeem in het RAM-geheugen en start daaruit op.</translation>
    </message>
</context>
<context>
    <name>unetbootinui</name>
    <message>
        <location filename="unetbootin.ui" line="20"/>
        <source>Unetbootin</source>
        <translation>Unetbootin</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="44"/>
        <location filename="unetbootin.ui" line="65"/>
        <source>Select from a list of supported distributions</source>
        <translation>Kies uit een lijst van ondersteunde Linuxdistributies</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="47"/>
        <source>&amp;Distribution</source>
        <translation>&amp;Linuxdistributie</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="131"/>
        <source>Specify a disk image file to load</source>
        <translation>Kies een schijfimagebestand om te laden</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="134"/>
        <source>Disk&amp;image</source>
        <translation>Schijf&amp;image</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="147"/>
        <source>Manually specify a kernel and initrd to load</source>
        <translation>Handmatig een te laden kernel en initrd opgeven</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="150"/>
        <source>&amp;Custom</source>
        <translation>&amp;Aangepast</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="414"/>
        <location filename="unetbootin.ui" line="430"/>
        <source>Space to reserve for user files which are preserved across reboots. Works only for LiveUSBs for Ubuntu and derivatives. If value exceeds drive capacity, the maximum space available will be used.</source>
        <translation>Ruimte die gereserveerd wordt voor gebruikersbestanden (documenten e.d.) die bewaard moeten blijven na herstart. Werkt alleen voor Live-USB&apos;s voor Ubuntu en daarvan afgeleide versies. Als de waarde de schijfcapaciteit overschrijdt, zal de maximale hoeveelheid beschikbare ruimte worden gebruikt.</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="417"/>
        <source>Space used to preserve files across reboots (Ubuntu only):</source>
        <translation>Ruimte die gereserveerd wordt voor gebruikersbestanden (documenten e.d.) die bewaard moeten blijven na herstart (alleen voor Ubuntu):</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="440"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="503"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="506"/>
        <source>Return</source>
        <translation>Terug</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="513"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="516"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="561"/>
        <source>Reboot Now</source>
        <translation>Nu herstarten</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="568"/>
        <source>Exit</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="660"/>
        <source>1. Downloading Files</source>
        <translation>1. Bestanden downloaden</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="667"/>
        <source>2. Extracting and Copying Files</source>
        <translation>2. Bestanden uitpakken en kopiëren</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="674"/>
        <source>3. Installing Bootloader</source>
        <translation>3. Opstartlader installeren</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="681"/>
        <source>4. Installation Complete, Reboot</source>
        <translation>4. Installatie gereed; herstarten</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="477"/>
        <location filename="unetbootin.ui" line="496"/>
        <source>Select the target drive to install to</source>
        <translation>Selecteer de doelschijf waarop geïnstalleerd moet worden</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="480"/>
        <source>Dri&amp;ve:</source>
        <translation>Sch&amp;ijf:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="451"/>
        <location filename="unetbootin.ui" line="470"/>
        <source>Select the installation target type</source>
        <translation>Selecteer het type medium waarop geïnstalleerd moet worden</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="454"/>
        <source>&amp;Type:</source>
        <translation>&amp;Type:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="81"/>
        <source>Select the distribution version</source>
        <translation>Kies de distributieversie</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="347"/>
        <source>Select disk image file</source>
        <translation>Selecteer schijfimagebestand (iso)</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="286"/>
        <location filename="unetbootin.ui" line="350"/>
        <location filename="unetbootin.ui" line="375"/>
        <location filename="unetbootin.ui" line="400"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="188"/>
        <source>Select the disk image type</source>
        <translation>Het type schijfimage selecteren</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="243"/>
        <source>Specify a floppy/hard disk image, or CD image (ISO) file to load</source>
        <translation>Kies een te laden diskette-/harde schijf-imagebestand of CD-imagebestand (ISO)</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="207"/>
        <location filename="unetbootin.ui" line="258"/>
        <source>Specify a kernel file to load</source>
        <translation>Geef een te laden kernelbestand op</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="283"/>
        <source>Select kernel file</source>
        <translation>Selecteer kernelbestand</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="293"/>
        <location filename="unetbootin.ui" line="312"/>
        <source>Specify an initrd file to load</source>
        <translation>Kies een te laden initrd-bestand</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="372"/>
        <source>Select initrd file</source>
        <translation>Selecteer initrd-bestand</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="397"/>
        <source>Select syslinux.cfg or isolinux.cfg file</source>
        <translation>Selecteer syslinux.cfg-bestand of isolinux.cfg-bestand</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="226"/>
        <location filename="unetbootin.ui" line="321"/>
        <source>Specify parameters and options to pass to the kernel</source>
        <translation>Geef parameters en opties op die aan de kernel moeten worden doorgegeven</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="210"/>
        <source>&amp;Kernel:</source>
        <translation>&amp;Kernel:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="296"/>
        <source>Init&amp;rd:</source>
        <translation>Init&amp;rd:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="229"/>
        <source>&amp;Options:</source>
        <translation>&amp;Opties:</translation>
    </message>
</context>
<context>
    <name>uninstaller</name>
    <message>
        <location filename="main.cpp" line="156"/>
        <source>Uninstallation Complete</source>
        <translation>Deïnstallatie gereed</translation>
    </message>
    <message>
        <location filename="main.cpp" line="157"/>
        <source>%1 has been uninstalled.</source>
        <translation>%1 is gedeïnstalleerd.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="322"/>
        <source>Must run as root</source>
        <translation>Moet als root draaien</translation>
    </message>
    <message>
        <location filename="main.cpp" line="324"/>
        <source>%2 must be run as root. Close it, and re-run using either:&lt;br/&gt;&lt;b&gt;sudo %1&lt;/b&gt;&lt;br/&gt;or:&lt;br/&gt;&lt;b&gt;su - -c &apos;%1&apos;&lt;/b&gt;</source>
        <translation>%2 moet als root draaien. Sluit het af en start het opnieuw met ofwel:&lt;br/&gt;&lt;b&gt;sudo %1&lt;/b&gt;&lt;br/&gt;ofwel:&lt;br/&gt;&lt;b&gt;su - -c &apos;%1&apos;&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="361"/>
        <source>%1 Uninstaller</source>
        <translation>%1 Verwijderaar</translation>
    </message>
    <message>
        <location filename="main.cpp" line="362"/>
        <source>%1 is currently installed. Remove the existing version?</source>
        <translation>%1 is momenteel geïnstalleerd. Huidige versie verwijderen?</translation>
    </message>
</context>
</TS>
