/********************************************************************************
** Form generated from reading UI file 'unetbootin.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UNETBOOTIN_H
#define UI_UNETBOOTIN_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QTextBrowser>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_unetbootinui
{
public:
    QGridLayout *gridLayout;
    QWidget *firstlayer;
    QGridLayout *gridLayout_2;
    QRadioButton *radioDistro;
    QHBoxLayout *distrolayout;
    QComboBox *distroselect;
    QComboBox *dverselect;
    QTextBrowser *intromessage;
    QVBoxLayout *radioLayout;
    QVBoxLayout *radioButtonLayout;
    QRadioButton *radioFloppy;
    QRadioButton *radioManual;
    QSpacerItem *verticalSpacer;
    QWidget *optionslayer;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *labelslayout;
    QComboBox *diskimagetypeselect;
    QLabel *labelkernel;
    QLabel *labeloption;
    QVBoxLayout *inputlayout;
    QLineEdit *FloppyPath;
    QHBoxLayout *kernellayout;
    QLineEdit *KernelPath;
    QPushButton *KernelFileSelector;
    QLabel *labelinitrd;
    QLineEdit *InitrdPath;
    QLineEdit *OptionEnter;
    QVBoxLayout *buttonslayout;
    QPushButton *FloppyFileSelector;
    QPushButton *InitrdFileSelector;
    QPushButton *CfgFileSelector;
    QHBoxLayout *persistencelayout;
    QLabel *persistencelabel;
    QSpinBox *persistencevalue;
    QLabel *persistenceMBlabel;
    QHBoxLayout *targetlayout;
    QLabel *labeltype;
    QComboBox *typeselect;
    QLabel *labeldrive;
    QComboBox *driveselect;
    QPushButton *okbutton;
    QPushButton *cancelbutton;
    QWidget *secondlayer;
    QGridLayout *oldsecondlayer;
    QWidget *rebootlayer;
    QVBoxLayout *verticalLayout_4;
    QTextBrowser *rebootmsgtext;
    QHBoxLayout *exitbuttonslayout;
    QPushButton *frebootbutton;
    QPushButton *fexitbutton;
    QWidget *progresslayer;
    QVBoxLayout *verticalLayout_5;
    QLabel *pdesc5;
    QLabel *pdesc4;
    QLabel *pdesc3;
    QLabel *pdesc2;
    QLabel *pdesc1;
    QProgressBar *tprogress;
    QVBoxLayout *statuslayout;
    QLabel *sdesc1;
    QLabel *sdesc2;
    QLabel *sdesc3;
    QLabel *sdesc4;
    QLabel *sdesc5;

    void setupUi(QWidget *unetbootinui)
    {
        if (unetbootinui->objectName().isEmpty())
            unetbootinui->setObjectName(QString::fromUtf8("unetbootinui"));
        unetbootinui->resize(524, 360);
        unetbootinui->setMinimumSize(QSize(524, 360));
        gridLayout = new QGridLayout(unetbootinui);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        firstlayer = new QWidget(unetbootinui);
        firstlayer->setObjectName(QString::fromUtf8("firstlayer"));
        gridLayout_2 = new QGridLayout(firstlayer);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        radioDistro = new QRadioButton(firstlayer);
        radioDistro->setObjectName(QString::fromUtf8("radioDistro"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(radioDistro->sizePolicy().hasHeightForWidth());
        radioDistro->setSizePolicy(sizePolicy);
        radioDistro->setMinimumSize(QSize(120, 0));
        radioDistro->setChecked(true);

        gridLayout_2->addWidget(radioDistro, 0, 0, 1, 1);

        distrolayout = new QHBoxLayout();
        distrolayout->setObjectName(QString::fromUtf8("distrolayout"));
        distroselect = new QComboBox(firstlayer);
        distroselect->setObjectName(QString::fromUtf8("distroselect"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(distroselect->sizePolicy().hasHeightForWidth());
        distroselect->setSizePolicy(sizePolicy1);

        distrolayout->addWidget(distroselect);

        dverselect = new QComboBox(firstlayer);
        dverselect->setObjectName(QString::fromUtf8("dverselect"));
        sizePolicy1.setHeightForWidth(dverselect->sizePolicy().hasHeightForWidth());
        dverselect->setSizePolicy(sizePolicy1);

        distrolayout->addWidget(dverselect);


        gridLayout_2->addLayout(distrolayout, 0, 1, 1, 1);

        intromessage = new QTextBrowser(firstlayer);
        intromessage->setObjectName(QString::fromUtf8("intromessage"));
        QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(intromessage->sizePolicy().hasHeightForWidth());
        intromessage->setSizePolicy(sizePolicy2);
        intromessage->setMinimumSize(QSize(0, 30));
        intromessage->setStyleSheet(QString::fromUtf8("background-color: transparent;"));
        intromessage->setFrameShape(QFrame::NoFrame);
        intromessage->setFrameShadow(QFrame::Plain);
        intromessage->setOpenExternalLinks(true);

        gridLayout_2->addWidget(intromessage, 1, 0, 1, 2);

        radioLayout = new QVBoxLayout();
        radioLayout->setObjectName(QString::fromUtf8("radioLayout"));
        radioButtonLayout = new QVBoxLayout();
        radioButtonLayout->setObjectName(QString::fromUtf8("radioButtonLayout"));
        radioFloppy = new QRadioButton(firstlayer);
        radioFloppy->setObjectName(QString::fromUtf8("radioFloppy"));
        radioFloppy->setMinimumSize(QSize(0, 25));

        radioButtonLayout->addWidget(radioFloppy);

        radioManual = new QRadioButton(firstlayer);
        radioManual->setObjectName(QString::fromUtf8("radioManual"));
        radioManual->setMinimumSize(QSize(0, 30));

        radioButtonLayout->addWidget(radioManual);


        radioLayout->addLayout(radioButtonLayout);

        verticalSpacer = new QSpacerItem(20, 28, QSizePolicy::Minimum, QSizePolicy::Preferred);

        radioLayout->addItem(verticalSpacer);


        gridLayout_2->addLayout(radioLayout, 2, 0, 1, 1);

        optionslayer = new QWidget(firstlayer);
        optionslayer->setObjectName(QString::fromUtf8("optionslayer"));
        optionslayer->setEnabled(false);
        horizontalLayout = new QHBoxLayout(optionslayer);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        labelslayout = new QVBoxLayout();
        labelslayout->setObjectName(QString::fromUtf8("labelslayout"));
        diskimagetypeselect = new QComboBox(optionslayer);
        diskimagetypeselect->setObjectName(QString::fromUtf8("diskimagetypeselect"));

        labelslayout->addWidget(diskimagetypeselect);

        labelkernel = new QLabel(optionslayer);
        labelkernel->setObjectName(QString::fromUtf8("labelkernel"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(labelkernel->sizePolicy().hasHeightForWidth());
        labelkernel->setSizePolicy(sizePolicy3);
        labelkernel->setMinimumSize(QSize(50, 0));

        labelslayout->addWidget(labelkernel);

        labeloption = new QLabel(optionslayer);
        labeloption->setObjectName(QString::fromUtf8("labeloption"));
        labeloption->setMinimumSize(QSize(50, 0));

        labelslayout->addWidget(labeloption);


        horizontalLayout->addLayout(labelslayout);

        inputlayout = new QVBoxLayout();
        inputlayout->setObjectName(QString::fromUtf8("inputlayout"));
        FloppyPath = new QLineEdit(optionslayer);
        FloppyPath->setObjectName(QString::fromUtf8("FloppyPath"));

        inputlayout->addWidget(FloppyPath);

        kernellayout = new QHBoxLayout();
        kernellayout->setObjectName(QString::fromUtf8("kernellayout"));
        KernelPath = new QLineEdit(optionslayer);
        KernelPath->setObjectName(QString::fromUtf8("KernelPath"));
        QSizePolicy sizePolicy4(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(KernelPath->sizePolicy().hasHeightForWidth());
        KernelPath->setSizePolicy(sizePolicy4);

        kernellayout->addWidget(KernelPath);

        KernelFileSelector = new QPushButton(optionslayer);
        KernelFileSelector->setObjectName(QString::fromUtf8("KernelFileSelector"));
        QSizePolicy sizePolicy5(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(25);
        sizePolicy5.setVerticalStretch(25);
        sizePolicy5.setHeightForWidth(KernelFileSelector->sizePolicy().hasHeightForWidth());
        KernelFileSelector->setSizePolicy(sizePolicy5);
        KernelFileSelector->setMinimumSize(QSize(25, 25));
        KernelFileSelector->setAcceptDrops(false);

        kernellayout->addWidget(KernelFileSelector);

        labelinitrd = new QLabel(optionslayer);
        labelinitrd->setObjectName(QString::fromUtf8("labelinitrd"));

        kernellayout->addWidget(labelinitrd);

        InitrdPath = new QLineEdit(optionslayer);
        InitrdPath->setObjectName(QString::fromUtf8("InitrdPath"));
        QSizePolicy sizePolicy6(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(20);
        sizePolicy6.setHeightForWidth(InitrdPath->sizePolicy().hasHeightForWidth());
        InitrdPath->setSizePolicy(sizePolicy6);

        kernellayout->addWidget(InitrdPath);


        inputlayout->addLayout(kernellayout);

        OptionEnter = new QLineEdit(optionslayer);
        OptionEnter->setObjectName(QString::fromUtf8("OptionEnter"));

        inputlayout->addWidget(OptionEnter);


        horizontalLayout->addLayout(inputlayout);

        buttonslayout = new QVBoxLayout();
        buttonslayout->setObjectName(QString::fromUtf8("buttonslayout"));
        FloppyFileSelector = new QPushButton(optionslayer);
        FloppyFileSelector->setObjectName(QString::fromUtf8("FloppyFileSelector"));
        sizePolicy5.setHeightForWidth(FloppyFileSelector->sizePolicy().hasHeightForWidth());
        FloppyFileSelector->setSizePolicy(sizePolicy5);
        FloppyFileSelector->setMinimumSize(QSize(25, 25));
        FloppyFileSelector->setAcceptDrops(false);

        buttonslayout->addWidget(FloppyFileSelector);

        InitrdFileSelector = new QPushButton(optionslayer);
        InitrdFileSelector->setObjectName(QString::fromUtf8("InitrdFileSelector"));
        sizePolicy5.setHeightForWidth(InitrdFileSelector->sizePolicy().hasHeightForWidth());
        InitrdFileSelector->setSizePolicy(sizePolicy5);
        InitrdFileSelector->setMinimumSize(QSize(25, 25));
        InitrdFileSelector->setAcceptDrops(false);

        buttonslayout->addWidget(InitrdFileSelector);

        CfgFileSelector = new QPushButton(optionslayer);
        CfgFileSelector->setObjectName(QString::fromUtf8("CfgFileSelector"));
        sizePolicy5.setHeightForWidth(CfgFileSelector->sizePolicy().hasHeightForWidth());
        CfgFileSelector->setSizePolicy(sizePolicy5);
        CfgFileSelector->setMinimumSize(QSize(25, 25));
        CfgFileSelector->setAcceptDrops(false);

        buttonslayout->addWidget(CfgFileSelector);


        horizontalLayout->addLayout(buttonslayout);


        gridLayout_2->addWidget(optionslayer, 2, 1, 1, 1);

        persistencelayout = new QHBoxLayout();
        persistencelayout->setObjectName(QString::fromUtf8("persistencelayout"));
        persistencelabel = new QLabel(firstlayer);
        persistencelabel->setObjectName(QString::fromUtf8("persistencelabel"));

        persistencelayout->addWidget(persistencelabel);

        persistencevalue = new QSpinBox(firstlayer);
        persistencevalue->setObjectName(QString::fromUtf8("persistencevalue"));
        sizePolicy4.setHeightForWidth(persistencevalue->sizePolicy().hasHeightForWidth());
        persistencevalue->setSizePolicy(sizePolicy4);
        persistencevalue->setMaximum(9999);

        persistencelayout->addWidget(persistencevalue);

        persistenceMBlabel = new QLabel(firstlayer);
        persistenceMBlabel->setObjectName(QString::fromUtf8("persistenceMBlabel"));

        persistencelayout->addWidget(persistenceMBlabel);


        gridLayout_2->addLayout(persistencelayout, 3, 0, 1, 2);

        targetlayout = new QHBoxLayout();
        targetlayout->setObjectName(QString::fromUtf8("targetlayout"));
        labeltype = new QLabel(firstlayer);
        labeltype->setObjectName(QString::fromUtf8("labeltype"));

        targetlayout->addWidget(labeltype);

        typeselect = new QComboBox(firstlayer);
        typeselect->setObjectName(QString::fromUtf8("typeselect"));
        sizePolicy1.setHeightForWidth(typeselect->sizePolicy().hasHeightForWidth());
        typeselect->setSizePolicy(sizePolicy1);

        targetlayout->addWidget(typeselect);

        labeldrive = new QLabel(firstlayer);
        labeldrive->setObjectName(QString::fromUtf8("labeldrive"));

        targetlayout->addWidget(labeldrive);

        driveselect = new QComboBox(firstlayer);
        driveselect->setObjectName(QString::fromUtf8("driveselect"));
        sizePolicy1.setHeightForWidth(driveselect->sizePolicy().hasHeightForWidth());
        driveselect->setSizePolicy(sizePolicy1);

        targetlayout->addWidget(driveselect);

        okbutton = new QPushButton(firstlayer);
        okbutton->setObjectName(QString::fromUtf8("okbutton"));

        targetlayout->addWidget(okbutton);

        cancelbutton = new QPushButton(firstlayer);
        cancelbutton->setObjectName(QString::fromUtf8("cancelbutton"));

        targetlayout->addWidget(cancelbutton);


        gridLayout_2->addLayout(targetlayout, 4, 0, 1, 2);


        gridLayout->addWidget(firstlayer, 1, 1, 1, 1);

        secondlayer = new QWidget(unetbootinui);
        secondlayer->setObjectName(QString::fromUtf8("secondlayer"));
        oldsecondlayer = new QGridLayout(secondlayer);
        oldsecondlayer->setObjectName(QString::fromUtf8("oldsecondlayer"));
        rebootlayer = new QWidget(secondlayer);
        rebootlayer->setObjectName(QString::fromUtf8("rebootlayer"));
        rebootlayer->setEnabled(false);
        verticalLayout_4 = new QVBoxLayout(rebootlayer);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        rebootmsgtext = new QTextBrowser(rebootlayer);
        rebootmsgtext->setObjectName(QString::fromUtf8("rebootmsgtext"));
        rebootmsgtext->setStyleSheet(QString::fromUtf8("background-color: transparent;"));
        rebootmsgtext->setFrameShape(QFrame::NoFrame);
        rebootmsgtext->setFrameShadow(QFrame::Plain);
        rebootmsgtext->setLineWidth(0);
        rebootmsgtext->setOpenExternalLinks(true);

        verticalLayout_4->addWidget(rebootmsgtext);

        exitbuttonslayout = new QHBoxLayout();
        exitbuttonslayout->setObjectName(QString::fromUtf8("exitbuttonslayout"));
        exitbuttonslayout->setContentsMargins(300, -1, -1, -1);
        frebootbutton = new QPushButton(rebootlayer);
        frebootbutton->setObjectName(QString::fromUtf8("frebootbutton"));

        exitbuttonslayout->addWidget(frebootbutton);

        fexitbutton = new QPushButton(rebootlayer);
        fexitbutton->setObjectName(QString::fromUtf8("fexitbutton"));

        exitbuttonslayout->addWidget(fexitbutton);


        verticalLayout_4->addLayout(exitbuttonslayout);


        oldsecondlayer->addWidget(rebootlayer, 1, 0, 1, 1);

        progresslayer = new QWidget(secondlayer);
        progresslayer->setObjectName(QString::fromUtf8("progresslayer"));
        progresslayer->setEnabled(false);
        verticalLayout_5 = new QVBoxLayout(progresslayer);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        pdesc5 = new QLabel(progresslayer);
        pdesc5->setObjectName(QString::fromUtf8("pdesc5"));
        pdesc5->setOpenExternalLinks(true);

        verticalLayout_5->addWidget(pdesc5);

        pdesc4 = new QLabel(progresslayer);
        pdesc4->setObjectName(QString::fromUtf8("pdesc4"));
        pdesc4->setOpenExternalLinks(true);

        verticalLayout_5->addWidget(pdesc4);

        pdesc3 = new QLabel(progresslayer);
        pdesc3->setObjectName(QString::fromUtf8("pdesc3"));
        pdesc3->setOpenExternalLinks(true);

        verticalLayout_5->addWidget(pdesc3);

        pdesc2 = new QLabel(progresslayer);
        pdesc2->setObjectName(QString::fromUtf8("pdesc2"));
        pdesc2->setOpenExternalLinks(true);

        verticalLayout_5->addWidget(pdesc2);

        pdesc1 = new QLabel(progresslayer);
        pdesc1->setObjectName(QString::fromUtf8("pdesc1"));
        pdesc1->setOpenExternalLinks(true);

        verticalLayout_5->addWidget(pdesc1);

        tprogress = new QProgressBar(progresslayer);
        tprogress->setObjectName(QString::fromUtf8("tprogress"));
        QSizePolicy sizePolicy7(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(0);
        sizePolicy7.setHeightForWidth(tprogress->sizePolicy().hasHeightForWidth());
        tprogress->setSizePolicy(sizePolicy7);
        tprogress->setMinimumSize(QSize(0, 20));
        tprogress->setValue(24);

        verticalLayout_5->addWidget(tprogress);


        oldsecondlayer->addWidget(progresslayer, 2, 0, 1, 1);

        statuslayout = new QVBoxLayout();
        statuslayout->setObjectName(QString::fromUtf8("statuslayout"));
        sdesc1 = new QLabel(secondlayer);
        sdesc1->setObjectName(QString::fromUtf8("sdesc1"));

        statuslayout->addWidget(sdesc1);

        sdesc2 = new QLabel(secondlayer);
        sdesc2->setObjectName(QString::fromUtf8("sdesc2"));

        statuslayout->addWidget(sdesc2);

        sdesc3 = new QLabel(secondlayer);
        sdesc3->setObjectName(QString::fromUtf8("sdesc3"));

        statuslayout->addWidget(sdesc3);

        sdesc4 = new QLabel(secondlayer);
        sdesc4->setObjectName(QString::fromUtf8("sdesc4"));

        statuslayout->addWidget(sdesc4);

        sdesc5 = new QLabel(secondlayer);
        sdesc5->setObjectName(QString::fromUtf8("sdesc5"));

        statuslayout->addWidget(sdesc5);


        oldsecondlayer->addLayout(statuslayout, 0, 0, 1, 1);


        gridLayout->addWidget(secondlayer, 0, 0, 2, 2);

#ifndef QT_NO_SHORTCUT
        labelkernel->setBuddy(KernelPath);
        labeloption->setBuddy(OptionEnter);
        labelinitrd->setBuddy(InitrdPath);
        labeltype->setBuddy(typeselect);
        labeldrive->setBuddy(driveselect);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(radioDistro, distroselect);
        QWidget::setTabOrder(distroselect, dverselect);
        QWidget::setTabOrder(dverselect, radioFloppy);
        QWidget::setTabOrder(radioFloppy, diskimagetypeselect);
        QWidget::setTabOrder(diskimagetypeselect, FloppyPath);
        QWidget::setTabOrder(FloppyPath, FloppyFileSelector);
        QWidget::setTabOrder(FloppyFileSelector, radioManual);
        QWidget::setTabOrder(radioManual, KernelPath);
        QWidget::setTabOrder(KernelPath, KernelFileSelector);
        QWidget::setTabOrder(KernelFileSelector, InitrdPath);
        QWidget::setTabOrder(InitrdPath, InitrdFileSelector);
        QWidget::setTabOrder(InitrdFileSelector, OptionEnter);
        QWidget::setTabOrder(OptionEnter, CfgFileSelector);
        QWidget::setTabOrder(CfgFileSelector, typeselect);
        QWidget::setTabOrder(typeselect, driveselect);
        QWidget::setTabOrder(driveselect, okbutton);
        QWidget::setTabOrder(okbutton, cancelbutton);
        QWidget::setTabOrder(cancelbutton, frebootbutton);
        QWidget::setTabOrder(frebootbutton, fexitbutton);
        QWidget::setTabOrder(fexitbutton, intromessage);
        QWidget::setTabOrder(intromessage, rebootmsgtext);

        retranslateUi(unetbootinui);

        distroselect->setCurrentIndex(-1);
        dverselect->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(unetbootinui);
    } // setupUi

    void retranslateUi(QWidget *unetbootinui)
    {
        unetbootinui->setWindowTitle(QApplication::translate("unetbootinui", "Unetbootin", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        radioDistro->setToolTip(QApplication::translate("unetbootinui", "Select from a list of supported distributions", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        radioDistro->setText(QApplication::translate("unetbootinui", "&Distribution", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        distroselect->setToolTip(QApplication::translate("unetbootinui", "Select from a list of supported distributions", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        dverselect->setToolTip(QApplication::translate("unetbootinui", "Select the distribution version", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        radioFloppy->setToolTip(QApplication::translate("unetbootinui", "Specify a disk image file to load", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        radioFloppy->setText(QApplication::translate("unetbootinui", "Disk&image", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        radioManual->setToolTip(QApplication::translate("unetbootinui", "Manually specify a kernel and initrd to load", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        radioManual->setText(QApplication::translate("unetbootinui", "&Custom", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        diskimagetypeselect->setToolTip(QApplication::translate("unetbootinui", "Select the disk image type", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        labelkernel->setToolTip(QApplication::translate("unetbootinui", "Specify a kernel file to load", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        labelkernel->setText(QApplication::translate("unetbootinui", "&Kernel:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        labeloption->setToolTip(QApplication::translate("unetbootinui", "Specify parameters and options to pass to the kernel", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        labeloption->setText(QApplication::translate("unetbootinui", "&Options:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        FloppyPath->setToolTip(QApplication::translate("unetbootinui", "Specify a floppy/hard disk image, or CD image (ISO) file to load", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        KernelPath->setToolTip(QApplication::translate("unetbootinui", "Specify a kernel file to load", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        KernelPath->setText(QString());
#ifndef QT_NO_TOOLTIP
        KernelFileSelector->setToolTip(QApplication::translate("unetbootinui", "Select kernel file", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        KernelFileSelector->setText(QApplication::translate("unetbootinui", "...", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        labelinitrd->setToolTip(QApplication::translate("unetbootinui", "Specify an initrd file to load", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        labelinitrd->setText(QApplication::translate("unetbootinui", "Init&rd:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        InitrdPath->setToolTip(QApplication::translate("unetbootinui", "Specify an initrd file to load", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        OptionEnter->setToolTip(QApplication::translate("unetbootinui", "Specify parameters and options to pass to the kernel", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        FloppyFileSelector->setToolTip(QApplication::translate("unetbootinui", "Select disk image file", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        FloppyFileSelector->setText(QApplication::translate("unetbootinui", "...", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        InitrdFileSelector->setToolTip(QApplication::translate("unetbootinui", "Select initrd file", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        InitrdFileSelector->setText(QApplication::translate("unetbootinui", "...", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        CfgFileSelector->setToolTip(QApplication::translate("unetbootinui", "Select syslinux.cfg or isolinux.cfg file", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        CfgFileSelector->setText(QApplication::translate("unetbootinui", "...", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        persistencelabel->setToolTip(QApplication::translate("unetbootinui", "Space to reserve for user files which are preserved across reboots. Works only for LiveUSBs for Ubuntu and derivatives. If value exceeds drive capacity, the maximum space available will be used.", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        persistencelabel->setText(QApplication::translate("unetbootinui", "Space used to preserve files across reboots (Ubuntu only):", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        persistencevalue->setToolTip(QApplication::translate("unetbootinui", "Space to reserve for user files which are preserved across reboots. Works only for LiveUSBs for Ubuntu and derivatives. If value exceeds drive capacity, the maximum space available will be used.", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        persistenceMBlabel->setText(QApplication::translate("unetbootinui", "MB", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        labeltype->setToolTip(QApplication::translate("unetbootinui", "Select the installation target type", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        labeltype->setText(QApplication::translate("unetbootinui", "&Type:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        typeselect->setToolTip(QApplication::translate("unetbootinui", "Select the installation target type", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        labeldrive->setToolTip(QApplication::translate("unetbootinui", "Select the target drive to install to", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        labeldrive->setText(QApplication::translate("unetbootinui", "Dri&ve:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        driveselect->setToolTip(QApplication::translate("unetbootinui", "Select the target drive to install to", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        okbutton->setText(QApplication::translate("unetbootinui", "OK", 0, QApplication::UnicodeUTF8));
        okbutton->setShortcut(QApplication::translate("unetbootinui", "Return", 0, QApplication::UnicodeUTF8));
        cancelbutton->setText(QApplication::translate("unetbootinui", "Cancel", 0, QApplication::UnicodeUTF8));
        cancelbutton->setShortcut(QApplication::translate("unetbootinui", "Esc", 0, QApplication::UnicodeUTF8));
        frebootbutton->setText(QApplication::translate("unetbootinui", "Reboot Now", 0, QApplication::UnicodeUTF8));
        fexitbutton->setText(QApplication::translate("unetbootinui", "Exit", 0, QApplication::UnicodeUTF8));
        pdesc5->setText(QString());
        pdesc4->setText(QString());
        pdesc3->setText(QString());
        pdesc2->setText(QString());
        pdesc1->setText(QString());
        sdesc1->setText(QApplication::translate("unetbootinui", "1. Downloading Files", 0, QApplication::UnicodeUTF8));
        sdesc2->setText(QApplication::translate("unetbootinui", "2. Extracting and Copying Files", 0, QApplication::UnicodeUTF8));
        sdesc3->setText(QApplication::translate("unetbootinui", "3. Installing Bootloader", 0, QApplication::UnicodeUTF8));
        sdesc4->setText(QApplication::translate("unetbootinui", "4. Installation Complete, Reboot", 0, QApplication::UnicodeUTF8));
        sdesc5->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class unetbootinui: public Ui_unetbootinui {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UNETBOOTIN_H
