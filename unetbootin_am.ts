<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="am_ET">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="266"/>
        <source>LeftToRight</source>
        <translation>ከግራ ወደ ቀኝ</translation>
    </message>
</context>
<context>
    <name>unetbootin</name>
    <message>
        <location filename="unetbootin.cpp" line="213"/>
        <location filename="unetbootin.cpp" line="314"/>
        <location filename="unetbootin.cpp" line="315"/>
        <location filename="unetbootin.cpp" line="384"/>
        <location filename="unetbootin.cpp" line="558"/>
        <location filename="unetbootin.cpp" line="3436"/>
        <location filename="unetbootin.cpp" line="3449"/>
        <location filename="unetbootin.cpp" line="3631"/>
        <location filename="unetbootin.cpp" line="4291"/>
        <source>Hard Disk</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="215"/>
        <location filename="unetbootin.cpp" line="311"/>
        <location filename="unetbootin.cpp" line="312"/>
        <location filename="unetbootin.cpp" line="386"/>
        <location filename="unetbootin.cpp" line="562"/>
        <location filename="unetbootin.cpp" line="728"/>
        <location filename="unetbootin.cpp" line="748"/>
        <location filename="unetbootin.cpp" line="1002"/>
        <location filename="unetbootin.cpp" line="1577"/>
        <location filename="unetbootin.cpp" line="1664"/>
        <location filename="unetbootin.cpp" line="2593"/>
        <location filename="unetbootin.cpp" line="2636"/>
        <location filename="unetbootin.cpp" line="3440"/>
        <location filename="unetbootin.cpp" line="3466"/>
        <location filename="unetbootin.cpp" line="3635"/>
        <location filename="unetbootin.cpp" line="3959"/>
        <location filename="unetbootin.cpp" line="4295"/>
        <source>USB Drive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="216"/>
        <location filename="unetbootin.cpp" line="233"/>
        <location filename="unetbootin.cpp" line="234"/>
        <location filename="unetbootin.cpp" line="350"/>
        <location filename="unetbootin.cpp" line="676"/>
        <location filename="unetbootin.cpp" line="679"/>
        <location filename="unetbootin.cpp" line="680"/>
        <location filename="unetbootin.cpp" line="3525"/>
        <source>ISO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="217"/>
        <location filename="unetbootin.cpp" line="229"/>
        <location filename="unetbootin.cpp" line="230"/>
        <location filename="unetbootin.cpp" line="356"/>
        <location filename="unetbootin.cpp" line="676"/>
        <location filename="unetbootin.cpp" line="684"/>
        <location filename="unetbootin.cpp" line="685"/>
        <location filename="unetbootin.cpp" line="3517"/>
        <source>Floppy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="250"/>
        <location filename="unetbootin.cpp" line="256"/>
        <location filename="unetbootin.cpp" line="260"/>
        <location filename="unetbootin.cpp" line="264"/>
        <location filename="unetbootin.cpp" line="268"/>
        <location filename="unetbootin.cpp" line="274"/>
        <location filename="unetbootin.cpp" line="302"/>
        <source>either</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="281"/>
        <source>LiveUSB persistence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="296"/>
        <source>FAT32-formatted USB drive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="300"/>
        <source>EXT2-formatted USB drive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="676"/>
        <source>Open Disk Image File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="676"/>
        <source>All Files</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="694"/>
        <location filename="unetbootin.cpp" line="702"/>
        <location filename="unetbootin.cpp" line="710"/>
        <source>All Files (*)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="694"/>
        <source>Open Kernel File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="702"/>
        <source>Open Initrd File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="710"/>
        <source>Open Bootloader Config File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="732"/>
        <source>Insert a USB flash drive</source>
        <translation>ዩ.ኤስ.ቢ ፍላሽ ዲስኩን ያስገቡ</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="733"/>
        <source>No USB flash drives were found. If you have already inserted a USB drive, try reformatting it as FAT32.</source>
        <translation>ዩ.ኤስ.ቢ ፍላሽ ዲስክ አልተገኘም።  ዩ.ኤስ.ቢ ፍላሽ ዲስክ ከሰኩ በFAT32 ፎርማት ያድርጉት።</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="752"/>
        <source>%1 not mounted</source>
        <translation>1% ማውንት አልተደረገም</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="753"/>
        <source>You must first mount the USB drive %1 to a mountpoint. Most distributions will do this automatically after you remove and reinsert the USB drive.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="768"/>
        <source>Select a distro</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="769"/>
        <source>You must select a distribution to load.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="783"/>
        <source>Select a disk image file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="784"/>
        <source>You must select a disk image file to load.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="798"/>
        <source>Select a kernel and/or initrd file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="799"/>
        <source>You must select a kernel and/or initrd file to load.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="813"/>
        <source>Diskimage file not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="814"/>
        <source>The specified diskimage file %1 does not exist.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="828"/>
        <source>Kernel file not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="829"/>
        <source>The specified kernel file %1 does not exist.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="843"/>
        <source>Initrd file not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="844"/>
        <source>The specified initrd file %1 does not exist.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="948"/>
        <source>%1 exists, overwrite?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="949"/>
        <source>The file %1 already exists. Press &apos;Yes to All&apos; to overwrite it and not be prompted again, &apos;Yes&apos; to overwrite files on an individual basis, and &apos;No&apos; to retain your existing version. If in doubt, press &apos;Yes to All&apos;.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="975"/>
        <source>%1 is out of space, abort installation?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="976"/>
        <source>The directory %1 is out of space. Press &apos;Yes&apos; to abort installation, &apos;No&apos; to ignore this error and attempt to continue installation, and &apos;No to All&apos; to ignore all out-of-space errors.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1070"/>
        <source>Locating kernel file in %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1121"/>
        <source>Copying kernel file from %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1127"/>
        <source>Locating initrd file in %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1168"/>
        <source>Copying initrd file from %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1174"/>
        <location filename="unetbootin.cpp" line="1254"/>
        <source>Extracting bootloader configuration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1483"/>
        <location filename="unetbootin.cpp" line="1509"/>
        <source>&lt;b&gt;Extracting compressed iso:&lt;/b&gt; %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1763"/>
        <source>Copying file, please wait...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1764"/>
        <location filename="unetbootin.cpp" line="2578"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1765"/>
        <location filename="unetbootin.cpp" line="2579"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1766"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; 0 bytes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1804"/>
        <source>Extracting files, please wait...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1805"/>
        <source>&lt;b&gt;Archive:&lt;/b&gt; %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1806"/>
        <source>&lt;b&gt;Source:&lt;/b&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1807"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1808"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; 0 of %1 files</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1811"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; %1 (%2)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1812"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1%2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1813"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; %1 of %2 files</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2577"/>
        <source>Downloading files, please wait...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2580"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; 0 bytes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2704"/>
        <source>Download of %1 %2 from %3 failed. Please try downloading the ISO file from the website directly and supply it via the diskimage option.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2727"/>
        <location filename="unetbootin.cpp" line="2742"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; %1 of %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2757"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; %1 of %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2848"/>
        <source>Searching in &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2852"/>
        <source>%1/%2 matches in &lt;a href=&quot;%3&quot;&gt;%3&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3125"/>
        <source>%1 not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3126"/>
        <source>%1 not found. This is required for %2 install mode.
Install the &quot;%3&quot; package or your distribution&apos;s equivalent.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3417"/>
        <source>(Current)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3418"/>
        <source>(Done)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3710"/>
        <source>Configuring grub2 on %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3722"/>
        <source>Configuring grldr on %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3750"/>
        <source>Configuring grub on %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4048"/>
        <source>Installing syslinux to %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4083"/>
        <source>Installing extlinux to %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4243"/>
        <source>Syncing filesystems</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4248"/>
        <source>Setting up persistence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4293"/>
        <source>After rebooting, select the </source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4298"/>
        <source>After rebooting, select the USB boot option in the BIOS boot menu.%1
Reboot now?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4301"/>
        <source>The created USB device will not boot off a Mac. Insert it into a PC, and select the USB boot option in the BIOS boot menu.%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="764"/>
        <source>== Select Distribution ==</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>unetbootinui</name>
    <message>
        <location filename="unetbootin.ui" line="20"/>
        <source>Unetbootin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="44"/>
        <location filename="unetbootin.ui" line="65"/>
        <source>Select from a list of supported distributions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="47"/>
        <source>&amp;Distribution</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="131"/>
        <source>Specify a disk image file to load</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="134"/>
        <source>Disk&amp;image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="147"/>
        <source>Manually specify a kernel and initrd to load</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="150"/>
        <source>&amp;Custom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="414"/>
        <location filename="unetbootin.ui" line="430"/>
        <source>Space to reserve for user files which are preserved across reboots. Works only for LiveUSBs for Ubuntu and derivatives. If value exceeds drive capacity, the maximum space available will be used.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="417"/>
        <source>Space used to preserve files across reboots (Ubuntu only):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="440"/>
        <source>MB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="503"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="506"/>
        <source>Return</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="513"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="516"/>
        <source>Esc</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="561"/>
        <source>Reboot Now</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="568"/>
        <source>Exit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="660"/>
        <source>1. Downloading Files</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="667"/>
        <source>2. Extracting and Copying Files</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="674"/>
        <source>3. Installing Bootloader</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="681"/>
        <source>4. Installation Complete, Reboot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="477"/>
        <location filename="unetbootin.ui" line="496"/>
        <source>Select the target drive to install to</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="480"/>
        <source>Dri&amp;ve:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="451"/>
        <location filename="unetbootin.ui" line="470"/>
        <source>Select the installation target type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="454"/>
        <source>&amp;Type:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="81"/>
        <source>Select the distribution version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="347"/>
        <source>Select disk image file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="286"/>
        <location filename="unetbootin.ui" line="350"/>
        <location filename="unetbootin.ui" line="375"/>
        <location filename="unetbootin.ui" line="400"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="188"/>
        <source>Select the disk image type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="243"/>
        <source>Specify a floppy/hard disk image, or CD image (ISO) file to load</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="207"/>
        <location filename="unetbootin.ui" line="258"/>
        <source>Specify a kernel file to load</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="283"/>
        <source>Select kernel file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="293"/>
        <location filename="unetbootin.ui" line="312"/>
        <source>Specify an initrd file to load</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="372"/>
        <source>Select initrd file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="397"/>
        <source>Select syslinux.cfg or isolinux.cfg file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="226"/>
        <location filename="unetbootin.ui" line="321"/>
        <source>Specify parameters and options to pass to the kernel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="210"/>
        <source>&amp;Kernel:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="296"/>
        <source>Init&amp;rd:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="229"/>
        <source>&amp;Options:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>uninstaller</name>
    <message>
        <location filename="main.cpp" line="156"/>
        <source>Uninstallation Complete</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main.cpp" line="157"/>
        <source>%1 has been uninstalled.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main.cpp" line="322"/>
        <source>Must run as root</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main.cpp" line="324"/>
        <source>%2 must be run as root. Close it, and re-run using either:&lt;br/&gt;&lt;b&gt;sudo %1&lt;/b&gt;&lt;br/&gt;or:&lt;br/&gt;&lt;b&gt;su - -c &apos;%1&apos;&lt;/b&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main.cpp" line="361"/>
        <source>%1 Uninstaller</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main.cpp" line="362"/>
        <source>%1 is currently installed. Remove the existing version?</source>
        <translation></translation>
    </message>
</context>
</TS>
