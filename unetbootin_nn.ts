<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="nn_NO">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="266"/>
        <source>LeftToRight</source>
        <translation>Venstre-til-høgre</translation>
    </message>
</context>
<context>
    <name>unetbootin</name>
    <message>
        <location filename="unetbootin.cpp" line="213"/>
        <location filename="unetbootin.cpp" line="314"/>
        <location filename="unetbootin.cpp" line="315"/>
        <location filename="unetbootin.cpp" line="384"/>
        <location filename="unetbootin.cpp" line="558"/>
        <location filename="unetbootin.cpp" line="3436"/>
        <location filename="unetbootin.cpp" line="3449"/>
        <location filename="unetbootin.cpp" line="3631"/>
        <location filename="unetbootin.cpp" line="4291"/>
        <source>Hard Disk</source>
        <translation>Harddisk</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="215"/>
        <location filename="unetbootin.cpp" line="311"/>
        <location filename="unetbootin.cpp" line="312"/>
        <location filename="unetbootin.cpp" line="386"/>
        <location filename="unetbootin.cpp" line="562"/>
        <location filename="unetbootin.cpp" line="728"/>
        <location filename="unetbootin.cpp" line="748"/>
        <location filename="unetbootin.cpp" line="1002"/>
        <location filename="unetbootin.cpp" line="1577"/>
        <location filename="unetbootin.cpp" line="1664"/>
        <location filename="unetbootin.cpp" line="2593"/>
        <location filename="unetbootin.cpp" line="2636"/>
        <location filename="unetbootin.cpp" line="3440"/>
        <location filename="unetbootin.cpp" line="3466"/>
        <location filename="unetbootin.cpp" line="3635"/>
        <location filename="unetbootin.cpp" line="3959"/>
        <location filename="unetbootin.cpp" line="4295"/>
        <source>USB Drive</source>
        <translation>USB-disk</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="216"/>
        <location filename="unetbootin.cpp" line="233"/>
        <location filename="unetbootin.cpp" line="234"/>
        <location filename="unetbootin.cpp" line="350"/>
        <location filename="unetbootin.cpp" line="676"/>
        <location filename="unetbootin.cpp" line="679"/>
        <location filename="unetbootin.cpp" line="680"/>
        <location filename="unetbootin.cpp" line="3525"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="217"/>
        <location filename="unetbootin.cpp" line="229"/>
        <location filename="unetbootin.cpp" line="230"/>
        <location filename="unetbootin.cpp" line="356"/>
        <location filename="unetbootin.cpp" line="676"/>
        <location filename="unetbootin.cpp" line="684"/>
        <location filename="unetbootin.cpp" line="685"/>
        <location filename="unetbootin.cpp" line="3517"/>
        <source>Floppy</source>
        <translation>Diskett</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="250"/>
        <location filename="unetbootin.cpp" line="256"/>
        <location filename="unetbootin.cpp" line="260"/>
        <location filename="unetbootin.cpp" line="264"/>
        <location filename="unetbootin.cpp" line="268"/>
        <location filename="unetbootin.cpp" line="274"/>
        <location filename="unetbootin.cpp" line="302"/>
        <source>either</source>
        <translation>enten</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="281"/>
        <source>LiveUSB persistence</source>
        <translation>LiveUSB-lagring</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="296"/>
        <source>FAT32-formatted USB drive</source>
        <translation>FAT32-formatert USB-eining</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="300"/>
        <source>EXT2-formatted USB drive</source>
        <translation>EXT2-formatert USB-eining</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="676"/>
        <source>Open Disk Image File</source>
        <translation>Opna diskbiletfil</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="676"/>
        <source>All Files</source>
        <translation>Alle filer</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="694"/>
        <location filename="unetbootin.cpp" line="702"/>
        <location filename="unetbootin.cpp" line="710"/>
        <source>All Files (*)</source>
        <translation>Alle filer (*)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="694"/>
        <source>Open Kernel File</source>
        <translation>Opne kjernefil</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="702"/>
        <source>Open Initrd File</source>
        <translation>Opne initrd-fil</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="710"/>
        <source>Open Bootloader Config File</source>
        <translation>Opne konfigurasjonsfil for oppstartslastar</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="732"/>
        <source>Insert a USB flash drive</source>
        <translation>Set inn ein USB-disk</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="733"/>
        <source>No USB flash drives were found. If you have already inserted a USB drive, try reformatting it as FAT32.</source>
        <translation>Ingen USB-diskar vart funne. Om du allereie har sett inn USB-disken, prøv å formatere den som FAT32.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="752"/>
        <source>%1 not mounted</source>
        <translation>%1 ikkje montert</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="753"/>
        <source>You must first mount the USB drive %1 to a mountpoint. Most distributions will do this automatically after you remove and reinsert the USB drive.</source>
        <translation>Du må først montere USB-disken %1 til eit monteringspunkt. Dei fleste distribusjonar ordnar dette automatisk etter at du har fjerna og sett inn att USB-disken.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="768"/>
        <source>Select a distro</source>
        <translation>Vel ein distribusjon</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="769"/>
        <source>You must select a distribution to load.</source>
        <translation>Du må velje distribusjonen som skal brukast.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="783"/>
        <source>Select a disk image file</source>
        <translation>Vel ei diskbiletfil</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="784"/>
        <source>You must select a disk image file to load.</source>
        <translation>Du må velja diskbiletfila som skal brukast.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="798"/>
        <source>Select a kernel and/or initrd file</source>
        <translation>Vel ei kjerne- og/eller ei initrd-fil</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="799"/>
        <source>You must select a kernel and/or initrd file to load.</source>
        <translation>Du må velje kjerne- og/eller initrd-fila som skal brukast</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="813"/>
        <source>Diskimage file not found</source>
        <translation>Biletfil ikkje funne</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="814"/>
        <source>The specified diskimage file %1 does not exist.</source>
        <translation>Den valde biletfila %1 finst ikkje.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="828"/>
        <source>Kernel file not found</source>
        <translation>Kjernefil ikkje funne</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="829"/>
        <source>The specified kernel file %1 does not exist.</source>
        <translation>Den valde kernel-fila %1 finst ikkje.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="843"/>
        <source>Initrd file not found</source>
        <translation>Initrd-fil ikkje funne</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="844"/>
        <source>The specified initrd file %1 does not exist.</source>
        <translation>Den valde initrd-fila %1 finst ikkje.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="948"/>
        <source>%1 exists, overwrite?</source>
        <translation>%1 finst, overskrive?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="949"/>
        <source>The file %1 already exists. Press &apos;Yes to All&apos; to overwrite it and not be prompted again, &apos;Yes&apos; to overwrite files on an individual basis, and &apos;No&apos; to retain your existing version. If in doubt, press &apos;Yes to All&apos;.</source>
        <translation>Fila %1 finst allereie. Trykk &quot;Ja til alt&quot; for å overskrive og ikkje bli spurt igjen. &quot;Ja&quot; for å overskrive enkeltfiler, og &quot;Nei&quot; for å behalde den eksisterande versjonen. Om du er usikker, trykk &quot;Ja til alt&quot;.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="975"/>
        <source>%1 is out of space, abort installation?</source>
        <translation>%1 har ikkje meir ledig plass, avbryte installasjonen?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="976"/>
        <source>The directory %1 is out of space. Press &apos;Yes&apos; to abort installation, &apos;No&apos; to ignore this error and attempt to continue installation, and &apos;No to All&apos; to ignore all out-of-space errors.</source>
        <translation>Mappa %1 har ikkje meir ledig plass. Trykk &quot;Ja&quot; for å avbryte installasjonen, &quot;Nei&quot; for å ignorere denne feilen og prøve å halde fram med installasjonen, og &quot;Nei til alt&quot; for å ignorere alle feil som følgje av for lite ledig plass.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1070"/>
        <source>Locating kernel file in %1</source>
        <translation>Finn kjernefil i %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1121"/>
        <source>Copying kernel file from %1</source>
        <translation>Kopierar kjernefil frå %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1127"/>
        <source>Locating initrd file in %1</source>
        <translation>Finn initrd-fil i %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1168"/>
        <source>Copying initrd file from %1</source>
        <translation>Kopierar initrd-fil frå %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1174"/>
        <location filename="unetbootin.cpp" line="1254"/>
        <source>Extracting bootloader configuration</source>
        <translation>Hentar ut konfigurasjon om oppstartslastar</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1483"/>
        <location filename="unetbootin.cpp" line="1509"/>
        <source>&lt;b&gt;Extracting compressed iso:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Pakkar ut komprimert ISO-fil:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1763"/>
        <source>Copying file, please wait...</source>
        <translation>Kopierar fil, vent litt...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1764"/>
        <location filename="unetbootin.cpp" line="2578"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;b&gt;Kjelde:&lt;/b&gt; &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1765"/>
        <location filename="unetbootin.cpp" line="2579"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Mål:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1766"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; 0 bytes</source>
        <translation>&lt;b&gt;Kopiert:&lt;/b&gt; 0 bytes</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1804"/>
        <source>Extracting files, please wait...</source>
        <translation>Pakkar ut filer, vent litt...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1805"/>
        <source>&lt;b&gt;Archive:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Arkiv:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1806"/>
        <source>&lt;b&gt;Source:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Kjelde:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1807"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Mål:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1808"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; 0 of %1 files</source>
        <translation>&lt;b&gt;Pakka ut:&lt;/b&gt; 0 av %1 filer</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1811"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; %1 (%2)</source>
        <translation>&lt;b&gt;Kjelde:&lt;/b&gt; %1 (%2)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1812"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1%2</source>
        <translation>&lt;b&gt;Mål:&lt;/b&gt; %1%2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1813"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; %1 of %2 files</source>
        <translation>&lt;b&gt;Pakka ut:&lt;/b&gt; %1 av %2 filer</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2577"/>
        <source>Downloading files, please wait...</source>
        <translation>Lastar ned filer, vent litt...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2580"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; 0 bytes</source>
        <translation>&lt;b&gt;Lasta ned:&lt;/b&gt; 0 bytes</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2704"/>
        <source>Download of %1 %2 from %3 failed. Please try downloading the ISO file from the website directly and supply it via the diskimage option.</source>
        <translation>Klarte ikkje laste ned %1 %2 frå %3. Prøv å lasta ned ISO-fila direkte frå nettstaden, og vel den med diskbilete-alternativet.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2727"/>
        <location filename="unetbootin.cpp" line="2742"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; %1 of %2</source>
        <translation>&lt;b&gt;Lasta ned:&lt;/b&gt; %1 av %2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2757"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; %1 of %2</source>
        <translation>&lt;b&gt;Kopiert:&lt;/b&gt; %1 av %2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2848"/>
        <source>Searching in &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>Søkjer i &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2852"/>
        <source>%1/%2 matches in &lt;a href=&quot;%3&quot;&gt;%3&lt;/a&gt;</source>
        <translation>%1/%2 treff i &lt;a href=&quot;%3&quot;&gt;%3&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3125"/>
        <source>%1 not found</source>
        <translation>Fann ikkje %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3126"/>
        <source>%1 not found. This is required for %2 install mode.
Install the &quot;%3&quot; package or your distribution&apos;s equivalent.</source>
        <translation>Fann ikkje %1. Dette er naudsynt for installasjonsmetoden %2.
Installer &quot;%3&quot;-pakken tilsvarande din distribusjon.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3417"/>
        <source>(Current)</source>
        <translation>(Noverande)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3418"/>
        <source>(Done)</source>
        <translation>(Ferdig)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3710"/>
        <source>Configuring grub2 on %1</source>
        <translation>Set opp grub2 på %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3722"/>
        <source>Configuring grldr on %1</source>
        <translation>Set opp grldr på %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3750"/>
        <source>Configuring grub on %1</source>
        <translation>Set opp grub på %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4048"/>
        <source>Installing syslinux to %1</source>
        <translation>Installerar syslinux til %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4083"/>
        <source>Installing extlinux to %1</source>
        <translation>Installerar extlinux til %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4243"/>
        <source>Syncing filesystems</source>
        <translation>Synkroniserar filsystem</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4248"/>
        <source>Setting up persistence</source>
        <translation>Set opp vedvarande lagring</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4293"/>
        <source>After rebooting, select the </source>
        <translation>Etter omstart, vel </translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4298"/>
        <source>After rebooting, select the USB boot option in the BIOS boot menu.%1
Reboot now?</source>
        <translation>Etter omstart, vel USB som oppstartsmetode i BIOS-menyen. %1
Starte på nytt no?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4301"/>
        <source>The created USB device will not boot off a Mac. Insert it into a PC, and select the USB boot option in the BIOS boot menu.%1</source>
        <translation>USB-eininga vil ikkje starta opp frå ein Mac. Kopla den til ein PC og vel USB-oppstartsvalet i BIOS-menyen.%1</translation>
    </message>
    <message>
        <source>
*IMPORTANT* Before rebooting, place an Ubuntu alternate (not desktop) install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.ubuntu.com</source>
        <translation type="obsolete">
*VIKTIG* Før omstart må du plassere eit diskbilete (ISO) av den alternative Ubuntu-versjonen (ikkje skrivebordsversjonen) i øvste mappa på harddisken eller USB-disken. Desse filene finn du på cdimage.ubuntu.com.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;mirrors.kernel.org&apos; when prompted for a server, and enter &apos;/centos/%1/os/%2&apos; when asked for the folder.</source>
        <translation type="obsolete">
*VIKTIG* Etter omstart, ignorer alle feilmeldingar, og vel tilbake om du blir spurt etter ein CD. Gå deretter til hovudmenyen, vel &quot;Start installasjon&quot;, vel &quot;Nettverk&quot; som kjelde og &quot;HTTP&quot; som protokoll. Skriv inn &quot;mirrors.kernel.org&quot; som server og &quot;/centos/%1/os/%2&quot; når du blir spurt etter mappe.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* Before rebooting, place a Debian install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.debian.org</source>
        <translation type="obsolete">
*VIKITIG* Før omstart må du plassere ei Debian-iso-fil i øverste mappa på harddisken eller USB-disken. Denne fila finn du på cdimage.debian.org</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.fedora.redhat.com&apos; when prompted for a server, and enter &apos;/pub/fedora/linux/development/%1/os&apos; when asked for the folder.</source>
        <translation type="obsolete">
*VIKTIG* Etter omstart, ignorer alle feilmeldingar, og vel tilbake om du blir spurt etter ein CD. Gå deretter til hovudmenyen, vel &quot;Start installasjon&quot;, vel &quot;Nettverk&quot; som kjelde og &quot;HTTP&quot; som protokoll. Skriv inn &quot;download.fedora.redhat.com&quot; som server og &quot;/pub/fedora/linux/development/%1/os&quot; når du blir spurt etter mappe.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.fedora.redhat.com&apos; when prompted for a server, and enter &apos;/pub/fedora/linux/releases/%1/Fedora/%2/os&apos; when asked for the folder.</source>
        <translation type="obsolete">
*VIKTIG* Etter omstart, ignorer alle feilmeldingar, og vel tilbake om du blir spurt etter ein CD. Gå deretter til hovudmenyen, vel &quot;Start installasjon&quot;, vel &quot;Nettverk&quot; som kjelde og &quot;HTTP&quot; som protokoll. Skriv inn &quot;download.fedora.redhat.com&quot; som server og &quot;/pub/fedora/linux/releases/%1/Fedora/%2/os&quot; når du blir spurt etter mappe.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.opensuse.org&apos; when prompted for a server, and enter &apos;/factory/repo/oss&apos; when asked for the folder.</source>
        <translation type="obsolete">
*VIKTIG* Etter omstart, ignorer alle feilmeldingar, og vel tilbake om du blir spurt etter ein CD. Gå deretter til hovudmenyen, vel &quot;Start installasjon&quot;, vel &quot;Nettverk&quot; som kjelde og &quot;HTTP&quot; som protokoll. Skriv inn &quot;download.opensuse.org&quot; som server og &quot;/factory/repo/oss&quot; når du blir spurt etter mappe.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.opensuse.org&apos; when prompted for a server, and enter &apos;/distribution/%1/repo/oss&apos; when asked for the folder.</source>
        <translation type="obsolete">
*VIKTIG* Etter omstart, ignorer alle feilmeldingar, og vel tilbake om du blir spurt etter ein CD. Gå deretter til hovudmenyen, vel &quot;Start installasjon&quot;, vel &quot;Nettverk&quot; som kjelde og &quot;HTTP&quot; som protokoll. Skriv inn &quot;download.opensuse.org&quot; som server og &quot;/distribution/%1/repo/oss&quot; når du blir spurt etter mappe.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="764"/>
        <source>== Select Distribution ==</source>
        <translation>== Vel distribusjon ==</translation>
    </message>
    <message>
        <source>== Select Version ==</source>
        <translation type="obsolete">== Vel versjon ==</translation>
    </message>
    <message>
        <source>Welcome to &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;, the Universal Netboot Installer. Usage:&lt;ol&gt;&lt;li&gt;Select a distribution and version to download from the list above, or manually specify files to load below.&lt;/li&gt;&lt;li&gt;Select an installation type, and press OK to begin installing.&lt;/li&gt;&lt;/ol&gt;</source>
        <translation type="obsolete">Velkomen til &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;, det universielle verktøyet for netboot-installasjon. Bruk:&lt;ol&gt;&lt;li&gt;Vel ein distribusjon og versjon du vil laste ned frå lista ovanfor, eller vel filer manuelt nedanfor.&lt;/li&gt;&lt;li&gt;Vel ein installasjonstype, og trykk OK for å starte installasjonen.&lt;/li&gt;&lt;/ol&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.archlinux.org/&quot;&gt;http://www.archlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Arch Linux is a lightweight distribution optimized for speed and flexibility.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for installation over the internet (FTP).</source>
        <translation type="obsolete">&lt;b&gt;Nettstad:&lt;/b&gt; &lt;a href=&quot;http://www.archlinux.org/&quot;&gt;http://www.archlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Skildring:&lt;/b&gt; Arch Linux er ein lett distribusjon laga for å vera rask og fleksibel.&lt;br/&gt;&lt;b&gt;Installasjonsnotat:&lt;/b&gt; Standardversjonen tillèt installasjon over internett (FTP).</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.backtrack-linux.org/&quot;&gt;http://www.backtrack-linux.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; BackTrack is a distribution focused on network analysis and penetration testing.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; BackTrack is booted and run in live mode; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Nettstad:&lt;/b&gt; &lt;a href=&quot;http://www.backtrack-linux.org/&quot;&gt;http://www.backtrack-linux.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Skildring:&lt;/b&gt; BackTrack er ein distribusjon med fokus på nettverksanalyse og gjennomtrengingstesting.&lt;br/&gt;&lt;b&gt;Installasjonsnotat:&lt;/b&gt; BackTrack vert køyrt direkte frå minnepinnen; installasjon er ikkje nødvendig.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.centos.org/&quot;&gt;http://www.centos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; CentOS is a free Red Hat Enterprise Linux clone.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation type="obsolete">&lt;b&gt;Nettstad:&lt;/b&gt; &lt;a href=&quot;http://www.centos.org/&quot;&gt;http://www.centos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Skildring:&lt;/b&gt; CentOS er ein gratis versjon av Red Hat Enterprise Linux.&lt;br/&gt;&lt;b&gt;Installasjonsnotat:&lt;/b&gt; Standardversjonen tillèt installasjon over internett (FTP) og lokal installasjon ved hjelp av førehandsnedlasta ISO-filer.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://clonezilla.org/&quot;&gt;http://clonezilla.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; CloneZilla is a distribution used for disk backup and imaging.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; CloneZilla is booted and run in live mode; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Nettstad:&lt;/b&gt; &lt;a href=&quot;http://clonezilla.org/&quot;&gt;http://clonezilla.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Skildring:&lt;/b&gt; Clonezilla er ein distribusjon brukt til tryggleikskopiering og spegling av harddisk.&lt;br/&gt;&lt;b&gt;Installasjonsnotat:&lt;/b&gt; Clonezilla vert køyrt direkte frå minnepinnen; installasjon er ikkje nødvendig.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://damnsmalllinux.org/&quot;&gt;http://damnsmalllinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Damn Small Linux is a minimalist distribution designed for older computers.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional.</source>
        <translation type="obsolete">&lt;b&gt;Nettstad:&lt;/b&gt; &lt;a href=&quot;http://damnsmalllinux.org/&quot;&gt;http://damnsmalllinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Skildring:&lt;/b&gt; Damn Small Linux er ein minimal distribusjon laga for eldre datamaskiner.&lt;br/&gt;&lt;b&gt;Installasjonsnotat:&lt;/b&gt; Live-versjonen lastar heile systemet inn i RAM og startar opp frå minnet. Installasjon er valfritt og ikkje nødvendig.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.debian.org/&quot;&gt;http://www.debian.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Debian is a community-developed Linux distribution that supports a wide variety of architectures and offers a large repository of packages.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The NetInstall version allows for installation over FTP. If you would like to use a pre-downloaded install iso, use the HdMedia option, and then place the install iso file on the root directory of your hard drive or USB drive</source>
        <translation type="obsolete">&lt;b&gt;Nettstad:&lt;/b&gt; &lt;a href=&quot;http://www.debian.org/&quot;&gt;http://www.debian.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Skildring:&lt;/b&gt;Debian er ein fellesskaputvikla Linux-distribusjon som støttar mange arkitekturarar og som tilbyr eit stort pakkearkiv.&lt;br/&gt;&lt;b&gt;Installasjonsnotat:&lt;/b&gt;NetInstall-versjonen tillèt installasjon over FTP. Dersom du vil bruka ein førehandsnedlasta iso-fil, kan du bruka HdMedia-alternativet, og plassere iso-fila i øvste mappa på harddisken eller USB-stasjonen.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.dreamlinux.com.br/&quot;&gt;http://www.dreamlinux.com.br&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Dreamlinux is a user-friendly Debian-based distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Nettstad:&lt;/b&gt; &lt;a href=&quot;http://www.dreamlinux.com.br/&quot;&gt;http://www.dreamlinux.com.br&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Skildring:&lt;/b&gt; Dreamlinux er ein brukarvenleg distribusjon basert på Debian.&lt;br/&gt;&lt;b&gt;Installasjonsnotat:&lt;/b&gt; Live-versjonen kan starta opp direkte frå USB-stasjonen, der installasjonsprogrammet kan startast.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freedrweb.com/livecd&quot;&gt;http://www.freedrweb.com/livecd&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Dr.Web AntiVirus is an anti-virus emergency kit to restore a system that broke due to malware.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which malware scans can be launched.</source>
        <translation type="obsolete">&lt;b&gt;Nettstad:&lt;/b&gt; &lt;a href=&quot;http://www.freedrweb.com/livecd&quot;&gt;http://www.freedrweb.com/livecd&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Skildring:&lt;/b&gt; Dr.Web AntiVirus er eit anitvirusverktøy for gjenoppretting av system øydelagd av skadeleg programvare.&lt;br/&gt;&lt;b&gt;Installasjonsnotat:&lt;/b&gt; Live-versjonen kan starta opp direkte frå USB-stasjonen, der ein kan søkja etter skadeleg programvare.</translation>
    </message>
</context>
<context>
    <name>unetbootinui</name>
    <message>
        <location filename="unetbootin.ui" line="20"/>
        <source>Unetbootin</source>
        <translation>Unetbootin</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="44"/>
        <location filename="unetbootin.ui" line="65"/>
        <source>Select from a list of supported distributions</source>
        <translation>Vel frå ei liste over støtta distribusjonar</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="47"/>
        <source>&amp;Distribution</source>
        <translation>&amp;Distribusjon</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="131"/>
        <source>Specify a disk image file to load</source>
        <translation>Vel diskbiletfila som skal brukast</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="134"/>
        <source>Disk&amp;image</source>
        <translation>Disk&amp;bilete</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="147"/>
        <source>Manually specify a kernel and initrd to load</source>
        <translation>Vel kjerne og initrd som skal brukast</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="150"/>
        <source>&amp;Custom</source>
        <translation>&amp;Sjølvvald</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="414"/>
        <location filename="unetbootin.ui" line="430"/>
        <source>Space to reserve for user files which are preserved across reboots. Works only for LiveUSBs for Ubuntu and derivatives. If value exceeds drive capacity, the maximum space available will be used.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="417"/>
        <source>Space used to preserve files across reboots (Ubuntu only):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="440"/>
        <source>MB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="503"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="506"/>
        <source>Return</source>
        <translation>Enter</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="513"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="516"/>
        <source>Esc</source>
        <translation>Escape</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="561"/>
        <source>Reboot Now</source>
        <translation>Start på nytt no</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="568"/>
        <source>Exit</source>
        <translation>Avslutt</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="660"/>
        <source>1. Downloading Files</source>
        <translation>1. Lastar ned filer</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="667"/>
        <source>2. Extracting and Copying Files</source>
        <translation>2. Pakkar ut og kopierer filer</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="674"/>
        <source>3. Installing Bootloader</source>
        <translation>3. Installerer oppstartslastar</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="681"/>
        <source>4. Installation Complete, Reboot</source>
        <translation>4. Installasjon ferdig, omstart</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="477"/>
        <location filename="unetbootin.ui" line="496"/>
        <source>Select the target drive to install to</source>
        <translation>Vel eininga som skal installerast til</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="480"/>
        <source>Dri&amp;ve:</source>
        <translation>&amp;Eining:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="451"/>
        <location filename="unetbootin.ui" line="470"/>
        <source>Select the installation target type</source>
        <translation>Vel type installasjonsmål</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="454"/>
        <source>&amp;Type:</source>
        <translation>&amp;Type:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="81"/>
        <source>Select the distribution version</source>
        <translation>Vel distribusjonsversjon</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="347"/>
        <source>Select disk image file</source>
        <translation>Vel diskbiletfil</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="286"/>
        <location filename="unetbootin.ui" line="350"/>
        <location filename="unetbootin.ui" line="375"/>
        <location filename="unetbootin.ui" line="400"/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="188"/>
        <source>Select the disk image type</source>
        <translation>Vel type diskbilete</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="243"/>
        <source>Specify a floppy/hard disk image, or CD image (ISO) file to load</source>
        <translation>Vel diskett/harddiskbiletet eller CD-biletfila (ISO) som skal brukast</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="207"/>
        <location filename="unetbootin.ui" line="258"/>
        <source>Specify a kernel file to load</source>
        <translation>Vel kjernefila som skal brukast</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="283"/>
        <source>Select kernel file</source>
        <translation>Vel kjernefil</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="293"/>
        <location filename="unetbootin.ui" line="312"/>
        <source>Specify an initrd file to load</source>
        <translation>Vel initrd-fila som skal brukast</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="372"/>
        <source>Select initrd file</source>
        <translation>Vel initrd-fil</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="397"/>
        <source>Select syslinux.cfg or isolinux.cfg file</source>
        <translation>Vel fila syslinux.cfg eller isolinux.cfg</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="226"/>
        <location filename="unetbootin.ui" line="321"/>
        <source>Specify parameters and options to pass to the kernel</source>
        <translation>Vel parameter og val som skal sendast til kjerna</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="210"/>
        <source>&amp;Kernel:</source>
        <translation>&amp;Kjerne:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="296"/>
        <source>Init&amp;rd:</source>
        <translation>Init&amp;rd:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="229"/>
        <source>&amp;Options:</source>
        <translation>&amp;Innstillingar</translation>
    </message>
</context>
<context>
    <name>uninstaller</name>
    <message>
        <location filename="main.cpp" line="156"/>
        <source>Uninstallation Complete</source>
        <translation>Avinstallasjon fullført</translation>
    </message>
    <message>
        <location filename="main.cpp" line="157"/>
        <source>%1 has been uninstalled.</source>
        <translation>%1 er avinstallert.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="322"/>
        <source>Must run as root</source>
        <translation>Må køyrast som root</translation>
    </message>
    <message>
        <location filename="main.cpp" line="324"/>
        <source>%2 must be run as root. Close it, and re-run using either:&lt;br/&gt;&lt;b&gt;sudo %1&lt;/b&gt;&lt;br/&gt;or:&lt;br/&gt;&lt;b&gt;su - -c &apos;%1&apos;&lt;/b&gt;</source>
        <translation>%2 må køyrast som root. Lukk den og køyr på nytt, ved å bruka anten:&lt;br/&gt;&lt;b&gt;sudo %1&lt;/b&gt;&lt;br/&gt;eller&lt;br/&gt;&lt;b&gt;su - -c «%1»&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="361"/>
        <source>%1 Uninstaller</source>
        <translation>Avinstallasjon av %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="362"/>
        <source>%1 is currently installed. Remove the existing version?</source>
        <translation>%1 er installert. Fjerna den noverande versjonen?</translation>
    </message>
</context>
</TS>
