/*
distrover.cpp from UNetbootin <http://unetbootin.sourceforge.net>
Copyright (C) 2007-2008 Geza Kovacs <geza0kovacs@gmail.com>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License at <http://www.gnu.org/licenses/> for more details.
*/

#ifndef ubuntuverlist
#define ubuntuverlist \
"12.04_NetInstall" << "12.04_NetInstall_x64" << "12.04_HdMedia" << "12.04_HdMedia_x64" << "12.04_Live" << "12.04_Live_x64" << \
"12.10_NetInstall" << "12.10_NetInstall_x64" << "12.10_HdMedia" << "12.10_HdMedia_x64" << "12.10_Live" << "12.10_Live_x64" << \
"13.04_NetInstall" << "13.04_NetInstall_x64" << "13.04_HdMedia" << "13.04_HdMedia_x64" << "13.04_Live" << "13.04_Live_x64" << \
"13.10_NetInstall" << "13.10_NetInstall_x64" << "13.10_HdMedia" << "13.10_HdMedia_x64" << "13.10_Live" << "13.10_Live_x64" << \
"14.04_NetInstall" << "14.04_NetInstall_x64" << "14.04_HdMedia" << "14.04_HdMedia_x64" << "14.04_Live" << "14.04_Live_x64" << \
"Daily_Live" << "Daily_Live_x64"
#endif

distroselect->addItem(unetbootin::tr("=#= Select Distribution =#="), (QStringList() << unetbootin::tr("=#= Select Version =#=") <<
unetbootin::tr("Welcome to <a href=\"https://notabug.org/tomlukeywood/Librebootin\">LibreBootin</a>, a libre Universal Netboot Installer. Usage:"
	"<ol><li>Select a distribution and version to download from the list above, or manually specify files to load below.</li>"
	"<li>Select an installation type, and press OK to begin installing.</li><p>Note:<br>LibreBootin is a fork of unetbootin that has been modified<br/>to include only Libre Distros.</p></ol>") <<
unetbootin::tr("=#= Select Version =#=")));

/*
 * i would add guix but there are no iso images only usb images
 * so if you want to install guix you can very easy with the dd program!
 * */

/*Trisquel*/
distroselect->addItem("Trisquel", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://www.trisquel.info/\">http://www.trisquel.info</a><br/>"
"<b>Description:<br></b>Trisquel GNU/Linux is a fully free operating system for home users, small enterprises and educational centers.<br/>") << "Trisquel_32-bit"));

/*Trisquel Mini*/
distroselect->addItem("Trisquel_Mini", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://trisquel.info/en/wiki/trisquel-mini/\">http://trisquel.info/en/wiki/trisquel-mini/</a><br/>"
"<b>Description:<br></b>Trisquel Mini is a lightweight version of Trisquel running the LXDE desktop environment.<br/>") << "Trisquel-Mini_32-bit"));

/*Blag*/
distroselect->addItem("Blag", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://www.blagblagblag.org/\">http://www.blagblagblag.org/</a><br/>"
"<b>Description:<br></b>Blag is an operating system with a suite of graphics, internet, audio, video, office and peer to peer file sharing applications.<br/>") << "Blag_32-bit"));

/*Debian*/
/*distroselect->addItem("Debian", (QStringList() << "Stable_NetInstall" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://www.debian.org/\">http://www.debian.org</a><br/>"
	"<b><h2>WARNING:</h2><p>Debian is made of compleatly free software but it gives the option to install non-free software. if you are using debian make sure you dont install any non-free software as this will limit your freedom.</p>Description:</b> Debian is a community-developed Linux distribution that supports a wide variety of architectures and offers a large repository of packages.<br/>"
	"<b>Install Notes:</b> The NetInstall version allows for installation over FTP. If you would like to use a pre-downloaded install iso, use the HdMedia option, and then place the install iso file on the root directory of your hard drive or USB drive") <<
"Stable_NetInstall" << "Stable_NetInstall_x64" << "Stable_HdMedia" << "Stable_HdMedia_x64" << "Testing_NetInstall" << "Testing_NetInstall_x64" << "Testing_HdMedia" << "Testing_HdMedia_x64" << "Unstable_NetInstall" << "Unstable_NetInstall_x64" << "Unstable_HdMedia" << "Unstable_HdMedia_x64"));
//	"Stable_NetInstall" << "Stable_NetInstall_x64" << "Stable_Live" << "Testing_NetInstall" << "Testing_NetInstall_x64" << "Testing_Live" << "Unstable_NetInstall" << "Unstable_NetInstall_x64" << "Unstable_Live"));
*/
/*Dragora*/
distroselect->addItem("Dragora", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://www.dragora.org\">http://www.dragora.org</a><br/>"
"<b>Description:<br></b>Dragora is a trustworthy GNU/Linux-Libre distribution based on the concept of simplicity with the goal of being a multi-purpose operating system. Dragora respects the freedom of the user with the values of free software and provides control to those who use it. It is developed entirely by volunteers and it is published under the terms of the GNU General Public License. <br/>") << "Dragora_32-bit"));

/*Dynebolic*/
distroselect->addItem("Dynebolic", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://www.dyne.org/software/dynebolic/\">http://www.dyne.org/software/dynebolic/</a><br/>"
"<b>Description:<br></b><br>Free software operating system for media activists, artists and creatives. Dynebolic is  as a practical tool for multimedia production: you can manipulate and broadcast both sound and video with tools to record, edit, encode and stream, having automatically recognized most device and peripherals: audio, video, TV, network cards, firewire, usb and more; all using only free software!<br/>") << "Dynebolic_32-bit"));

/*Musix*/
distroselect->addItem("Musix", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://musixdistro.wordpress.com/\">http://musixdistro.wordpress.com/</a><br/>"
"<b>Description:<br></b>Musix, a GNU+Linux distribution based on Knoppix, with special emphasis on audio production.<br/>") << "Musix_32-bit"));

/*Parabola*/
distroselect->addItem("Parabola", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://www.parabola.nu/\">http://www.parabola.nu/</a><br/>"
"<b>Description:<br></b>The Parabola project is a community driven effort to provide a fully free (as in freedom) operating system that is simple and lightweight. Derived from Arch Linux, Parabola GNU/Linux-libre provides packages from Arch that meet the Free System Distribution Guidelines (FSDG) and replacements for the packages that don't meet this requirement. Packages are provided for the i686, x86-64, and mips64el architectures.<br/>") << "Parabola_32-bit"));

/*Ututo*/
distroselect->addItem("Ututo", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://www.ututo.org/descargas/\">http://www.ututo.org/descargas/</a><br/>"
"<b>Description:<br></b>Ututo XS, a GNU/Linux distribution based on Gentoo. It was the first fully free GNU/Linux system recognized by the GNU Project.<br/>") << "Ututo_32-bit"));

/*Gnewsense*/
distroselect->addItem("gNewSense", (QStringList() << "Latest_Live" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://www.gnewsense.org/\">http://www.gnewsense.org</a><br/>"
	"<b>Description:</b> gNewSense is an FSF-endorsed distribution based on Ubuntu with all non-free components removed.<br/>"
	"<b>Install Notes:</b> The Live version allows for booting in Live mode, from which the installer can optionally be launched.") <<
"Latest_Live"));

/*Guix SD*/
/*guix will be included when its distributed as a iso file
distroselect->addItem("Guix_SD", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"https://www.gnu.org/software/guix/\">https://www.gnu.org/software/guix/</a><br/>"
	"<b>Description:</b><br/>Guix System Distribution is an advanced GNU/Linux distro built on top of GNU Guix (pronounced \"Geeks\"),<br/>"
	"a purely functional package manager for the GNU system.<br/>"
	) <<
"Guix_SD"));
*/

/*64 bit
 * 
 * */

/*Trisquel 64-bit*/
distroselect->addItem("Trisquel_64-bit", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://www.trisquel.info/\">http://www.trisquel.info</a><br/>"
"<b>Description:<br></b>Trisquel GNU/Linux is a fully free operating system for home users, small enterprises and educational centers.<br/>") << "Trisquel_64-bit"));

/*Trisquel Mini 64-bit*/
distroselect->addItem("Trisquel_Mini_64-bit", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://trisquel.info/en/wiki/trisquel-mini/\">http://trisquel.info/en/wiki/trisquel-mini/</a><br/>"
"<b>Description:<br></b>Trisquel Mini is a lightweight version of Trisquel running the LXDE desktop environment.<br/>") << "Trisquel-Mini_64-bit"));

/*Blag 64-bit*/
distroselect->addItem("Blag_64-bit", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://www.blagblagblag.org/\">http://www.blagblagblag.org/</a><br/>"
"<b>Description:<br></b>Blag is an operating system. Blag has a suite of graphics, internet, audio, video, office, and peer to peer file sharing applications. you can replace a windoz installation with blag.<br/>") << "Blag_64-bit"));

/*Dragora 64-bit*/
distroselect->addItem("Dragora_64-bit", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://www.dragora.org\">http://www.dragora.org</a><br/>"
"<b>Description:<br></b>Dragora is a trustworthy GNU/Linux-Libre distribution based on the concept of simplicity with the goal of being a multi-purpose operating system. Dragora respects the freedom of the user with the values of free software and provides control to those who use it. It is developed entirely by volunteers and it is published under the terms of the GNU General Public License. <br/>") << "Dragora_64-bit"));

/*Ututo 64-bit*/
distroselect->addItem("Ututo_64-bit", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://www.ututo.org/descargas/\">http://www.ututo.org/descargas/</a><br/>"
"<b>Description:<br></b>Ututo XS, a GNU/Linux distribution based on Gentoo. It was the first fully free GNU/Linux system recognized by the GNU Project.<br/>") << "Ututo_64-bit"));

/*Gnewsense 64-bit*/
distroselect->addItem("gNewSense_64-bit", (QStringList() << "Latest_Live_64-bit" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://www.gnewsense.org/\">http://www.gnewsense.org</a><br/>"
	"<b>Description:</b> gNewSense is an FSF-endorsed distribution based on Ubuntu with all non-free components removed.<br/>"
	"<b>Install Notes:</b> The Live version allows for booting in Live mode, from which the installer can optionally be launched.") <<
"Latest_Live_64-bit"));

/*Liberty BSD*/
distroselect->addItem("LibertyBSD_64-bit", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"http://www.libertybsd.net/\">http://www.libertybsd.net/</a><br/>"
"<b>Description:<br></b>"
"OpenBSD ships with several pieces of non-free, binary only firmware in the base system, and depending on the hardware detected, by default a script will download more at first boot,<br/> without informing the user of this.<br/>"
"While there may be good reasons for including this firmware,<br/> with a default installation you might end up running some of these non-free programs without even knowing it.<br/>"
"That's why I decided to make a \"deblobbed\" version of OpenBSD. So that you can get all of the benefits of OpenBSD,<br/>while being sure that there are no non-free blobs lurking in the depths of your system.<br/>This version is called LibertyBSD.</br>") << "LibertyBSD_64-bit"));

/*Guix SD 64-bit*/
/*
 *guix will be included when its distributed as a iso file
distroselect->addItem("Guix_SD_64-bit", (QStringList() << "core" <<
unetbootin::tr("<b>Homepage:</b> <a href=\"https://www.gnu.org/software/guix/\">https://www.gnu.org/software/guix/</a><br/>"
	"<b>Description:</b><br/>Guix System Distribution is an advanced GNU/Linux distro built on top of GNU Guix (pronounced \"Geeks\"),<br/>"
	"a purely functional package manager for the GNU system.<br/>"
	) <<
"Guix_SD_64-bit"));
*/
