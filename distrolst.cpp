/*
distrolst.cpp from UNetbootin <http://unetbootin.sourceforge.net>
Copyright (C) 2007-2008 Geza Kovacs <geza0kovacs@gmail.com>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License at <http://www.gnu.org/licenses/> for more details.
*/

#ifndef debianrelnamereplace
#define debianrelnamereplace \
	relname \
	.replace("unstable", "sid") \
    .replace("testing", "jessie") \
    .replace("stable", "wheezy");
#endif

#ifndef ubunturelnamereplace
#define ubunturelnamereplace \
	relname \
	.replace("14.04", "trusty") \
	.replace("13.10", "saucy") \
	.replace("13.04", "raring") \
	.replace("12.10", "quantal") \
	.replace("12.04", "precise") \
	.replace("11.10", "oneiric") \
	.replace("11.04", "natty") \
	.replace("10.10", "maverick") \
	.replace("10.04", "lucid") \
	.replace("9.10", "karmic") \
	.replace("9.04", "jaunty") \
	.replace("8.10", "intrepid") \
	.replace("8.04", "hardy") \
	.replace("7.10", "gutsy") \
	.replace("7.04", "feisty") \
	.replace("6.10", "edgy") \
	.replace("6.06", "dapper");
#endif

/*non-free*//*#ifndef ubuntunetinst
#define ubuntunetinst \
	if (ishdmedia) \
	{ \
		ubunturelnamereplace \
		downloadfile(QString("http://archive.ubuntu.com/ubuntu/dists/%1/main/installer-%2/current/images/hd-media/vmlinuz").arg(relname, cpuarch), QString("%1ubnkern").arg(targetPath)); \
		downloadfile(QString("http://archive.ubuntu.com/ubuntu/dists/%1/main/installer-%2/current/images/hd-media/initrd.gz").arg(relname, cpuarch), QString("%1ubninit").arg(targetPath)); \
		postinstmsg = unetbootin::tr("\n*IMPORTANT* Before rebooting, place an Ubuntu alternate (not desktop) install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.ubuntu.com"); \
	} \
	else if (isnetinstall) \
	{ \
		ubunturelnamereplace \
		downloadfile(QString("http://archive.ubuntu.com/ubuntu/dists/%1/main/installer-%2/current/images/netboot/ubuntu-installer/%2/linux").arg(relname, cpuarch), QString("%1ubnkern").arg(targetPath)); \
		downloadfile(QString("http://archive.ubuntu.com/ubuntu/dists/%1/main/installer-%2/current/images/netboot/ubuntu-installer/%2/initrd.gz").arg(relname, cpuarch), QString("%1ubninit").arg(targetPath)); \
	}
#endif
*/
/*non-free
#ifdef AUTOSUPERGRUBDISK

if (nameDistro == "Auto Super Grub Disk")
{
	instIndvfl("memdisk", QString("%1ubnkern").arg(targetPath));
	instIndvfl("asgd.img", QString("%1ubninit").arg(targetPath));
}

if (nameDistro == "Super Grub Disk")
{
        instIndvfl("memdisk", QString("%1ubnkern").arg(targetPath));
        instIndvfl("sgd.img", QString("%1ubninit").arg(targetPath));
}

#endif
*/
/*non-free*//*
#ifdef EEEPCLOS

if (nameDistro == "EeePCLinuxOS")
{
	downloadfile(QString("http://www.eeepclinuxos.com/eeepclos-%1.iso").arg(relname), isotmpf);
	extractiso(isotmpf);
}

#endif
*/
/*
#ifdef EEEUBUNTU

if (nameDistro == "Ubuntu Eee")
{
	downloadfile(QString("http://lubi.sourceforge.net/ubuntu-eee-%1.iso").arg(relname), isotmpf);
	extractiso(isotmpf);
}

#endif
*/
/*non-free
#ifdef ELIVE

if (nameDistro == "Elive")
{
	if (relname == "unstable")
		relname = "development";
	downloadfile(fileFilterNetDir(QStringList() << 
	"http://elive.icedslash.com/isos/"+relname+"/" <<
	"http://elive.leviathan-avc.com/"+relname+"/" <<
	"http://elive.jumbef.net/"+relname+"/" <<
	"http://elive.homogenica.com/"+relname+"/" <<
	"http://elive.evryanz.net/isos/"+relname+"/" <<
	"http://elive.7ds.pl/isos/"+relname+"/"
	, 524288000, 1048576000, QList<QRegExp>() << 
	QRegExp(".iso$", Qt::CaseInsensitive) << 
	QRegExp("elive\\S{0,}.iso$", Qt::CaseInsensitive)
    ), isotmpf);
	extractiso(isotmpf);
}

#endif
*/
/*FREEDOM!!!*/
#ifdef GNEWSENSE

if (nameDistro == "gNewSense")
{
	downloadfile(QString("http://cdimage.gnewsense.org/gnewsense-livecd-%1.iso").arg(relname), isotmpf);
	extractiso(isotmpf);
}

#endif

#ifdef KIWILINUX
;
/*commented out becuause non-free
if (nameDistro == "Kiwi Linux")
{
        downloadfile(QString("http://depo.osn.ro/content/distributii/linux/romanesti/kiwilinux-%1.iso").arg(relname), isotmpf);
        extractiso(isotmpf);

}
*/


#endif

#ifdef NIMBLEX
;
/*commented out becuause non-free
if (nameDistro == "NimbleX")
{
	downloadfile("http://public.nimblex.net/Download/NimbleX-latest.iso", isotmpf);
	extractiso(isotmpf);
}
*/
#endif

#ifdef SLITAZ
;
/*commented out becuause non-free
if (nameDistro == "SliTaz")
{
	if (relname == "webboot")
	{
		instIndvfl("gpxe", QString("%1ubnkern").arg(targetPath));
		kernelOpts = "url=http://mirror.slitaz.org/pxe/pxelinux.0";
		slinitrdLine = "";
		initrdLine = "";
		initrdOpts = "";
		initrdLoc = "";
//		downloadfile("http://mirror.slitaz.org/boot/slitaz-boot.iso", isotmpf);
//		extractiso(isotmpf);
	}
	else
	{
		downloadfile(fileFilterNetDir(QStringList() <<
		QString("http://mirror.slitaz.org/iso/%1/").arg(relname)
		, 3072000, 1048576000, QList<QRegExp>() <<
		QRegExp("^slitaz", Qt::CaseInsensitive) <<
		QRegExp(".iso$", Qt::CaseInsensitive) <<
		QRegExp("^slitaz-\\S{1,}.iso$", Qt::CaseInsensitive)
		), isotmpf);
		extractiso(isotmpf);
	}

}
*/
#endif

#ifdef XPUD
;
/*commented out becuause non-free
if (nameDistro == "xPUD")
{
	if (relname == "stable")
	{
		downloadfile("http://xpud.org/xpud-latest-iso.php", isotmpf);
	}
	else if (relname == "unstable")
	{
		downloadfile("http://xpud.org/xpud-latest-snapshot.php", isotmpf);
	}
	extractiso(isotmpf);
}
*/
#endif

#ifdef STDUNETBOOTIN
if (nameDistro == "Trisquel")
{
cpuarch = "i686";
relname = "trisquel";
downloadfile(QString("http://mirror.fsf.org/trisquel-images/%1_7.0_%2.iso").arg(relname, cpuarch), isotmpf);/*always i686 for some reason*/
extractiso(isotmpf);
}

if (nameDistro == "Trisquel_Mini")
{
cpuarch = "i686";
relname = "trisquel-mini";
downloadfile(QString("http://mirror.fsf.org/trisquel-images/%1_7.0_%2.iso").arg(relname, cpuarch), isotmpf);
extractiso(isotmpf);
}

if (nameDistro == "Blag")
{
cpuarch = "i686";
relname = "Blag";
downloadfile(QString("ftp://blag.fsf.org/140000/en/iso/%1-140k-%2.iso").arg(relname, cpuarch), isotmpf);
extractiso(isotmpf);
}

if (nameDistro == "Dragora")
{
cpuarch = "i486";
relname = "dragora";
downloadfile(QString("http://mirror.fsf.org/dragora-images/iso-2.2/%1-2.2-%2.iso").arg(relname, cpuarch), isotmpf);
extractiso(isotmpf);
}

if (nameDistro == "Dynebolic")
{
cpuarch = "i386";
relname = "dynebolic";
downloadfile(QString("http://mirror.fsf.org/dynebolic/dynebolic-3.0.0-beta.iso").arg(relname, cpuarch), isotmpf);
extractiso(isotmpf);
}

if (nameDistro == "Musix")
{
cpuarch = "i386";
relname = "Musix";
downloadfile(QString("http://musix.najval.net/musix/isos/MUSIX_GNU+Linux_3.0.1_Stable.iso").arg(relname, cpuarch), isotmpf);
extractiso(isotmpf);
}

if (nameDistro == "Parabola")
{
cpuarch = "i386";
relname = "Parabola";
/*http://repo.parabola.nu/iso/latest/parabola-2014.10.07-dual.iso currently not working*/
downloadfile(QString("http://alfplayer.com/parabola/iso/latest/parabola-2014.10.07-dual.iso").arg(relname, cpuarch), isotmpf);
extractiso(isotmpf);
}

if (nameDistro == "Ututo")
{
cpuarch = "i386";
relname = "Ututo";
downloadfile(QString("http://e7.ututo.org/isos/XS2012-04/UTUTO-XS-2012-04-Vivo-intel32.iso").arg(relname, cpuarch), isotmpf);
extractiso(isotmpf);
}

/* not included
 * if (nameDistro == "Debian")
{
	if (isarch64){cpuarch = "amd64";}else{cpuarch = "i386";}if (islivecd){
		debianrelnamereplace
		downloadfile(QString("http://live.debian.net/cdimage/%1-builds/current/%2/debian-live-%1-%2-gnome-desktop.iso").arg(relname, cpuarch), isotmpf);
		extractiso(isotmpf);}else if (ishdmedia){
		downloadfile(QString("http://ftp.debian.org/debian/dists/%1/main/installer-%2/current/images/hd-media/vmlinuz").arg(relname, cpuarch), QString("%1ubnkern").arg(targetPath));
		downloadfile(QString("http://ftp.debian.org/debian/dists/%1/main/installer-%2/current/images/hd-media/initrd.gz").arg(relname, cpuarch), QString("%1ubninit").arg(targetPath));
		postinstmsg = unetbootin::tr("\n*IMPORTANT* Before rebooting, place a Debian install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.debian.org");}else{
		downloadfile(QString("http://ftp.debian.org/debian/dists/%1/main/installer-%2/current/images/netboot/debian-installer/%2/linux").arg(relname, cpuarch), QString("%1ubnkern").arg(targetPath));
		downloadfile(QString("http://ftp.debian.org/debian/dists/%1/main/installer-%2/current/images/netboot/debian-installer/%2/initrd.gz").arg(relname, cpuarch), QString("%1ubninit").arg(targetPath));}
}
*/

/*
 * old gnewsense code
 *  downloadfile(fileFilterNetDir(QStringList() << 
	"http://cdimage.gnewsense.org/" << 
	"http://heanet.archive.gnewsense.org/gnewsense/cdimage/" <<
	"http://mirror.softwarelibre.nl/gnewsense/cdimage/"
	, 61440000, 1048576000, QList<QRegExp>() << 
	QRegExp("gnewsense\\S{0,}livecd\\S{0,}.iso$", Qt::CaseInsensitive) << 
	QRegExp("livecd\\S{0,}.iso$", Qt::CaseInsensitive) <<
	QRegExp("gnewsense\\S{0,}.iso$", Qt::CaseInsensitive) <<
	QRegExp(".iso$", Qt::CaseInsensitive)
	), isotmpf);
 * */

/*LIBRE!*/
if (nameDistro == "gNewSense")
{
	cpuarch = "i386";
    relname = "gnewsense";
    downloadfile(QString("http://cdimage.gnewsense.org/%1-live-3.1-%2-gnome.iso").arg(relname, cpuarch), isotmpf);
	extractiso(isotmpf);
}

/*if (nameDistro == "Guix_SD")
{
	cpuarch = "i686";
	relname = "guixsd"
}
*/

/*
 * 64-bit versions
 * */

if (nameDistro == "Trisquel_64-bit")
{
cpuarch = "amd64";
relname = "trisquel";
downloadfile(QString("http://mirror.fsf.org/trisquel-images/%1_7.0_%2.iso").arg(relname, cpuarch), isotmpf);
extractiso(isotmpf);
}

if (nameDistro == "Trisquel_Mini_64-bit")
{

cpuarch = "amd64";
relname = "trisquel-mini";
downloadfile(QString("http://mirror.fsf.org/trisquel-images/%1_7.0_%2.iso").arg(relname, cpuarch), isotmpf);
extractiso(isotmpf);
}
if (nameDistro == "Blag_64-bit")
{
cpuarch = "x86_64";
relname = "Blag";
downloadfile(QString("ftp://blag.fsf.org/140000/en/iso/%1-140k-%2.iso").arg(relname, cpuarch), isotmpf);
extractiso(isotmpf);
}
if (nameDistro == "Dragora_64-bit")
{
cpuarch = "x86_64";
relname = "dragora";
downloadfile(QString("http://mirror.fsf.org/dragora-images/iso-2.2/%1-2.2-%2.iso").arg(relname, cpuarch), isotmpf);
extractiso(isotmpf);
}
if (nameDistro == "Ututo_64-bit")
{
cpuarch = "x86_64";
relname = "Ututo";
downloadfile(QString("http://e7.ututo.org/isos/XS2012-04/UTUTO-XS-2012-04-Vivo-intel64.iso").arg(relname, cpuarch), isotmpf);
extractiso(isotmpf);
}
/*LIBRE!*/
if (nameDistro == "gNewSense_64-bit")
{
cpuarch = "x86_64";
relname = "GnewSense";
downloadfile(QString("cdimage.gnewsense.org/gnewsense-live-3.1-amd64-gnome.iso").arg(relname, cpuarch), isotmpf);
extractiso(isotmpf);
}

if (nameDistro == "LibertyBSD_64-bit")
{
cpuarch = "x86_64";
relname = "LibertyBSD";
downloadfile(QString("http://delwink.com/pub/LibertyBSD/5.6/amd64/install56.iso").arg(relname, cpuarch), isotmpf);
extractiso(isotmpf);	
}

#endif
