UNetbootin Source Revision 608
Copyright Geza Kovacs
Homepage at http://unetbootin.sourceforge.net
Licensed under the GNU GPL v2 and above, components from other projects are licensed under their respective licenses
Build generated on Sun Jun  1 20:37:20 PDT 2014

Download using git:
git clone https://github.com/gkovacs/unetbootin.git
cd unetbootin
git checkout 8a89df4a0f507582d340053b4d3f64ad27518368

Download using bzr:
bzr branch http://bazaar.launchpad.net/~vcs-imports/unetbootin/trunk -r608

Build instructions at http://sourceforge.net/apps/trac/unetbootin/wiki/compile

sed -i '/^RESOURCES/d' unetbootin.pro
lupdate-qt4 unetbootin.pro
lrelease-qt4 unetbootin.pro
qmake-qt4 "DEFINES += NOSTATIC" "RESOURCES -= unetbootin.qrc"
make

NOTICE:
this version of unetbootin has been modified
to only include libre gnu/linux distros

the modified files are
distrolst.cpp and distrover.cpp

also thanks to heather on trisquel
forms for correcting the spelling and
grammer for this program:
http://trisquel.info/en/users/heather
