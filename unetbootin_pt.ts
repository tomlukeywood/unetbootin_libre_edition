<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pt_PT">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="266"/>
        <source>LeftToRight</source>
        <translation>EsquerdaParaDireita</translation>
    </message>
</context>
<context>
    <name>unetbootin</name>
    <message>
        <location filename="unetbootin.cpp" line="213"/>
        <location filename="unetbootin.cpp" line="314"/>
        <location filename="unetbootin.cpp" line="315"/>
        <location filename="unetbootin.cpp" line="384"/>
        <location filename="unetbootin.cpp" line="558"/>
        <location filename="unetbootin.cpp" line="3436"/>
        <location filename="unetbootin.cpp" line="3449"/>
        <location filename="unetbootin.cpp" line="3631"/>
        <location filename="unetbootin.cpp" line="4291"/>
        <source>Hard Disk</source>
        <translation>Disco rígido</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="215"/>
        <location filename="unetbootin.cpp" line="311"/>
        <location filename="unetbootin.cpp" line="312"/>
        <location filename="unetbootin.cpp" line="386"/>
        <location filename="unetbootin.cpp" line="562"/>
        <location filename="unetbootin.cpp" line="728"/>
        <location filename="unetbootin.cpp" line="748"/>
        <location filename="unetbootin.cpp" line="1002"/>
        <location filename="unetbootin.cpp" line="1577"/>
        <location filename="unetbootin.cpp" line="1664"/>
        <location filename="unetbootin.cpp" line="2593"/>
        <location filename="unetbootin.cpp" line="2636"/>
        <location filename="unetbootin.cpp" line="3440"/>
        <location filename="unetbootin.cpp" line="3466"/>
        <location filename="unetbootin.cpp" line="3635"/>
        <location filename="unetbootin.cpp" line="3959"/>
        <location filename="unetbootin.cpp" line="4295"/>
        <source>USB Drive</source>
        <translation>Disco USB</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="216"/>
        <location filename="unetbootin.cpp" line="233"/>
        <location filename="unetbootin.cpp" line="234"/>
        <location filename="unetbootin.cpp" line="350"/>
        <location filename="unetbootin.cpp" line="676"/>
        <location filename="unetbootin.cpp" line="679"/>
        <location filename="unetbootin.cpp" line="680"/>
        <location filename="unetbootin.cpp" line="3525"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="217"/>
        <location filename="unetbootin.cpp" line="229"/>
        <location filename="unetbootin.cpp" line="230"/>
        <location filename="unetbootin.cpp" line="356"/>
        <location filename="unetbootin.cpp" line="676"/>
        <location filename="unetbootin.cpp" line="684"/>
        <location filename="unetbootin.cpp" line="685"/>
        <location filename="unetbootin.cpp" line="3517"/>
        <source>Floppy</source>
        <translation>Disquete</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="250"/>
        <location filename="unetbootin.cpp" line="256"/>
        <location filename="unetbootin.cpp" line="260"/>
        <location filename="unetbootin.cpp" line="264"/>
        <location filename="unetbootin.cpp" line="268"/>
        <location filename="unetbootin.cpp" line="274"/>
        <location filename="unetbootin.cpp" line="302"/>
        <source>either</source>
        <translation>cada(um)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="281"/>
        <source>LiveUSB persistence</source>
        <translation>Persistência</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="296"/>
        <source>FAT32-formatted USB drive</source>
        <translation>Disco USB formatado em FAT32</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="300"/>
        <source>EXT2-formatted USB drive</source>
        <translation>Disco USB formatado em EXT2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="676"/>
        <source>Open Disk Image File</source>
        <translation>Abrir imagem de disco</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="676"/>
        <source>All Files</source>
        <translation>Todos os ficheiros</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="694"/>
        <location filename="unetbootin.cpp" line="702"/>
        <location filename="unetbootin.cpp" line="710"/>
        <source>All Files (*)</source>
        <translation>Todos os ficheiros (*)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="694"/>
        <source>Open Kernel File</source>
        <translation>Abrir ficheiro Kernel</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="702"/>
        <source>Open Initrd File</source>
        <translation>Abrir ficheiro Initrd</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="710"/>
        <source>Open Bootloader Config File</source>
        <translation>Abrir ficheiro de configuração do carregador de arranque</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="732"/>
        <source>Insert a USB flash drive</source>
        <translation>Insira um dispositivo USB</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="733"/>
        <source>No USB flash drives were found. If you have already inserted a USB drive, try reformatting it as FAT32.</source>
        <translation>Nenhum dispositivo USB encontrado. Se já inseriu um dispositivo USB, tente formatá-lo como FAT32.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="752"/>
        <source>%1 not mounted</source>
        <translation>%1 não está montado</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="753"/>
        <source>You must first mount the USB drive %1 to a mountpoint. Most distributions will do this automatically after you remove and reinsert the USB drive.</source>
        <translation>Deve montar o dispositivo USB. A maioria das distribuições faz isto automaticamente após a remoção e reinserção do dispositivo.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="768"/>
        <source>Select a distro</source>
        <translation>Selecione uma distribuição</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="769"/>
        <source>You must select a distribution to load.</source>
        <translation>Tem que selecionar a distribuição a carregar.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="783"/>
        <source>Select a disk image file</source>
        <translation>Selecione uma imagem</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="784"/>
        <source>You must select a disk image file to load.</source>
        <translation>Tem que selecionar a imagem de disco a carregar.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="798"/>
        <source>Select a kernel and/or initrd file</source>
        <translation>Selecione o kernel e/ou ficheiro initrd</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="799"/>
        <source>You must select a kernel and/or initrd file to load.</source>
        <translation>Tem que selecionar um kernel e/ou o ficheiro initrd a carregar.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="813"/>
        <source>Diskimage file not found</source>
        <translation>Imagem não encontrada</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="814"/>
        <source>The specified diskimage file %1 does not exist.</source>
        <translation>A imagem especificada %1 não existe.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="828"/>
        <source>Kernel file not found</source>
        <translation>Ficheiro Kernel não encontrado</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="829"/>
        <source>The specified kernel file %1 does not exist.</source>
        <translation>O kernel %1 não existe.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="843"/>
        <source>Initrd file not found</source>
        <translation>Ficheiro initrd não encontrado</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="844"/>
        <source>The specified initrd file %1 does not exist.</source>
        <translation>O ficheiro initrd %1 não existe.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="948"/>
        <source>%1 exists, overwrite?</source>
        <translation>%1 existe, substituir ?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="949"/>
        <source>The file %1 already exists. Press &apos;Yes to All&apos; to overwrite it and not be prompted again, &apos;Yes&apos; to overwrite files on an individual basis, and &apos;No&apos; to retain your existing version. If in doubt, press &apos;Yes to All&apos;.</source>
        <translation>O ficheiro %1 já existe. Escolha &quot;Sim a tudo&quot; para substituir e não ser perguntado novamente. &quot;Sim&quot; para substituir individualmente e &quot;Não&quot; para manter a versão existente. Em caso de dúvida, escolha &quot;Sim a tudo&quot;.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="975"/>
        <source>%1 is out of space, abort installation?</source>
        <translation>%1 está sem espaço, cancelar instalação?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="976"/>
        <source>The directory %1 is out of space. Press &apos;Yes&apos; to abort installation, &apos;No&apos; to ignore this error and attempt to continue installation, and &apos;No to All&apos; to ignore all out-of-space errors.</source>
        <translation>O diretório %1 não possui espaço. Escolha &quot;Sim&quot; para abortar a instalação, &quot;Não&quot; para ignorar este erro e tentar a instalação e &quot;Não a tudo&quot; para ignorar os erros de falta de espaço.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1070"/>
        <source>Locating kernel file in %1</source>
        <translation>A localizar o ficheiro Kernel em %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1121"/>
        <source>Copying kernel file from %1</source>
        <translation>A copiar o ficheiro Kernel de %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1127"/>
        <source>Locating initrd file in %1</source>
        <translation>A localizar o ficheiro initrd em %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1168"/>
        <source>Copying initrd file from %1</source>
        <translation>A copiar o ficheiro initrd de %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1174"/>
        <location filename="unetbootin.cpp" line="1254"/>
        <source>Extracting bootloader configuration</source>
        <translation>A extrair a configuração do carregador de arranque</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1483"/>
        <location filename="unetbootin.cpp" line="1509"/>
        <source>&lt;b&gt;Extracting compressed iso:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;A extrair a ISO:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1763"/>
        <source>Copying file, please wait...</source>
        <translation>A copiar ficheiro. Por favor, aguarde...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1764"/>
        <location filename="unetbootin.cpp" line="2578"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>&lt;b&gt;Origem:&lt;/b&gt; &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1765"/>
        <location filename="unetbootin.cpp" line="2579"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Destino:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1766"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; 0 bytes</source>
        <translation>&lt;b&gt;Copiado:&lt;/b&gt; 0 bytes</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1804"/>
        <source>Extracting files, please wait...</source>
        <translation>A extrair ficheiros. Por favor, aguarde...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1805"/>
        <source>&lt;b&gt;Archive:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Arquivo:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1806"/>
        <source>&lt;b&gt;Source:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Origem:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1807"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Destino:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1808"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; 0 of %1 files</source>
        <translation>&lt;b&gt;Extraido(s):&lt;/b&gt; 0 de %1 ficheiros</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1811"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; %1 (%2)</source>
        <translation>&lt;b&gt;Origem:&lt;/b&gt; %1 (%2)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1812"/>
        <source>&lt;b&gt;Destination:&lt;/b&gt; %1%2</source>
        <translation>&lt;b&gt;Destino:&lt;/b&gt; %1%2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="1813"/>
        <source>&lt;b&gt;Extracted:&lt;/b&gt; %1 of %2 files</source>
        <translation>&lt;b&gt;Extraido:&lt;/b&gt; %1 de %2 ficheiros</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2577"/>
        <source>Downloading files, please wait...</source>
        <translation>A transferir ficheiros. Por favor, aguarde...</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2580"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; 0 bytes</source>
        <translation>&lt;b&gt;Transferido:&lt;/b&gt; 0 bytes</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2704"/>
        <source>Download of %1 %2 from %3 failed. Please try downloading the ISO file from the website directly and supply it via the diskimage option.</source>
        <translation>Falha ao transferir %1 %2 de %3. Tente transferir o ficheiro ISO na página web e utilize a opção imagem de disco.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2727"/>
        <location filename="unetbootin.cpp" line="2742"/>
        <source>&lt;b&gt;Downloaded:&lt;/b&gt; %1 of %2</source>
        <translation>&lt;b&gt;Transferido:&lt;/b&gt; %1 de %2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2757"/>
        <source>&lt;b&gt;Copied:&lt;/b&gt; %1 of %2</source>
        <translation>&lt;b&gt;Copiado:&lt;/b&gt; %1 de %2</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2848"/>
        <source>Searching in &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>A procurar em &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="2852"/>
        <source>%1/%2 matches in &lt;a href=&quot;%3&quot;&gt;%3&lt;/a&gt;</source>
        <translation>%1/%2 ocorrências em &lt;a href=&quot;%3&quot;&gt;%3&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3125"/>
        <source>%1 not found</source>
        <translation>%1 não encontrado</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3126"/>
        <source>%1 not found. This is required for %2 install mode.
Install the &quot;%3&quot; package or your distribution&apos;s equivalent.</source>
        <translation>%1 não foi encontrado. Este é necessário para o modo de instalação %2.
Instale o pacote &quot;%3&quot; ou o equivalente na sua distribuição.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3417"/>
        <source>(Current)</source>
        <translation>(Atual)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3418"/>
        <source>(Done)</source>
        <translation>(Concluído)</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3710"/>
        <source>Configuring grub2 on %1</source>
        <translation>A configurar grub2 em %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3722"/>
        <source>Configuring grldr on %1</source>
        <translation>A configurar grldr em %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="3750"/>
        <source>Configuring grub on %1</source>
        <translation>A configurar grub em %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4048"/>
        <source>Installing syslinux to %1</source>
        <translation>A instalar syslinux em %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4083"/>
        <source>Installing extlinux to %1</source>
        <translation>A instalar extlinux em %1</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4243"/>
        <source>Syncing filesystems</source>
        <translation>A sincronizar o sistema de ficheiros</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4248"/>
        <source>Setting up persistence</source>
        <translation>A definir memória persistente</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4293"/>
        <source>After rebooting, select the </source>
        <translation>Depois de reiniciar, selecione </translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4298"/>
        <source>After rebooting, select the USB boot option in the BIOS boot menu.%1
Reboot now?</source>
        <translation>Depois de reiniciar, seleccione a opção &quot;USB&quot; no menu de arranque da BIOS. %1
Reiniciar agora?</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="4301"/>
        <source>The created USB device will not boot off a Mac. Insert it into a PC, and select the USB boot option in the BIOS boot menu.%1</source>
        <translation>O dispositivo criado não arranca em Mac. Insira o dispositivo num PC e escolha a opção USB no menu de arranque da BIOS. %1</translation>
    </message>
    <message>
        <source>
*IMPORTANT* Before rebooting, place an Ubuntu alternate (not desktop) install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.ubuntu.com</source>
        <translation type="obsolete">
*IMPORTANTE* Antes de reiniciar, coloque um ficheiro de instalação do Ubuntu alternate (não desktop) no diretório raiz do disco rígido ou do disco USB. Estes ficheiros podem ser obtidos em cdimage.ubuntu.com</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;mirrors.kernel.org&apos; when prompted for a server, and enter &apos;/centos/%1/os/%2&apos; when asked for the folder.</source>
        <translation type="obsolete">
*IMPORTANTE* Depois de reiniciar, ignore quaisquer mensagens de erro e retroceda caso seja solicitado um CD. Vá até o menu principal, selecione a opção &quot;Iniciar instalação&quot;, escolha &quot;Rede&quot; como origem, &quot;HTTP&quot; como protocolo, &quot;mirrors.kernel.org&quot; como servidor e &quot;/centos/%1/os/%2&quot; como pasta.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* Before rebooting, place a Debian install iso file on the root directory of your hard drive or USB drive. These can be obtained from cdimage.debian.org</source>
        <translation type="obsolete">
*IMPORTANTE* Antes de reiniciar, coloque um ficheiro de instalação do Debian no diretório raiz do disco rígido ou do disco USB. Estes ficheiros podem ser obtidos em cdimage.debian.org</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.fedora.redhat.com&apos; when prompted for a server, and enter &apos;/pub/fedora/linux/development/%1/os&apos; when asked for the folder.</source>
        <translation type="obsolete">
*IMPORTANTE* Depois de reiniciar, ignore quaisquer mensagens de erro e retroceda caso seja solicitado um CD. Vá até o menu principal, selecione a opção &quot;Iniciar instalação&quot;, escolha &quot;Rede&quot; como origem, &quot;HTTP&quot; como protocolo, &quot;download.fedora.redhat.com&quot; como servidor e &quot;/pub/fedora/linux/development/%1/os&quot; como pasta.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.fedora.redhat.com&apos; when prompted for a server, and enter &apos;/pub/fedora/linux/releases/%1/Fedora/%2/os&apos; when asked for the folder.</source>
        <translation type="obsolete">
*IMPORTANTE* Depois de reiniciar, ignore quaisquer mensagens de erro e retroceda caso seja solicitado um CD. Vá até o menu principal, selecione a opção &quot;Iniciar instalação&quot;, escolha &quot;Rede&quot; como origem, &quot;HTTP&quot; como protocolo, &quot;download.fedora.redhat.com&quot; como servidor e &quot;/pub/fedora/linux/releases/%1/Fedora/%2/os&quot; como pasta.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.opensuse.org&apos; when prompted for a server, and enter &apos;/factory/repo/oss&apos; when asked for the folder.</source>
        <translation type="obsolete">
*IMPORTANTE* Depois de reiniciar, ignore quaisquer mensagens de erro e retroceda caso seja solicitado um CD. Vá até o menu principal, selecione a opção &quot;Iniciar instalação&quot;, escolha &quot;Rede&quot; como origem, &quot;HTTP&quot; como protocolo, &quot;download.opensuse.com&quot; como servidor e &quot;/factory/repo/oss&quot; como pasta.</translation>
    </message>
    <message>
        <source>
*IMPORTANT* After rebooting, ignore any error messages and select back if prompted for a CD, then go to the main menu, select the &apos;Start Installation&apos; option, choose &apos;Network&apos; as the source, choose &apos;HTTP&apos; as the protocol, enter &apos;download.opensuse.org&apos; when prompted for a server, and enter &apos;/distribution/%1/repo/oss&apos; when asked for the folder.</source>
        <translation type="obsolete">
*IMPORTANTE* Depois de reiniciar, ignore quaisquer mensagens de erro e retroceda caso seja solicitado um CD. Vá até o menu principal, selecione a opção &quot;Iniciar nstalação&quot;, escolha &quot;Rede&quot; como origem, &quot;HTTP&quot; como protocolo, &quot;download.opensuse.com&quot; como servidor e &quot;/distribution/%1/repo/oss&quot; como pasta.</translation>
    </message>
    <message>
        <location filename="unetbootin.cpp" line="764"/>
        <source>== Select Distribution ==</source>
        <translation>== Selecione a distribuição ==</translation>
    </message>
    <message>
        <source>== Select Version ==</source>
        <translation type="obsolete">== Selecione a versão ==</translation>
    </message>
    <message>
        <source>Welcome to &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;, the Universal Netboot Installer. Usage:&lt;ol&gt;&lt;li&gt;Select a distribution and version to download from the list above, or manually specify files to load below.&lt;/li&gt;&lt;li&gt;Select an installation type, and press OK to begin installing.&lt;/li&gt;&lt;/ol&gt;</source>
        <translation type="obsolete">Bem vindo ao &lt;a href=&quot;http://unetbootin.sourceforge.net/&quot;&gt;UNetbootin&lt;/a&gt;, o instalador universal de imagens. Utilização:&lt;ol&gt;&lt;li&gt;Selecione a distribuição e a versão a transferir na lista indicada ou indique manualmente, em baixo, os ficheiros a carregar.&lt;/li&gt;&lt;li&gt;Selecione o tipo de instalação e prima OK para iniciar a instalação.&lt;/li&gt;&lt;/ol&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.archlinux.org/&quot;&gt;http://www.archlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Arch Linux is a lightweight distribution optimized for speed and flexibility.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for installation over the internet (FTP).</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://www.archlinux.org/&quot;&gt;http://www.archlinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o Arch Linux é uma distribuição otimizada para velocidade e flexibilidade.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; a versão normal permite a instalação através da internet (FTP).</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.backtrack-linux.org/&quot;&gt;http://www.backtrack-linux.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; BackTrack is a distribution focused on network analysis and penetration testing.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; BackTrack is booted and run in live mode; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://www.backtrack-linux.org/&quot;&gt;http://www.backtrack-linux.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o BackTrack é uma distribuição focada na análise de redes e testes de vulnerabilidades.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; o BackTrack  arranca e corre em modo &quot;live&quot;; a instalação não é necessária.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.centos.org/&quot;&gt;http://www.centos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; CentOS is a free Red Hat Enterprise Linux clone.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://www.centos.org/&quot;&gt;http://www.centos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o CentOS é um clone da distribuição Red Hat Enterprise Linux.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; A versão normal permite a instalação através da internet (FTP) bem como a instalação através de ficheiros ISO.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://clonezilla.org/&quot;&gt;http://clonezilla.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; CloneZilla is a distribution used for disk backup and imaging.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; CloneZilla is booted and run in live mode; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://clonezilla.org/&quot;&gt;http://clonezilla.org/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o CloneZilla é uma distribuição utilizada para criar imagens e cópias de segurança de discos rígidos.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; o CloneZilla arranca e corre em modo &quot;live&quot;; a instalação não é necessária.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://damnsmalllinux.org/&quot;&gt;http://damnsmalllinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Damn Small Linux is a minimalist distribution designed for older computers.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version loads the entire system into RAM and boots from memory, so installation is not required but optional.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://damnsmalllinux.org/&quot;&gt;http://damnsmalllinux.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o Damn Small Linux é uma distribuição minimalista desenvolvida para computadores antigos.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; a versão &quot;Live&quot; carrega o sistema em RAM e arranca a partir da memória tornando a instalação opcional e não obrigatória.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.debian.org/&quot;&gt;http://www.debian.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Debian is a community-developed Linux distribution that supports a wide variety of architectures and offers a large repository of packages.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The NetInstall version allows for installation over FTP. If you would like to use a pre-downloaded install iso, use the HdMedia option, and then place the install iso file on the root directory of your hard drive or USB drive</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://www.debian.org/&quot;&gt;http://www.debian.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o Debian é uma distribuição desenvolvida pela comunidade Linux com suporte a diversas arquiteturas e que disponibiliza um vasto repositório de pacotes.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; a versão &quot;NetInstall&quot; permite a instalação através de FTP. Se pretende utilizar uma imagem de instalação, use a opção HdMedia e coloque-a no diretório raiz do disco rígido ou da unidade usb.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.dreamlinux.com.br/&quot;&gt;http://www.dreamlinux.com.br&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Dreamlinux is a user-friendly Debian-based distribution.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://www.dreamlinux.com.br/&quot;&gt;http://www.dreamlinux.com.br&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o Dreamlinux é uma distribuição baseada em Debian e muito fácil de utilizar.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; a versão &quot;Live&quot; permite o arranque no modo &quot;Live&quot;, a partir do qual pode iniciar a instalação.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freedrweb.com/livecd&quot;&gt;http://www.freedrweb.com/livecd&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Dr.Web AntiVirus is an anti-virus emergency kit to restore a system that broke due to malware.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which malware scans can be launched.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://www.freedrweb.com/livecd&quot;&gt;http://www.freedrweb.com/livecd&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o Dr.Web AntiVirus é um anti-virus de emergência para restaurar um sistema que ficou danificado devido a malware.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; a versão &quot;Live&quot; permite o arranque em modo &quot;Live&quot;, a partir do qual inicia a procura e a remoção do malware.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.elivecd.org/&quot;&gt;http://www.elivecd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Elive is a Debian-based distribution featuring the Enlightenment window manager.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://www.elivecd.org/&quot;&gt;http://www.elivecd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o Elive é uma distribuição baseada em Debian mas que utiliza o gestor de janelas Enlightenment.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; a versão &quot;Live&quot; permite o arranque no modo &quot;Live&quot;, a partir do qual pode iniciar a instalação.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://fedoraproject.org/&quot;&gt;http://fedoraproject.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Fedora is a Red Hat sponsored community distribution which showcases the latest cutting-edge free/open-source software.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://fedoraproject.org/&quot;&gt;http://fedoraproject.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o Fedora é uma distribuição desenvolvida pela comunidade e patrocinada pela RedHat e inclui as últimas novidades do mundo do software livre/aberto.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; a versão &quot;Live&quot; permite o arranque no modo &quot;Live&quot;, a partir do qual pode iniciar a instalação. A versão &quot;NetInstall&quot; permite a instalação através da internet (FTP), bem como a instalação através de ficheiros ISO.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freebsd.org/&quot;&gt;http://www.freebsd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeBSD is a general-purpose Unix-like operating system designed for scalability and performance.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default version allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://www.freebsd.org/&quot;&gt;http://www.freebsd.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o FreeBSD é um sistema operativo do tipo Unix, desenvolvido para a personalização e desempenho.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; a versão padrão permite a instalação através da internet (FTP), bem como através da utilização de ficheiros ISO.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.freedos.org/&quot;&gt;http://www.freedos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeDOS is a free MS-DOS compatible operating system.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; See the &lt;a href=&quot;http://fd-doc.sourceforge.net/wiki/index.php?n=FdDocEn.FdInstall&quot;&gt;manual&lt;/a&gt; for installation details.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://www.freedos.org/&quot;&gt;http://www.freedos.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o FreeDOS é um sistema operativo compatível com MS-DOS.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; Consulte o &lt;a href=&quot;http://fd-doc.sourceforge.net/wiki/index.php?n=FdDocEn.FdInstall&quot;&gt;manual&lt;/a&gt; para detalhes de instalação.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://freenas.org/&quot;&gt;http://www.freenas.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; FreeNAS is an embedded open source NAS (Network-Attached Storage) distribution based on FreeBSD.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The LiveCD version creates a RAM drive for FreeNAS, and uses a FAT formatted floppy disk or USB key for saving the configuration file. The embedded version allows installation to hard disk.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://freenas.org/&quot;&gt;http://www.freenas.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o FreeNAS é uma distribuição NAS (Network-Attached Storage) baseada em FreeBSD.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; a versão &quot;Live&quot; cria uma unidade RAM para o FreeNAS e utiliza uma disquete ou disco USB no formato FAT para gravar as configurações. A versão &quot;embedded&quot; permite a instalação no disco.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://frugalware.org/&quot;&gt;http://frugalware.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Frugalware is a general-purpose Slackware-based distro for advanced users.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The default option allows for both installation over the internet (FTP), or offline installation using pre-downloaded installation ISO files.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://frugalware.org/&quot;&gt;http://frugalware.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o Frugalware é uma distribuição baseada em Slackware e vocacionada para utilizadores experientes.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; a versão normal permite a instalação através da internet (FTP) bem como a instalação através da utilização de ficheiros ISO.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.geexbox.org/&quot;&gt;http://www.geexbox.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; GeeXboX is an Embedded Linux Media Center Distribution.&lt;br/&gt;</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://www.geexbox.org/&quot;&gt;http://www.geexbox.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; O GeeXboX é uma distribuição Embedded Linux Media Center.&lt;br/&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; gNewSense is an FSF-endorsed distribution based on Ubuntu with all non-free components removed.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Pagina inicial:&lt;/b&gt; &lt;a href=&quot;http://www.gnewsense.org/&quot;&gt;http://www.gnewsense.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; O gNewSense é uma distribuição apoiada pela FSF, baseada em Ubuntu mas que não inclui os compnentes com direitos de propriedade.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; A versão &quot;Live&quot; permite o arranque no modo &quot;Live&quot;, a partir do qual pode iniciar a instalação.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://gujin.sourceforge.net/&quot;&gt;http://gujin.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Gujin is a graphical boot manager which can bootstrap various volumes and files.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; Gujin simply boots and runs; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://gujin.sourceforge.net/&quot;&gt;http://gujin.sourceforge.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o Gujin é um gestor de arranque gráfico capaz de arrancar e gerir diversos discos e ficheiros.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; o Gujin simplesmente arranca e é utilizado. A instalação não é necessária.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://ftp.kaspersky.com/devbuilds/RescueDisk/&quot;&gt;http://ftp.kaspersky.com/devbuilds/RescueDisk/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Kaspersky Rescue Disk detects and removes malware from your Windows installation.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which malware scans can be launched.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://ftp.kaspersky.com/devbuilds/RescueDisk/&quot;&gt;http://ftp.kaspersky.com/devbuilds/RescueDisk/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o disco de recuperação Kaspersky deteta e remove o malware da sua instalação Windows.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; A versão &quot;Live&quot; permite arrancar no modo &quot;Live&quot;, a partir do qual a análise e remoção do malware é efetuada.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.kubuntu.org/&quot;&gt;http://www.kubuntu.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Kubuntu is an official Ubuntu derivative featuring the KDE desktop.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over FTP, and can install Kubuntu and other official Ubuntu derivatives. If you would like to use a pre-downloaded alternate (not desktop) install iso, use the HdMedia option, and then place the alternate install iso file on the root directory of your hard drive or USB drive</source>
        <translation type="obsolete">&lt;b&gt;Pagina inicial:&lt;/b&gt; &lt;a href=&quot;http://www.kubuntu.org/&quot;&gt;http://www.kubuntu.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o Kubuntu é uma distribuição oficial do Ubuntu, mas com o ambiente de trabalho KDE.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; A versão &quot;Live&quot; permite o arranque no modo &quot;Live&quot;, a partir do qual pode iniciar a instalação. A versão &quot;NetInstall&quot; permite a instalação através da internet (FTP) bem como a instalação através da utilização de ficheiros ISO. Se quiser utilizar a versão alternate (não desktop), utilize a opção HdMedia, e coloque o ficheiro ISO no diretório raiz do disco rígido ou dispositivo USB.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://linuxconsole.org/&quot;&gt;http://linuxconsole.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; LinuxConsole is a desktop distro to play games, easy to install, easy to use and fast to boot .&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The 1.0.2010 is latest 1.0, now available on rolling release (run liveuptate to update modules and kernel).</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://linuxconsole.org/&quot;&gt;http://linuxconsole.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o LinuxConsole é uma distribuição vocacionada para jogos, bastante rápida e fácil de utilizar.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; A versão 1.0.2010 é a versão mais recente, agora disponível como &quot;rolling release&quot; (execute o &quot;liveuptate&quot; para atualizar os módulos e o kernel).</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://linuxmint.com/&quot;&gt;http://linuxmint.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Linux Mint is a user-friendly Ubuntu-based distribution which includes additional proprietary codecs and other software by default.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://linuxmint.com/&quot;&gt;http://linuxmint.com&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o Linux Mint é uma distribuição baseada em Ubuntu que inclui aplicações e codificadores proprietários na sua instalação.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; a versão &quot;Live&quot; permite arrancar em modo &quot;Live&quot;, a partir do qual pode efetuar a instalação.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.mandriva.com/&quot;&gt;http://www.mandriva.com/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; Mandriva is a user-friendly distro formerly known as Mandrake Linux.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; The Live version allows for booting in Live mode, from which the installer can optionally be launched. The NetInstall version allows for installation over the internet (FTP) or via pre-downloaded &lt;a href=&quot;http://www.mandriva.com/en/download&quot;&gt;&quot;Free&quot; iso image files&lt;/a&gt;.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://www.mandriva.com/&quot;&gt;http://www.mandriva.com/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o Mandriva é uma distribuição fácil de utilizar, anteriormente conhecida como Mandrake Linux.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; a versão &quot;Live&quot; permite o arranque no modo &quot;Live&quot;, a partir do qual pode iniciar a instalação. A versão &quot;NetInstall&quot; permite a instalação através da internet (FTP) ou utilizando os ficheiros de imagem &lt;a href=&quot;http://www.mandriva.com/en/download&quot;&gt;&quot;Free&quot;&lt;/a&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.mepis.org/&quot;&gt;http://www.mepis.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; MEPIS is a Debian-based distribution. SimplyMEPIS is a user-friendly version based on KDE, while AntiX is a lightweight version for older computers.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; MEPIS supports booting in Live mode, from which the installer can optionally be launched.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://www.mepis.org/&quot;&gt;http://www.mepis.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o MEPIS é uma distribuição baseada em Debian. O SimplyMEPIS é a versão KDE, enquanto que o AntiX é uma versão para computadores mais antigos.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; MEPIS a versão &quot;Live&quot; permite o arranque no modo &quot;Live&quot;, a partir do qual pode iniciar a instalação.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://netbootcd.tuxfamily.org/&quot;&gt;http://netbootcd.tuxfamily.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; NetbootCD is a small boot CD that downloads and boots network-based installers for other distributions.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NetbootCD boots and runs in live mode.</source>
        <translation type="obsolete">&lt;b&gt;Pagina inicial:&lt;/b&gt; &lt;a href=&quot;http://netbootcd.tuxfamily.org/&quot;&gt;http://netbootcd.tuxfamily.org&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o NetbootCD é um CD de arranque que disponibiliza a transferência de instaladores através da web para outras distribuições.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; o NetbootCD é executado no modo &quot;Live&quot;.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://www.nimblex.net/&quot;&gt;http://www.nimblex.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; NimbleX is a small, versatile Slackware-based distribution. It is built using the linux-live scripts, and features the KDE desktop. It can be booted from CD or flash memory (USB pens or MP3 players), and can easily be customized and extended.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NimbleX boots in Live mode.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://www.nimblex.net/&quot;&gt;http://www.nimblex.net&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o NimbleX é uma distribuição pequena e versátil, baseada em Slackware. É compilada através de scripts linux e tem o ambiente de trabalho KDE. Pode ser iniciado através de CD ou dispositivo amovíveis (USB, leitores MP3...) e pode ser moldado e expandido de acordo com o seu gosto.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; o NimbleX é executado no modo &quot;Live&quot;.</translation>
    </message>
    <message>
        <source>&lt;b&gt;Homepage:&lt;/b&gt; &lt;a href=&quot;http://pogostick.net/~pnh/ntpasswd/&quot;&gt;http://pogostick.net/~pnh/ntpasswd/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Description:&lt;/b&gt; The Offline NT Password and Registry Editor can reset Windows passwords and edit the registry on Windows 2000-Vista.&lt;br/&gt;&lt;b&gt;Install Notes:&lt;/b&gt; NTPasswd is booted and run in live mode; no installation is required to use it.</source>
        <translation type="obsolete">&lt;b&gt;Página inicial:&lt;/b&gt; &lt;a href=&quot;http://pogostick.net/~pnh/ntpasswd/&quot;&gt;http://pogostick.net/~pnh/ntpasswd/&lt;/a&gt;&lt;br/&gt;&lt;b&gt;Descrição:&lt;/b&gt; o Offline NT Password and Registry Editor pode repor as suas senhas e editar o registo de sistemas Windows 2000-Vista.&lt;br/&gt;&lt;b&gt;Notas de instalação:&lt;/b&gt; o NTPasswd é executado no modo &quot;Live&quot; e não requer instalação.</translation>
    </message>
</context>
<context>
    <name>unetbootinui</name>
    <message>
        <location filename="unetbootin.ui" line="20"/>
        <source>Unetbootin</source>
        <translation>Unetbootin</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="44"/>
        <location filename="unetbootin.ui" line="65"/>
        <source>Select from a list of supported distributions</source>
        <translation>Selecione de uma lista de distribuições suportadas</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="47"/>
        <source>&amp;Distribution</source>
        <translation>&amp;Distribuição</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="131"/>
        <source>Specify a disk image file to load</source>
        <translation>Especifique a imagem de disco a carregar</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="134"/>
        <source>Disk&amp;image</source>
        <translation>Imagem de d&amp;isco</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="147"/>
        <source>Manually specify a kernel and initrd to load</source>
        <translation>Especifique o kernel e initrd a carregar</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="150"/>
        <source>&amp;Custom</source>
        <translation>&amp;Personalizado</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="414"/>
        <location filename="unetbootin.ui" line="430"/>
        <source>Space to reserve for user files which are preserved across reboots. Works only for LiveUSBs for Ubuntu and derivatives. If value exceeds drive capacity, the maximum space available will be used.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="417"/>
        <source>Space used to preserve files across reboots (Ubuntu only):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="440"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="503"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="506"/>
        <source>Return</source>
        <translation>Recuar</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="513"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="516"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="561"/>
        <source>Reboot Now</source>
        <translation>Reiniciar agora</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="568"/>
        <source>Exit</source>
        <translation>Sair</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="660"/>
        <source>1. Downloading Files</source>
        <translation>1. Transferir ficheiros</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="667"/>
        <source>2. Extracting and Copying Files</source>
        <translation>2. Extrair e copiar ficheiros</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="674"/>
        <source>3. Installing Bootloader</source>
        <translation>3. Instalar carregador de arranque</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="681"/>
        <source>4. Installation Complete, Reboot</source>
        <translation>4. Instalação terminada. Deve reiniciar</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="477"/>
        <location filename="unetbootin.ui" line="496"/>
        <source>Select the target drive to install to</source>
        <translation>Selecione o disco de destino da instalação</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="480"/>
        <source>Dri&amp;ve:</source>
        <translation>Dis&amp;co:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="451"/>
        <location filename="unetbootin.ui" line="470"/>
        <source>Select the installation target type</source>
        <translation>Selecione o tipo de instalação desejado</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="454"/>
        <source>&amp;Type:</source>
        <translation>&amp;Tipo:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="81"/>
        <source>Select the distribution version</source>
        <translation>Selecione a versão da distribuição</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="347"/>
        <source>Select disk image file</source>
        <translation>Selecione o ficheiro da imagem</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="286"/>
        <location filename="unetbootin.ui" line="350"/>
        <location filename="unetbootin.ui" line="375"/>
        <location filename="unetbootin.ui" line="400"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="188"/>
        <source>Select the disk image type</source>
        <translation>Selecione o tipo da imagem</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="243"/>
        <source>Specify a floppy/hard disk image, or CD image (ISO) file to load</source>
        <translation>Especifique a imagem de disquete/disco rígido ou a imagem (ISO) a carregar</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="207"/>
        <location filename="unetbootin.ui" line="258"/>
        <source>Specify a kernel file to load</source>
        <translation>Especifique o ficheiro kernel a carregar</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="283"/>
        <source>Select kernel file</source>
        <translation>Selecione o ficheiro kernel</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="293"/>
        <location filename="unetbootin.ui" line="312"/>
        <source>Specify an initrd file to load</source>
        <translation>Especifique o ficheiro initrd a carregar</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="372"/>
        <source>Select initrd file</source>
        <translation>Selecione o ficheiro initrd</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="397"/>
        <source>Select syslinux.cfg or isolinux.cfg file</source>
        <translation>Selecione o ficheiro syslinux.cfg ou isolinux.cfg</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="226"/>
        <location filename="unetbootin.ui" line="321"/>
        <source>Specify parameters and options to pass to the kernel</source>
        <translation>Especifique os parâmetros e opções a transmitir ao kernel</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="210"/>
        <source>&amp;Kernel:</source>
        <translation>&amp;Kernel:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="296"/>
        <source>Init&amp;rd:</source>
        <translation>Init&amp;rd:</translation>
    </message>
    <message>
        <location filename="unetbootin.ui" line="229"/>
        <source>&amp;Options:</source>
        <translation>&amp;Opções:</translation>
    </message>
</context>
<context>
    <name>uninstaller</name>
    <message>
        <location filename="main.cpp" line="156"/>
        <source>Uninstallation Complete</source>
        <translation>Desinstalação terminada</translation>
    </message>
    <message>
        <location filename="main.cpp" line="157"/>
        <source>%1 has been uninstalled.</source>
        <translation>%1 foi desinstalado.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="322"/>
        <source>Must run as root</source>
        <translation>Deve ser executado como root</translation>
    </message>
    <message>
        <location filename="main.cpp" line="324"/>
        <source>%2 must be run as root. Close it, and re-run using either:&lt;br/&gt;&lt;b&gt;sudo %1&lt;/b&gt;&lt;br/&gt;or:&lt;br/&gt;&lt;b&gt;su - -c &apos;%1&apos;&lt;/b&gt;</source>
        <translation>Deve executar %s como root. Feche a aplicação e utilize: &lt;br/&gt;&lt;b&gt;sudo %1&lt;/b&gt;&lt;br/&gt;ou:&lt;br/&gt;&lt;b&gt;su - -c %1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="361"/>
        <source>%1 Uninstaller</source>
        <translation>Desinstalador do %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="362"/>
        <source>%1 is currently installed. Remove the existing version?</source>
        <translation>%1 está instalado. Remover a versão existente?</translation>
    </message>
</context>
</TS>
