/****************************************************************************
** Meta object code from reading C++ file 'unetbootin.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "unetbootin.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'unetbootin.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_customver[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_customver[] = {
    "customver\0"
};

void customver::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData customver::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject customver::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_customver,
      qt_meta_data_customver, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &customver::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *customver::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *customver::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_customver))
        return static_cast<void*>(const_cast< customver*>(this));
    return QObject::qt_metacast(_clname);
}

int customver::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_uninstaller[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_uninstaller[] = {
    "uninstaller\0"
};

void uninstaller::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData uninstaller::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject uninstaller::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_uninstaller,
      qt_meta_data_uninstaller, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &uninstaller::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *uninstaller::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *uninstaller::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_uninstaller))
        return static_cast<void*>(const_cast< uninstaller*>(this));
    return QObject::qt_metacast(_clname);
}

int uninstaller::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_copyfileT[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      28,   11,   10,   10, 0x05,
      56,   10,   10,   10, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_copyfileT[] = {
    "copyfileT\0\0dlbytes,maxbytes\0"
    "datacopied64(qint64,qint64)\0finished()\0"
};

void copyfileT::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        copyfileT *_t = static_cast<copyfileT *>(_o);
        switch (_id) {
        case 0: _t->datacopied64((*reinterpret_cast< qint64(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2]))); break;
        case 1: _t->finished(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData copyfileT::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject copyfileT::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_copyfileT,
      qt_meta_data_copyfileT, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &copyfileT::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *copyfileT::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *copyfileT::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_copyfileT))
        return static_cast<void*>(const_cast< copyfileT*>(this));
    return QThread::qt_metacast(_clname);
}

int copyfileT::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void copyfileT::datacopied64(qint64 _t1, qint64 _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void copyfileT::finished()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
static const uint qt_meta_data_nDirListStor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      24,   14,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_nDirListStor[] = {
    "nDirListStor\0\0curDirUrl\0"
    "sAppendSelfUrlInfoList(QUrlInfo)\0"
};

void nDirListStor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        nDirListStor *_t = static_cast<nDirListStor *>(_o);
        switch (_id) {
        case 0: _t->sAppendSelfUrlInfoList((*reinterpret_cast< QUrlInfo(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData nDirListStor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject nDirListStor::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_nDirListStor,
      qt_meta_data_nDirListStor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &nDirListStor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *nDirListStor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *nDirListStor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_nDirListStor))
        return static_cast<void*>(const_cast< nDirListStor*>(this));
    return QObject::qt_metacast(_clname);
}

int nDirListStor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_unetbootin[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      30,   12,   11,   11, 0x08,
      87,   71,   11,   11, 0x08,
     126,   11,   11,   11, 0x08,
     162,   11,   11,   11, 0x08,
     207,   11,   11,   11, 0x08,
     239,   11,   11,   11, 0x08,
     271,   11,   11,   11, 0x08,
     303,   11,   11,   11, 0x08,
     332,   11,   11,   11, 0x08,
     358,   11,   11,   11, 0x08,
     385,   11,   11,   11, 0x08,
     427,  410,   11,   11, 0x0a,
     453,  410,   11,   11, 0x0a,
     487,  410,   11,   11, 0x0a,
     521,   11,   11,   11, 0x0a,
     543,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_unetbootin[] = {
    "unetbootin\0\0distroselectIndex\0"
    "on_distroselect_currentIndexChanged(int)\0"
    "typeselectIndex\0on_typeselect_currentIndexChanged(int)\0"
    "on_dverselect_currentIndexChanged()\0"
    "on_diskimagetypeselect_currentIndexChanged()\0"
    "on_FloppyFileSelector_clicked()\0"
    "on_KernelFileSelector_clicked()\0"
    "on_InitrdFileSelector_clicked()\0"
    "on_CfgFileSelector_clicked()\0"
    "on_cancelbutton_clicked()\0"
    "on_frebootbutton_clicked()\0"
    "on_fexitbutton_clicked()\0dlbytes,maxbytes\0"
    "dlprogressupdate(int,int)\0"
    "dlprogressupdate64(qint64,qint64)\0"
    "cpprogressupdate64(qint64,qint64)\0"
    "on_okbutton_clicked()\0killApplication()\0"
};

void unetbootin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        unetbootin *_t = static_cast<unetbootin *>(_o);
        switch (_id) {
        case 0: _t->on_distroselect_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_typeselect_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_dverselect_currentIndexChanged(); break;
        case 3: _t->on_diskimagetypeselect_currentIndexChanged(); break;
        case 4: _t->on_FloppyFileSelector_clicked(); break;
        case 5: _t->on_KernelFileSelector_clicked(); break;
        case 6: _t->on_InitrdFileSelector_clicked(); break;
        case 7: _t->on_CfgFileSelector_clicked(); break;
        case 8: _t->on_cancelbutton_clicked(); break;
        case 9: _t->on_frebootbutton_clicked(); break;
        case 10: _t->on_fexitbutton_clicked(); break;
        case 11: _t->dlprogressupdate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 12: _t->dlprogressupdate64((*reinterpret_cast< qint64(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2]))); break;
        case 13: _t->cpprogressupdate64((*reinterpret_cast< qint64(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2]))); break;
        case 14: _t->on_okbutton_clicked(); break;
        case 15: _t->killApplication(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData unetbootin::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject unetbootin::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_unetbootin,
      qt_meta_data_unetbootin, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &unetbootin::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *unetbootin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *unetbootin::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_unetbootin))
        return static_cast<void*>(const_cast< unetbootin*>(this));
    return QWidget::qt_metacast(_clname);
}

int unetbootin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
